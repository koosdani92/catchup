package hu.bme.xj4vjg.catchupapi;

import javax.ws.rs.core.Response;

/**
 * A szerver m�k�d�se sor�n el�fordul� hib�khoz tartoz� �zenetek.
 */
public class ApiMessages {
	// INFO priorit�s� �zenetek
	public static final String API_REQUEST_SUCCESSFUL = "Keres sikeresen teljesitve.";
	public static final String GCM_MESSAGE_SENT = "GCM �rtes�t�s elk�ldve.";
	public static final String GCM_MESSAGE_PREPARING = "GCM �zenet el�k�sz�t�se";
	
	// Hiba priorit�s� �zenetek
	public static final String DEFAULT_ERROR = "Ismeretlen belso hiba.";
	public static final String ERROR_LOADING_JDBC_DRIVER = "A JDBC meghajt� bet�lt�se sikertelen volt.";
	public static final String ERROR_CONNECTING_TO_DATABASE = "Nem sikerult csatlakozni az adatbazis szerverhez.";
	public static final String ERROR_CLOSING_DB_CONNECTION = "Adatbazis kapcsolat lezarasa sikertelen.";
	public static final String ERROR_CLOSING_DB_STATEMENT = "Statement lezarasa sikertelen.";
	public static final String ERROR_DB = "Ismeretlen adatbazis hiba.";
	public static final String NON_EXISTENT_USER = "Ismeretlen felhaszn�l�.";
	public static final String ERROR_SENDING_GCM_MESSAGE = "GCM �zenet k�ld�se sikertelen.";
}
