package hu.bme.xj4vjg.catchupapi;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EnumType;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import hu.bme.xj4vjg.catchupapi.ApiException.ApiExceptionType;
import hu.bme.xj4vjg.catchupapi.param.AnswerEventInviteRequest;
import hu.bme.xj4vjg.catchupapi.param.AnswerFriendRequestRequest;
import hu.bme.xj4vjg.catchupapi.param.AuthRequest;
import hu.bme.xj4vjg.catchupapi.param.BasicLoginRequest;
import hu.bme.xj4vjg.catchupapi.param.BasicRegisterRequest;
import hu.bme.xj4vjg.catchupapi.param.CreateEventRequest;
import hu.bme.xj4vjg.catchupapi.param.Event;
import hu.bme.xj4vjg.catchupapi.param.EventInvitationState;
import hu.bme.xj4vjg.catchupapi.param.EventInvite;
import hu.bme.xj4vjg.catchupapi.param.FacebookLoginRequest;
import hu.bme.xj4vjg.catchupapi.param.FacebookSignInRequest;
import hu.bme.xj4vjg.catchupapi.param.FriendRequest;
import hu.bme.xj4vjg.catchupapi.param.FriendRequestState;
import hu.bme.xj4vjg.catchupapi.param.GetEventRequest;
import hu.bme.xj4vjg.catchupapi.param.GoogleLoginRequest;
import hu.bme.xj4vjg.catchupapi.param.GoogleSignInRequest;
import hu.bme.xj4vjg.catchupapi.param.InvitedUser;
import hu.bme.xj4vjg.catchupapi.param.LoginResponse;
import hu.bme.xj4vjg.catchupapi.param.Notification;
import hu.bme.xj4vjg.catchupapi.param.NotificationType;
import hu.bme.xj4vjg.catchupapi.param.SearchUserRequest;
import hu.bme.xj4vjg.catchupapi.param.SendEventInviteRequest;
import hu.bme.xj4vjg.catchupapi.param.SendFriendRequestRequest;
import hu.bme.xj4vjg.catchupapi.param.UpdateGcmTokenRequest;
import hu.bme.xj4vjg.catchupapi.param.User;
import hu.bme.xj4vjg.catchupapi.param.UserDetail;
import sun.util.resources.LocaleNames_hi;

/**
 * REST API oszt�ly a h�v�sok fogad�s�ra.
 * URI: "domain"/CatchUpApi/
 */
@Path("")
public class CatchUpServer {
	// Bejelentkez�shez t�mogatott k�z�ss�gi h�l�zat t�pusok
	public static enum SocialNetworkType {Google, Facebook};
	
	// MySQL szerver sztringek a kapcsol�d�shoz �s inicializ�l�shoz.
	private static final String URI_MYSQL_JDBC_DRIVER = 
			"com.mysql.jdbc.Driver";
	private static final String URI_DB_CONNECTION = 
			"jdbc:mysql://localhost/catchupdb?user=CatchUpApi&password=cfrzkj";
	// Adatb�zis kapcsolat objektum
	private Connection connection = null;
	
	/**
	 * JDBC driver inicializ�l�sa �s az adatb�ziskapcsolat l�trehoz�sa.
	 * @return Az adatb�ziskapcsolat objektum. Sikertelen csatlakoz�s eset�n NULL.
	 */
	private Connection CreateDbConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(URI_DB_CONNECTION);
		} catch (ClassNotFoundException e) {
			Logger.Log(ApiMessages.ERROR_LOADING_JDBC_DRIVER);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_CONNECTING_TO_DATABASE);
		}
		return null;
	}
	
	/**
	 * Adatb�zis kapcsolat �s a hozz� tartoz� Statement objektumok lez�r�sa.
	 * @param statements A Statement objektumok.
	 */
	private void closeDb(Statement... statements) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_CLOSING_DB_CONNECTION);
		}
		
		for (Statement statement : statements) {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				Logger.Log(ApiMessages.ERROR_CLOSING_DB_STATEMENT);
			}
		}
	}
	
	/**
	 * Adatb�zis utas�t�s lez�r�sa.
	 * @param statements A Statement objektum.
	 */
	private void closeStatement(Statement... statements) {
		try {
			for (Statement statement : statements) {
				if (statement != null) {
					statement.close();
				}
			}
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_CLOSING_DB_CONNECTION);
		}
	}
	
	/**
	 * Az �sszes felhaszn�l� email c�m�nek list�z�sa. 
	 * @return RestResponse<List<String>>
	 * 	A felhaszn�l�k email c�mei.
	 */
	@GET
	@Path("/Users")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<List<String>> listUserEmails() {
		// DB kapcsolat l�trehoz�sa
		Statement statement = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<List<String>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// Email c�mek lek�rdez�se �s feldolgoz�sa
		try {
			statement = connection.createStatement();
			String queryString = "SELECT Email FROM catchupdb.user";
			ResultSet resultSet = statement.executeQuery(queryString);
			
			List<String> emails = new ArrayList<String>();
			while (resultSet.next()) {
				emails.add(resultSet.getString(1));
			}
			
			Logger.Log("listUserEmails", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<List<String>>(emails);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<List<String>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statement);
		}
	}

	/**
	 * Felhaszn�l� regisztr�l�sa az adataianak megad�s�val.
	 * @param params A felhaszn�l� adatai.
	 * @return RestResponse<Boolean>
	 * 	A regisztr�ci� sikeress�ge.
	 */
	@PUT
	@Path("/BasicRegister")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Boolean> basicRegister(BasicRegisterRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement preparedStatementQuery = null;
		PreparedStatement preparedStatementInsert = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// Megn�zz�k, hogy van-e ilyen email c�mmel rendelkez� felhaszn�l�
		try {
			connection.setAutoCommit(false);
			String queryString = "SELECT Email FROM catchupdb.user WHERE UPPER(Email) LIKE UPPER(?)";
			preparedStatementQuery = connection.prepareStatement(queryString);
			preparedStatementQuery.setString(1, request.getEmail());
			ResultSet resultSet = preparedStatementQuery.executeQuery();
			// Ha m�r az email c�mmel regisztr�lt valaki, akkor hib�t jelz�nk false-al.
			if (resultSet.next()) {
				connection.commit();
				connection.setAutoCommit(true);
				Logger.Log("basicRegister", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			
			// Ha m�g nincs ilyen email c�m a rendszerben, akkor beregisztr�ljuk a felhaszn�l�t.
			String insertString = 
					"INSERT INTO catchupdb.user (Email, Password, FirstName, LastName) VALUES(?, ?, ?, ?)";
			preparedStatementInsert = connection.prepareStatement(insertString);
			preparedStatementInsert.setString(1, request.getEmail());
			preparedStatementInsert.setString(2, request.getPass());
			preparedStatementInsert.setString(3, request.getFirstName());
			preparedStatementInsert.setString(4, request.getLastName());
			preparedStatementInsert.executeUpdate();
			connection.commit();
			connection.setAutoCommit(true);
			
			Logger.Log("basicRegister", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<Boolean>(Boolean.TRUE);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(preparedStatementQuery, preparedStatementInsert);
		}
	}
	
	/**
	 * Felhaszn�l� bejelentkez�se email c�m �s jelsz� megad�s�val.
	 * @param params A felhaszn�l� adatai.
	 * @return RestResponse<LoginResponse>
	 * 	A bejelentkez�s sikeress�ge.
	 */
	@POST
	@Path("/BasicLogin")
	@Consumes(MediaType.APPLICATION_JSON)
	public RestResponse<LoginResponse> basicLogin(BasicLoginRequest request) {
		int userId = -1;
		// DB kapcsolat l�trehoz�sa
		PreparedStatement preparedStatement = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<LoginResponse>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// Ellen�rizz�k a felhaszn�l� email c�m�t �s jelszav�t
		try {
			String queryString = 
					"SELECT Id, Email, Password, FirstName, LastName "
					+ "FROM catchupdb.user "
					+ "WHERE UPPER(Email) LIKE UPPER(?) AND Password LIKE ?";
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setString(1, request.getEmail());
			preparedStatement.setString(2, request.getPass());
			ResultSet resultSet = preparedStatement.executeQuery();
			// Ha az eredm�nylista �res, akkor a bejelentkez�s sikertelen,
			// ellenkez� esetben pedig sikeres
			if (resultSet.next()) {
				userId = resultSet.getInt(1);
				User user = new User(
						resultSet.getInt(1),
						resultSet.getString(2),
						resultSet.getString(4),
						resultSet.getString(5));
				LoginResponse response = new LoginResponse(true, userId, user);
				Logger.Log("basicLogin", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<LoginResponse>(response);
			}
			else {
				LoginResponse response = new LoginResponse(false, userId);
				Logger.Log("basicLogin", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<LoginResponse>(response);
			}
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<LoginResponse>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(preparedStatement);
		}
	}
	
	/**
	 * Felhaszn�l� bejelentkez�se Facebook fi�kkal.
	 * @param params A felhaszn�l� Facebook azonos�t�ja.
	 * @return RestResponse<LoginResponse>
	 * 	A bejelentkez�s sikeress�g�t �s a felhaszn�l� azonos�t�j�t tartalmaz� objektum.
	 *  Sikertelen bejelentkez�s eset�n az azonos�t� -1 �rt�ket vesz fel.
	 */
	@POST
	@Path("/FacebookLogin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RestResponse<LoginResponse> facebookLogin(FacebookLoginRequest request) {
		return socialLogin(SocialNetworkType.Facebook, request.getFacebookId());
	}
	
	/**
	 * Felhaszn�l� bejelentkez�se Google fi�kkal.
	 * @param params A felhaszn�l� Google azonos�t�ja.
	 * @return RestResponse<LoginResponse>
	 * 	A bejelentkez�s sikeress�g�t �s a felhaszn�l� azonos�t�j�t tartalmaz� objektum.
	 *  Sikertelen bejelentkez�s eset�n az azonos�t� -1 �rt�ket vesz fel.
	 */
	@POST
	@Path("/GoogleLogin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RestResponse<LoginResponse> googleLogin(GoogleLoginRequest request) {
		return socialLogin(SocialNetworkType.Google, request.getGoogleId());
	}

	/**
	 * Felhaszn�l� bejelentkez�se k�z�ss�gi h�l�zat seg�ts�g�vel.
	 * @param socialId A felhaszn�l� k�zz�s�gi h�l�zatbeli azonos�t�.
	 * @return RestResponse<LoginResponse>
	 *	A bejelentkez�s sikeress�g�t �s a felhaszn�l� azonos�t�j�t tartalmaz� objektum.
	 *  Sikertelen bejelentkez�s eset�n az azonos�t� -1 �rt�ket vesz fel.
	 */
	private RestResponse<LoginResponse> socialLogin(SocialNetworkType socialType, String socialId) {
		String socialString = null;
		switch (socialType) {
		case Facebook:
			socialString = "FacebookId";
			break;
		case Google:
			socialString = "GoogleId";
			break;
		}
		
		// DB kapcsolat l�trehoz�sa
		PreparedStatement preparedStatement = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<LoginResponse>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// Ellen�rizz�k, hogy van-e ilyen Google azonos�t� az adatb�zisban.
		try {
			String queryString = 
					"SELECT Id, " + socialString + ", Email, FirstName, LastName "
							+ "FROM catchupdb.user "
							+ "WHERE " + socialString + " LIKE ?";
			preparedStatement = connection.prepareStatement(queryString);
			preparedStatement.setString(1, socialId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			// Ha az eredm�nylista nem �res, akkor a bejelentkez�s sikeres,
			// ellenkez� esetben pedig sikertelen
			LoginResponse loginResponse;
			if (resultSet.next()) {
				User user = new User(
						resultSet.getInt(1),
						resultSet.getString(3),
						resultSet.getString(4),
						resultSet.getString(5));
				loginResponse = new LoginResponse(true, resultSet.getInt(1), user);
			}
			else {
				loginResponse = new LoginResponse(false, -1);
			}
			
			Logger.Log("Login in with " + socialString, ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<LoginResponse>(loginResponse);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<LoginResponse>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(preparedStatement);
		}
	}
	
	/**
	 * Felhaszn�l� bejelentkez�se Facebook fi�kkal.
	 * Ha �j felhaszn�l�r�l van sz�, akkor regisztr�ci�t is v�gez.
	 * @param FacebookSignInRequest
	 * 	A felhaszn�l� adatai.
	 * @return RestResponse<LoginResponse>
	 * 	A bejelentkez�s sikeress�g�t �s a felhaszn�l� azonos�t�j�t tartalmaz� objektum.
	 *  Sikertelen bejelentkez�s eset�n az azonos�t� -1 �rt�ket vesz fel.
	 */
	@PUT
	@Path("/FacebookSignIn")
	@Consumes(MediaType.APPLICATION_JSON)
	public RestResponse<LoginResponse> facebookSignIn(FacebookSignInRequest request) {
		return SocialSignIn(
				SocialNetworkType.Facebook, 
				request.getFacebbokId(), 
				request.getEmail(), 
				request.getFirstName(), 
				request.getLastName());
	}
	
	/**
	 * Felhaszn�l� bejelentkez�se Google fi�kkal.
	 * Ha �j felhaszn�l�r�l van sz�, akkor regisztr�ci�t is v�gez.
	 * @param GoogleSignInRequest
	 * 	A felhaszn�l� adatai.
	 * @return RestResponse<LoginResponse>
	 * 	A bejelentkez�s sikeress�g�t �s a felhaszn�l� azonos�t�j�t tartalmaz� objektum.
	 *  Sikertelen bejelentkez�s eset�n az azonos�t� -1 �rt�ket vesz fel.
	 */
	@PUT
	@Path("/GoogleSignIn")
	@Consumes(MediaType.APPLICATION_JSON)
	public RestResponse<LoginResponse> googleSignIn(GoogleSignInRequest request) {
		return SocialSignIn(
				SocialNetworkType.Google, 
				request.getGoogleId(), 
				request.getEmail(), 
				request.getFirstName(), 
				request.getLastName());
	}
	
	/**
	 * Felhaszn�l� bejelentkez�se k�z�ss�gi h�l�zat seg�ts�g�vel.
	 * Ha �j felhaszn�l�r�l van sz�, akkor regisztr�ci�t is v�gez.
	 * @param socialType A k�z�ss�gi h�l�zat t�pusa.
	 * @param socialId A felhaszn�l� k�z�ss�gi h�l�zatbeli azonos�t�ja.
	 * @param email A felhaszn�l� email c�me.
	 * @param firstName A felhaszn�l� keresztneve.
	 * @param lastName A felhaszn�l� vezet�kneve.
	 * @return RestResponse<LoginResponse>
	 * 	A bejelentkez�s sikeress�g�t �s a felhaszn�l� azonos�t�j�t tartalmaz� objektum.
	 *  Sikertelen bejelentkez�s eset�n az azonos�t� -1 �rt�ket vesz fel.
	 */
	private RestResponse<LoginResponse> SocialSignIn(
			SocialNetworkType socialType, String socialId, String email, String firstName, String lastName) {
		String socialString = null;
		int userId = -1;
		User user = null;
		switch (socialType) {
		case Facebook:
			socialString = "FacebookId";
			break;
		case Google:
			socialString = "GoogleId";
			break;
		}
		
		// DB kapcsolat l�trehoz�sa
		PreparedStatement preparedStatementQuery = null;
		PreparedStatement preparedStatementInsert = null;
		PreparedStatement preparedStatementQueryId = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<LoginResponse>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// Megn�zz�k, hogy a megadott k�z�ss�gi azonos�t�val m�r regisztr�ltak-e
		try {
			connection.setAutoCommit(false);
			String queryString = 
					"SELECT Id, " + socialString + ", Email, FirstName, LastName "
							+ "FROM catchupdb.user "
							+ "WHERE " + socialString + " LIKE ?";
			preparedStatementQuery = connection.prepareStatement(queryString);
			preparedStatementQuery.setString(1, socialId);
			ResultSet resultSet = preparedStatementQuery.executeQuery();
			
			// Ha m�g nincs ilyen k�z�ss�gi ID, akkor beregisztr�ljuk a felhaszn�l�t
			if (!resultSet.next()) {
				String insertString = 
						"INSERT INTO catchupdb.user (Email, FirstName, LastName, " + socialString + ") VALUES(?, ?, ?, ?)";
				preparedStatementInsert = connection.prepareStatement(insertString);
				preparedStatementInsert.setString(1, email);
				preparedStatementInsert.setString(2, firstName);
				preparedStatementInsert.setString(3, lastName);
				preparedStatementInsert.setString(4, socialId);
				preparedStatementInsert.executeUpdate();
				connection.commit();
				connection.setAutoCommit(true);				
				
				// Gener�lt ID lek�rdez�se
				queryString = 
						"SELECT Id, Email, FirstName, LastName "
						+ "FROM catchupdb.user "
						+ "WHERE " + socialString + " LIKE ?";
				preparedStatementQueryId = connection.prepareStatement(queryString);
				preparedStatementQueryId.setString(1, socialId);
				ResultSet resultSetId = preparedStatementQueryId.executeQuery();
				
				if (resultSetId.next()) {
					user = new User(
							resultSetId.getInt(1),
							resultSetId.getString(2),
							resultSetId.getString(3),
							resultSetId.getString(4));
					userId = resultSetId.getInt(1);
				}
				else {
					Logger.Log(ApiMessages.ERROR_DB);
					return new RestResponse<LoginResponse>(
							new ApiException(
									ApiExceptionType.Database, 
									ApiMessages.ERROR_DB));
				}
			}
			else {
				connection.commit();
				connection.setAutoCommit(true);
				
				user = new User(
						resultSet.getInt(1),
						resultSet.getString(3),
						resultSet.getString(4),
						resultSet.getString(5));
				userId = resultSet.getInt("Id");
			}
			
			// V�g�l bejelentkeztetj�k a felhaszn�l�t
			LoginResponse loginResponse = new LoginResponse(true, userId, user);
			Logger.Log("Sign in with " + socialString, ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<LoginResponse>(loginResponse);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<LoginResponse>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(preparedStatementQuery, preparedStatementInsert, preparedStatementQueryId);
		}
	}
	
	/**
	 * Felhaszn�l�hoz tartoz� GCM token friss�t�se.
	 * @param params A felhaszn�l� �j GCM token-e.
	 * @return RestResponse<Boolean>
	 * 	A friss�t�s sikeress�ge.
	 */
	@POST
	@Path("/UpdateGcmToken")
	@Consumes(MediaType.APPLICATION_JSON)
	public RestResponse<Boolean> updateGcmToken(UpdateGcmTokenRequest request) {
		int userId = -1;
		// DB kapcsolat l�trehoz�sa
		PreparedStatement preparedStatementDelete = null;
		PreparedStatement preparedStatementUpdate = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// 
		try {
			// T�r�lj�k a kapott tokent minden felhaszn�l�t�l
			String deleteString = 
					"UPDATE catchupdb.user SET GcmRegistrationToken = NULL WHERE GcmRegistrationToken = ?";
			preparedStatementDelete = connection.prepareStatement(deleteString);
			preparedStatementDelete.setString(1, request.getGcmToken());
			preparedStatementDelete.execute();
			
			// Fel�lirjuk az adott felhaszn�l�hoz tartoz� GCM regisztr�ci�s tokent
			String updateString = 
					"UPDATE catchupdb.user SET GcmRegistrationToken = ? WHERE Id = ?";
			preparedStatementUpdate = connection.prepareStatement(updateString);
			preparedStatementUpdate.setString(1, request.getGcmToken());
			preparedStatementUpdate.setInt(2, request.getUserId());
			int updatedColumns = preparedStatementUpdate.executeUpdate();
			// Ha a m�dos�tott rekordok sz�ma 0, akkor a userId ismeretlen
			Logger.Log("updateGcmToken", ApiMessages.API_REQUEST_SUCCESSFUL);
			if (updatedColumns <= 0) {
				return new RestResponse<Boolean>(
						new ApiException(
								ApiExceptionType.NonexistentUserId, 
								ApiMessages.NON_EXISTENT_USER));
			}
			else {
				return new RestResponse<Boolean>(Boolean.TRUE);
			}
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(preparedStatementDelete);
			closeDb(preparedStatementUpdate);
		}
	}
	
	/**
	 * A felhaszn�l� bar�tainak list�z�sa. 
	 * @return RestResponse<List<User>>
	 * 	A felhaszn�l� bar�tai.
	 */
	@POST
	@Path("/GetFriends")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<List<User>> getFriends(AuthRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement statement = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<List<User>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// User bar�tainak lek�rdez�se
		try {
			String queryString = 
					"(SELECT u.Id, u.Email, u.FirstName, u.LastName "
					+ "FROM catchupdb.user u "
					+ "INNER JOIN catchupdb.friend f ON u.Id = f.RequestedId "
				    + "INNER JOIN catchupdb.friendrequeststate s ON f.State = s.Id "
				    + "WHERE f.RequesterId = ? AND s.State = ?) "
				    + "UNION "
				    + "(SELECT u.Id, u.Email, u.FirstName, u.LastName " 
				    + "FROM catchupdb.user u "
				    + "INNER JOIN catchupdb.friend f ON u.Id = f.RequesterId "
			    	+ "INNER JOIN catchupdb.friendrequeststate s ON f.State = s.Id "
			        + "WHERE f.RequestedId = ? AND s.State = ?) ";
			statement = connection.prepareStatement(queryString);
			statement.setInt(1, request.getUserId());
			statement.setString(2, FriendRequestState.Accepted.toString());
			statement.setInt(3, request.getUserId());
			statement.setString(4, FriendRequestState.Accepted.toString());
			ResultSet resultSet = statement.executeQuery();
			
			List<User> users = new ArrayList<User>();
			while (resultSet.next()) {
				User user = new User(
						resultSet.getInt(1),
						resultSet.getString(2),
						resultSet.getString(3),
						resultSet.getString(4));
				users.add(user);
			}
			
			Logger.Log("getFriends", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<List<User>>(users);
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<List<User>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statement);
		}
	}
	
	/**
	 * Egy esem�ny lek�r�se.
	 * Ha az esem�ny nem l�tezik, akkor NULL-t ad vissza. 
	 * @return RestResponse<Event>
	 * 	Az esem�ny.
	 */
	@POST
	@Path("/GetEvent")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Event> getEvent(GetEventRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement statement = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Event>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			String sqlString = 
					"SELECT e.Id, u.Id, u.Email, u.FirstName, u.LastName, "
					+ "e.Title, e.Description, e.Date, e.Location "
					+ "FROM catchupdb.event e "
						+ "INNER JOIN catchupdb.user u ON e.OwnerId = u.Id "
				    + "WHERE e.Id = ?";
			statement = connection.prepareStatement(sqlString);
			statement.setInt(1, request.getEventId());
			ResultSet resultSet = statement.executeQuery();
			
			Event event = null;
			if (resultSet.next()) {
				int eventId = resultSet.getInt(1);
				User owner = new User(
						resultSet.getInt(2),
						resultSet.getString(3),
						resultSet.getString(4),
						resultSet.getString(5));
				String title = resultSet.getString(6);
				String description = resultSet.getString(7);
				description = (resultSet.wasNull()) ? null : description;
				Timestamp dateTs = resultSet.getTimestamp(8);
				Long date = (resultSet.wasNull()) ? null : dateTs.getTime();
				String location = resultSet.getString(9);
				location = (resultSet.wasNull()) ? null : location;
				event = new Event(
								eventId,
								owner, 
								getInvitedUsers(eventId),
								title, 
								description, 
								date, 
								location);
			}
			
			Logger.Log("getEvent", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<Event>(event);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Event>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statement);
		}
	}
	
	/**
	 * A felhaszn�l� esem�nyeinek list�z�sa
	 * Azokat az esem�nyeket adja vissza, amelyekre a Usert megh�vt�k,
	 * vagy a User hozta l�tre. 
	 * @return RestResponse<List<Event>
	 * 	A felhaszn�l� esem�nyei.
	 */
	@POST
	@Path("/GetEvents")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<List<Event>> getEvents(AuthRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement statement = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<List<Event>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// Lek�rdezz�k, az �sszes olyan esem�nyt, amiket a User hozott l�tre,
		// vagy amikre a Usert megh�vt�k
		try {
			String sqlString = 
					"(SELECT e.Id, u.Id, u.Email, u.FirstName, u.LastName, "
					+ "e.Title, e.Description, e.Date, e.Location "
					+ "FROM catchupdb.eventinvite i "
					+ "INNER JOIN catchupdb.event e ON i.EventId = e.Id "
				    + "INNER JOIN catchupdb.user u ON e.OwnerId = u.Id "
				    + "WHERE i.InvitedId = ? AND i.State != 3) "
				    + "UNION "
				    + "(SELECT e.Id, u.Id, u.Email, u.FirstName, u.LastName, "
					+ "e.Title, e.Description, e.Date, e.Location "
					+ "FROM catchupdb.event e "
					+ "INNER JOIN catchupdb.user u ON e.OwnerId = u.Id "
				    + "WHERE e.OwnerId = ?)";
			statement = connection.prepareStatement(sqlString);
			statement.setInt(1, request.getUserId());
			statement.setInt(2, request.getUserId());
			ResultSet resultSet = statement.executeQuery();
			
			List<Event> events = new ArrayList<Event>();
			while (resultSet.next()) {
				int eventId = resultSet.getInt(1);
				User owner = new User(
						resultSet.getInt(2),
						resultSet.getString(3),
						resultSet.getString(4),
						resultSet.getString(5));
				String title = resultSet.getString(6);
				String description = resultSet.getString(7);
				description = (resultSet.wasNull()) ? null : description;
				Timestamp dateTs = resultSet.getTimestamp(8);
				Long date = (resultSet.wasNull()) ? null : dateTs.getTime();
				String location = resultSet.getString(9);
				location = (resultSet.wasNull()) ? null : location;
				events.add(
						new Event(
								eventId,
								owner, 
								getInvitedUsers(eventId),
								title, 
								description, 
								date, 
								location));
			}
			
			Logger.Log("getEvents", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<List<Event>>(events);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<List<Event>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statement);
		}
	}
	
	private List<InvitedUser> getInvitedUsers(int eventId) throws SQLException {
		String sqlString = null;
		PreparedStatement statementGetInvitedUsers = null;
		
		try {
			List<InvitedUser> invitedUsers = new ArrayList<InvitedUser>();
			sqlString = 
					"SELECT u.Id, u.Email, u.FirstName, u.LastName, i.State "
					+ "FROM catchupdb.eventinvite i "
						+ "INNER JOIN catchupdb.user u ON i.InvitedId = u.Id "
					+ "WHERE i.EventId = ?";
			statementGetInvitedUsers = connection.prepareStatement(sqlString);
			statementGetInvitedUsers.setInt(1, eventId);
			ResultSet resultSet = statementGetInvitedUsers.executeQuery();
			
			while (resultSet.next()) {
				InvitedUser invitedUser = new InvitedUser(
						new User(
								resultSet.getInt(1),
								resultSet.getString(2),
								resultSet.getString(3),
								resultSet.getString(4)), 
						EventInvitationState.getValueFromId(resultSet.getInt(5)));
				invitedUsers.add(invitedUser);
			}
			
			return invitedUsers;
		} catch (SQLException e) {
			throw e;
		} finally {
			closeStatement(statementGetInvitedUsers);
		}
	}
	
	/**
	 * Esem�ny l�trehoz�sa. 
	 * @return RestResponse<Boolean>
	 * 	A l�trehoz�s sikeress�ge.
	 */
	@POST
	@Path("/CreateEvent")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Event> createEvent(CreateEventRequest request) {
		// DB kapcsolat l�trehoz�sa
		Event event = null;
		PreparedStatement statementQueryId = null;
		PreparedStatement statementQueryOwner = null;
		PreparedStatement statementInsertEvent = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Event>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// Ha a k�r�s nem az esem�ny tulajdonos�t�l j�n, akkor nem hozzuk l�tre
		// az esesem�nyt
		if (request.getUserId() != request.getOwnerId()) {
			return new RestResponse<Event>(null, null);
		}
		
		try {
			connection.setAutoCommit(false);
			// Ellen�rizz�k, hogy a User(tulajdonos) l�tezik-e
			RestResponse<Boolean> userExists = checkUserExists(request.getUserId());
			if (userExists.getException() != null || !userExists.getResponse()) {
				return new RestResponse<Event>(null, userExists.getException());
			}
			
			// Esem�ny ID gener�l�sa
			int eventId = -1;
			String sqlString = 
					"SELECT max(Id) "
					+ "FROM catchupdb.event";
			statementQueryId = connection.prepareStatement(sqlString);
			ResultSet resultSet = statementQueryId.executeQuery();
			
			if (resultSet.next()) {
				eventId = resultSet.getInt(1) + 1;
			}
			else {
				Logger.Log(ApiMessages.ERROR_DB);
				return new RestResponse<Event>(
						new ApiException(
								ApiExceptionType.Database, 
								ApiMessages.ERROR_DB));
			}
			
			// Owner lek�r�se
			User owner;
			sqlString = 
					"SELECT Id, Email, FirstName, LastName "
					+ "FROM catchupdb.user "
					+ "WHERE Id = ?";
			statementQueryOwner = connection.prepareStatement(sqlString);
			statementQueryOwner.setInt(1, request.getUserId());
			resultSet = statementQueryOwner.executeQuery();
			
			if (resultSet.next()) {
				owner = new User(
						resultSet.getInt(1),
						resultSet.getString(2),
						resultSet.getString(3),
						resultSet.getString(4));
			}
			else {
				Logger.Log(ApiMessages.NON_EXISTENT_USER);
				return new RestResponse<Event>(
						new ApiException(
								ApiExceptionType.Database, 
								ApiMessages.ERROR_DB));
			}
			
			// Esem�ny l�trehoz�sa
			sqlString = 
					"INSERT INTO catchupdb.event "
							+ "(Id, OwnerId, Title, Description, Date, Location) "
					+ "VALUES (?, ?, ?, ?, ?, ?)";
			statementInsertEvent = connection.prepareStatement(sqlString);
			statementInsertEvent.setInt(1, eventId);
			statementInsertEvent.setInt(2, request.getOwnerId());
			statementInsertEvent.setString(3, request.getTitle());
			statementInsertEvent.setString(4, request.getDescription());
			Timestamp date = (request.getDate() == null) 
					? null : new Timestamp(request.getDate());
			statementInsertEvent.setTimestamp(5, date);
			statementInsertEvent.setString(6, request.getLocation());
			int updatedColumns = statementInsertEvent.executeUpdate();
			
			if (updatedColumns <= 0) {
				Logger.Log("createEvent", ApiMessages.ERROR_DB);
				return new RestResponse<Event>(
						new ApiException(
								ApiExceptionType.Database, 
								ApiMessages.ERROR_DB));
			}
			connection.commit();
			connection.setAutoCommit(false);
			
			event = new Event(
					eventId, 
					owner, 
					new ArrayList<InvitedUser>(), 
					request.getTitle(), 
					request.getDescription(), 
					request.getDate(), 
					request.getLocation());
		
			Logger.Log("createEvent", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<Event>(event);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Event>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(
					statementQueryId,
					statementQueryOwner,
					statementInsertEvent);
		}
	}
	
	/**
	 * Felhaszn�l� kereslse a megadott keres�si string-el.
	 * �kezetekt�l �s nagybet�/kisbet�-t�l f�ggetlen�l v�gez keres�st
	 *  az Email c�mben, a 'vezet�k + keresztn�v'-ben �s a 'kereszt + vezet�kn�v'
	 *  stringekben. 
	 * @return RestResponse<List<User>>
	 * 	A keres�si felt�teleknek megfelel� felhaszn�l�k..
	 */
	@POST
	@Path("/SearchUser")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<List<UserDetail>> searchUser(SearchUserRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement statement = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<List<UserDetail>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		// A keres�si felt�teleknek megfelel� Userek keres�se
		try {
			String querySearchParam = "%" + request.getSearchString() + "%";
			String queryString = 
					"(SELECT u.Id, u.Email, u.FirstName, u.LastName, null "
					+ "FROM catchupdb.user u "
					+ "WHERE "
						+ "UPPER(u.Email) like UPPER(?) "
						+ "OR UPPER(CONCAT(u.FirstName, ' ', u.LastName)) like UPPER(?) "
						+ "OR UPPER(CONCAT(u.LastName, ' ', u.FirstName)) like UPPER(?)) "
					+ "UNION "
					+ "(SELECT u.Id, u.Email, u.FirstName, u.LastName, f.State "
					+ "FROM catchupdb.user u "
						+ "LEFT OUTER JOIN catchupdb.friend f ON u.Id = f.RequesterId "
					+ "WHERE "
							+ "(UPPER(u.Email) like UPPER(?) "
							+ "OR UPPER(CONCAT(u.FirstName, ' ', u.LastName)) like UPPER(?) "
							+ "OR UPPER(CONCAT(u.LastName, ' ', u.FirstName)) like UPPER(?)) "
					    + "AND "
					    	+ "(f.RequestedId = ?)) "
					+ "UNION "
					+ "(SELECT u.Id, u.Email, u.FirstName, u.LastName, f.State "
					+ "FROM catchupdb.user u "
						+ "LEFT OUTER JOIN catchupdb.friend f ON u.Id = f.RequestedId "
					+ "WHERE "
							+ "(UPPER(u.Email) like UPPER(?) "
							+ "OR UPPER(CONCAT(u.FirstName, ' ', u.LastName)) like UPPER(?) "
							+ "OR UPPER(CONCAT(u.LastName, ' ', u.FirstName)) like UPPER(?)) "
					    + "AND "
					    	+ "(f.RequesterId = ?))";
			statement = connection.prepareStatement(queryString);
			statement.setString(1, querySearchParam);
			statement.setString(2, querySearchParam);
			statement.setString(3, querySearchParam);
			statement.setString(4, querySearchParam);
			statement.setString(5, querySearchParam);
			statement.setString(6, querySearchParam);
			statement.setInt(7, request.getUserId());
			statement.setString(8, querySearchParam);
			statement.setString(9, querySearchParam);
			statement.setString(10, querySearchParam);
			statement.setInt(11, request.getUserId());
			ResultSet resultSet = statement.executeQuery();
			
			List<UserDetail> users = new ArrayList<UserDetail>();
			while (resultSet.next()) {
				FriendRequestState requestState = null;
				boolean friend = false;
				boolean pendingFriend = false;
				int requestStateId = resultSet.getInt(5);
				if (!resultSet.wasNull()) {
					requestState = FriendRequestState
							.getValueFromId(requestStateId);
					friend = (requestState == FriendRequestState.Accepted);
					pendingFriend = (requestState == FriendRequestState.Pending);
				}
				UserDetail userDetail = new UserDetail(
						new User(
							resultSet.getInt(1),
							resultSet.getString(2),
							resultSet.getString(3),
							resultSet.getString(4)),
						friend, 
						pendingFriend);
				
				int i = 0;
				boolean isStoredUser = false;
				while (i < users.size() && !isStoredUser ) {
					if (users.get(i).getUser().getId() 
							== userDetail.getUser().getId()) {
						isStoredUser  = true;
					}
					i++;
				}
				
				i--;
				if (!isStoredUser ) {
					users.add(userDetail);
				}
				else {
					UserDetail storedUser = users.get(i);
					if (!storedUser.isFriend()) {
						storedUser.setFriend(userDetail.isFriend());
					}
					if (!storedUser.isPendingFriend()) {
						storedUser.setPendingFriend(userDetail.isPendingFriend());
					}
				}
			}
			
			int i = 0;
			while (i < users.size()) {
				if (users.get(i).getUser().getId() == request.getUserId()) {
					break;
				}
				i++;
			}
			if (i < users.size()) {
				users.remove(i);
			}
			
			Logger.Log("searchUser", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<List<UserDetail>>(users);
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<List<UserDetail>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statement);
		}
	}

	/**
	 * A felhaszn�l�hoz tartoz� olvasatlan �rtes�t�sek sz�m�nak lek�r�se.
	 * Az �zenetek olvas�si �llapota nem v�ltozik a h�v�s hat�s�ra.
	 * @return RestResponse<Integer>
	 * 	Az olvasatlan �rtes�t�sek.
	 */
	@POST
	@Path("/GetNewNotificationCount")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Integer> getNewNotificationCount(AuthRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement statementFriendRequest = null;
		PreparedStatement statementEventInvite = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Integer>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			// Olvasatlan bar�t felk�r�sek/v�laszok sz�r�se
			String queryString = 
					"SELECT "
					+ "(SELECT COUNT(f.Id) " 
					+ "FROM catchupdb.user u " 
					+ "INNER JOIN catchupdb.friend f ON u.Id = f.RequestedId "
					+ "INNER JOIN catchupdb.friendrequeststate s ON f.State = s.Id "
					+ "WHERE f.RequesterId = ? AND s.State != ? AND f.RequesterSeen = 0) "
					+ "+ "
					+ "(SELECT COUNT(f.Id) " 
					+ "FROM catchupdb.user u " 
					+ "INNER JOIN catchupdb.friend f ON u.Id = f.RequesterId "
				    + "INNER JOIN catchupdb.friendrequeststate s ON f.State = s.Id "
					+ "WHERE f.RequestedId = ? AND s.State = ? AND f.RequestedSeen = 0) "
					+ "AS NewNotifications";
			statementFriendRequest = connection.prepareStatement(queryString);
			statementFriendRequest.setInt(1, request.getUserId());
			statementFriendRequest.setString(2, FriendRequestState.Pending.toString());
			statementFriendRequest.setInt(3, request.getUserId());
			statementFriendRequest.setString(4, FriendRequestState.Pending.toString());
			ResultSet resultSet = statementFriendRequest.executeQuery();
			
			Integer newNotificationCount = 0;
			if (resultSet.next()) {
				newNotificationCount = resultSet.getInt(1);
			}
			
			// Olvasatlan esem�ny megh�v�sok/v�laszok sz�r�se
			queryString = 
					"SELECT "
					+ "(SELECT COUNT(i.Id) "
					+ "FROM catchupdb.eventinvite i "
					+ "INNER JOIN catchupdb.eventinvitationstate s ON i.State = s.Id "
					+ "WHERE i.InvitedId = ? AND s.State = ? AND i.InvitedSeen = 0) "
					+ "+ " 
					+ "(SELECT COUNT(i.Id) "
					+ "FROM catchupdb.eventinvite i "
					+ "INNER JOIN catchupdb.event e ON i.EventId = e.Id "
					+ "INNER JOIN catchupdb.eventinvitationstate s ON i.State = s.Id "
					+ "WHERE e.OwnerId = ? AND s.State != ? AND i.OwnerSeen = 0)";
			statementEventInvite = connection.prepareStatement(queryString);
			statementEventInvite.setInt(1, request.getUserId());
			statementEventInvite.setString(2, EventInvitationState.Pending.toString());
			statementEventInvite.setInt(3, request.getUserId());
			statementEventInvite.setString(4, EventInvitationState.Pending.toString());
			resultSet = statementEventInvite.executeQuery();
			
			if (resultSet.next()) {
				newNotificationCount += resultSet.getInt(1);
			}
			
			Logger.Log("getNewNotificationCount", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<Integer>(newNotificationCount);
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Integer>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statementFriendRequest, statementEventInvite);
		}
	}
	
	/**
	 * A felhaszn�l�hoz tartoz� �rtes�t�sek lek�r�se.
	 * Az �zenetek olvas�si �llapota a k�r�s ut�n megv�ltozik.
	 * @return RestResponse<List<Notification>>
	 * 	Az aktu�lis �rtes�t�sek.
	 */
	@POST
	@Path("/GetNotifications")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<List<Notification>> getNotifications(AuthRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement statementUserQuery = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<List<Notification>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			connection.setAutoCommit(false);
			String sqlString = null;
			User user = null;
			List<Notification> notifications = new ArrayList<Notification>();
			// A felhaszn�l� adatainak lek�rdez�se
			sqlString = 
					"SELECT u.Email, u.FirstName, u.LastName "
					+ "FROM catchupdb.user u "
					+ "WHERE u.Id = ?";
			statementUserQuery = connection.prepareStatement(sqlString);
			statementUserQuery.setInt(1, request.getUserId());
			ResultSet resultSet = statementUserQuery.executeQuery();
			
			if (resultSet.next()) {
				user = new User(
						request.getUserId(),
						resultSet.getString(1),
						resultSet.getString(2),
						resultSet.getString(3));
			}
			// Ha nem tal�ltuk meg a Usert, akkor hib�t dobunk
			else {
				Logger.Log(ApiMessages.NON_EXISTENT_USER);
				return new RestResponse<List<Notification>>(
						new ApiException(
								ApiExceptionType.NonexistentUserId, 
								ApiMessages.NON_EXISTENT_USER));
			}
			
			// Bar�t felk�r�s �s esem�ny megh�v�s t�pus� �rtes�t�sek lek�r�se
			RestResponse<Boolean> friendRequestResponse = 
					getFriendRequestNotifications(user, notifications);
			RestResponse<Boolean> eventInviteResponse =
					getEventInviteNotifications(user, notifications);
			if (friendRequestResponse.getException() != null
					|| !friendRequestResponse.getResponse()
					|| eventInviteResponse.getException() != null
					|| !eventInviteResponse.getResponse()) {
				Logger.Log(ApiMessages.ERROR_DB);
				return new RestResponse<List<Notification>>(
						new ApiException(
								ApiExceptionType.Database, 
								ApiMessages.ERROR_DB));
			}
			
			connection.commit();
			connection.setAutoCommit(true);
			Logger.Log("getNotifications", ApiMessages.API_REQUEST_SUCCESSFUL);
			return new RestResponse<List<Notification>>(notifications);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<List<Notification>>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statementUserQuery);
		}
	}
	
	// TODO
	private RestResponse<Boolean> getEventInviteNotifications(
			User user, List<Notification> notifications) {
		List<Integer> eventInviteIds = null;
		String sqlString = null;
		PreparedStatement statementOwnerQuery = null;
		PreparedStatement statementOwnerUpdate = null;
		PreparedStatement statementInvitedQuery = null;
		PreparedStatement statementInvitedUpdate = null;
		ResultSet resultSet = null;
		StringBuilder sqlMultipleParamsBuilder = null;
		
		try {
			// �rtes�t�sek lek�r�se, amelyekben a User-t h�vt�k meg esem�nyre
			// �s m�g nem v�laszolt r�juk
			sqlString = 
					"SELECT i.Id,  e.Id, " 
					+ "u.Id, u.Email, u.FirstName, u.LastName, "
				    + "e.Title, e.Description, e.Date, e.Location, "
				    + "s.State, i.SendDate, i.AnswerDate, i.InvitedSeen "
					+ "FROM catchupdb.eventinvite i "
					+ "INNER JOIN catchupdb.event e ON i.EventId = e.Id "
				    + "INNER JOIN catchupdb.user u ON e.OwnerId = u.Id "
				    + "INNER JOIN catchupdb.eventinvitationstate s ON i.State = s.Id "
				+ "WHERE i.InvitedId = ? AND s.State = ?";
			statementOwnerQuery = connection.prepareStatement(sqlString);
			statementOwnerQuery.setInt(1, user.getId());
			statementOwnerQuery.setString(2, EventInvitationState.Pending.toString());
			resultSet = statementOwnerQuery.executeQuery();
			
			eventInviteIds = new ArrayList<Integer>();
			while (resultSet.next()) {
				eventInviteIds.add(resultSet.getInt(1));
				// Notification �ssze�l�t��sa
				User owner = new User(
						resultSet.getInt(3), 
						resultSet.getString(4), 
						resultSet.getString(5), 
						resultSet.getString(6));
				
				String description = resultSet.getString(8);
				description = (resultSet.wasNull()) ? null : description;
				Timestamp dateTs = resultSet.getTimestamp(9);
				Long date = (resultSet.wasNull()) ? null : dateTs.getTime();
				String location = resultSet.getString(10);
				location = (resultSet.wasNull()) ? null : location;
				Event event = new Event(
						resultSet.getInt(2), 
						owner, 
						getInvitedUsers(resultSet.getInt(2)),
						resultSet.getString(7), 
						description, 
						date, 
						location);
				
				Timestamp answerDateTs = resultSet.getTimestamp(13);
				Long answerDate = (resultSet.wasNull()) ? null : answerDateTs.getTime();
				Notification notification = new Notification( 
						new EventInvite(
								resultSet.getInt(1), 
								event, 
								user, 
								EventInvitationState.valueOf(resultSet.getString(11)),
								resultSet.getBoolean(14),
								resultSet.getTimestamp(12).getTime(), 
								answerDate));
				notifications.add(notification);
			}
			
			// User-hez tartoz� InvitedSeen bitek be�ll�t�sa olvasott �rt�kre
			if (eventInviteIds.size() > 0) {
				sqlMultipleParamsBuilder = new StringBuilder("?");
				for (int i = 0; i < eventInviteIds.size() - 1; i++) {
					sqlMultipleParamsBuilder.append(", ?");
				}
				sqlString = 
						"UPDATE catchupdb.eventinvite "
						+ "SET InvitedSeen = 1 "
						+ "WHERE Id IN (" + sqlMultipleParamsBuilder.toString() + ")";
				statementOwnerUpdate = connection.prepareStatement(sqlString);
				for (int i = 0; i < eventInviteIds.size(); i++) {
					statementOwnerUpdate.setInt(i + 1, eventInviteIds.get(i));
				}
				statementOwnerUpdate.execute();
			}
			
			// �rtes�t�sek lek�r�se, amelyekben a User h�vott meg m�sokat
			// �s azok m�r elfogadt�k vagy elutas�tott�k a megh�v�st
			sqlString = 
					"SELECT i.Id,  e.Id, " 
					+ "u.Id, u.Email, u.FirstName, u.LastName, "
				    + "e.Title, e.Description, e.Date, e.Location, "
				    + "s.State, i.SendDate, i.AnswerDate, i.OwnerSeen "
					+ "FROM catchupdb.eventinvite i "
					+ "INNER JOIN catchupdb.event e ON i.EventId = e.Id "
				    + "INNER JOIN catchupdb.user u ON i.InvitedId = u.Id "
				    + "INNER JOIN catchupdb.eventinvitationstate s ON i.State = s.Id "
					+ "WHERE e.OwnerId = ? AND s.State != ?";
			statementInvitedQuery = connection.prepareStatement(sqlString);
			statementInvitedQuery.setInt(1, user.getId());
			statementInvitedQuery.setString(2, EventInvitationState.Pending.toString());
			resultSet = statementInvitedQuery.executeQuery();
			
			eventInviteIds = new ArrayList<Integer>();
			while (resultSet.next()) {
				eventInviteIds.add(resultSet.getInt(1));
				// Notification �ssze�l�t��sa
				User invited = new User(
						resultSet.getInt(3), 
						resultSet.getString(4), 
						resultSet.getString(5), 
						resultSet.getString(6));
				
				String description = resultSet.getString(8);
				description = (resultSet.wasNull()) ? null : description;
				Timestamp dateTs = resultSet.getTimestamp(9);
				Long date = (resultSet.wasNull()) ? null : dateTs.getTime();
				String location = resultSet.getString(10);
				location = (resultSet.wasNull()) ? null : location;
				Event event = new Event(
						resultSet.getInt(2), 
						user, 
						getInvitedUsers(resultSet.getInt(2)),
						resultSet.getString(7), 
						description, 
						date, 
						location);
				
				Timestamp answerDateTs = resultSet.getTimestamp(13);
				Long answerDate = (resultSet.wasNull()) ? null : answerDateTs.getTime();
				Notification notification = new Notification( 
						new EventInvite(
								resultSet.getInt(1), 
								event, 
								invited, 
								EventInvitationState.valueOf(resultSet.getString(11)), 
								resultSet.getBoolean(14),
								resultSet.getTimestamp(12).getTime(), 
								answerDate));
				notifications.add(notification);
			}
			
			// User-hez tartoz� OwnerSeen bitek be�ll�t�sa olvasott �rt�kre
			if (eventInviteIds.size() > 0) {
				sqlMultipleParamsBuilder = new StringBuilder("?");
				for (int i = 0; i < eventInviteIds.size() - 1; i++) {
					sqlMultipleParamsBuilder.append(", ?");
				}
				sqlString = 
						"UPDATE catchupdb.eventinvite "
						+ "SET OwnerSeen = 1 "
						+ "WHERE Id IN (" + sqlMultipleParamsBuilder.toString() + ")";
				statementInvitedUpdate = connection.prepareStatement(sqlString);
				for (int i = 0; i < eventInviteIds.size(); i++) {
					statementInvitedUpdate.setInt(i + 1, eventInviteIds.get(i));
				}
				statementInvitedUpdate.execute();
			}
			
			return new RestResponse<Boolean>(Boolean.TRUE);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeStatement(
					statementOwnerQuery,
					statementOwnerUpdate,
					statementInvitedQuery,
					statementInvitedUpdate);
		}
	}
	
	private RestResponse<Boolean> getFriendRequestNotifications(
			User user, List<Notification> notifications) {
		List<Integer> friendRequestIds = null;
		String sqlString = null;
		PreparedStatement statementRequesterQuery = null;
		PreparedStatement statementRequesterUpdate = null;
		PreparedStatement statementRequestedQuery = null;
		PreparedStatement statementRequestedUpdate = null;
		ResultSet resultSet = null;
		StringBuilder sqlMultipleParamsBuilder = null;
		
		try {
			// �rtes�t�sek lek�r�se, amelyekben a User-t jel�lt�k bar�tnak
			// �s m�g nem v�laszolt r�juk
			sqlString = 
					"SELECT f.Id, u.Id, u.Email, u.FirstName, u.LastName, "
					+ "s.State, f.SendDate, f.RequestedSeen " 
					+ "FROM catchupdb.user u "
					+ "INNER JOIN catchupdb.friend f ON u.Id = f.RequesterId "
				    + "INNER JOIN catchupdb.friendrequeststate s ON f.State = s.Id "
					+ "WHERE f.RequestedId = ? AND s.State = ?";
			statementRequesterQuery = connection.prepareStatement(sqlString);
			statementRequesterQuery.setInt(1, user.getId());
			statementRequesterQuery.setString(2, FriendRequestState.Pending.toString());
			resultSet = statementRequesterQuery.executeQuery();
			
			friendRequestIds = new ArrayList<Integer>();
			while (resultSet.next()) {
				friendRequestIds.add(resultSet.getInt(1));
				User requester = new User(
						resultSet.getInt(2), 
						resultSet.getString(3), 
						resultSet.getString(4), 
						resultSet.getString(5));
				Notification notification = new Notification( 
						new FriendRequest(
								resultSet.getInt(1),
								requester,
								user, 
								FriendRequestState.valueOf(resultSet.getString(6)),
								resultSet.getBoolean(8),
								resultSet.getTimestamp(7).getTime(),
								null));
				notifications.add(notification);
			}
			
			// User-hez tartoz� RequestedSeen bitek be�ll�t�sa olvasott �rt�kre
			if (friendRequestIds.size() > 0) {
				sqlMultipleParamsBuilder = new StringBuilder("?");
				for (int i = 0; i < friendRequestIds.size() - 1; i++) {
					sqlMultipleParamsBuilder.append(", ?");
				}
				sqlString = 
						"UPDATE catchupdb.friend "
						+ "SET RequestedSeen = 1 "
						+ "WHERE Id IN (" + sqlMultipleParamsBuilder.toString() + ")";
				statementRequesterUpdate = connection.prepareStatement(sqlString);
				for (int i = 0; i < friendRequestIds.size(); i++) {
					statementRequesterUpdate.setInt(i + 1, friendRequestIds.get(i));
				}
				statementRequesterUpdate.execute();
			}
			
			// �rtes�t�sek lek�r�se, amelyekben a User jel�lt bar�tnak m�sokat
			// �s azok m�r elfogadt�k vagy elutas�tott�k a jel�l�st
			sqlString = 
					"SELECT f.Id, u.Id, u.Email, u.FirstName, u.LastName, "
					+ "s.State, f.SendDate, f.AnswerDate, f.RequesterSeen " 
					+ "FROM catchupdb.user u "
					+ "INNER JOIN catchupdb.friend f ON u.Id = f.RequestedId "
				    + "INNER JOIN catchupdb.friendrequeststate s ON f.State = s.Id "
					+ "WHERE f.RequesterId = ? AND s.State != ?";
			statementRequestedQuery = connection.prepareStatement(sqlString);
			statementRequestedQuery.setInt(1, user.getId());
			statementRequestedQuery.setString(2, FriendRequestState.Pending.toString());
			resultSet = statementRequestedQuery.executeQuery();
			
			friendRequestIds = new ArrayList<Integer>();
			while (resultSet.next()) {
				friendRequestIds.add(resultSet.getInt(1));
				User requested = new User(
						resultSet.getInt(2), 
						resultSet.getString(3), 
						resultSet.getString(4), 
						resultSet.getString(5));
				Timestamp answerTimeTs = resultSet.getTimestamp(8);
				Long answerTime = (resultSet.wasNull()) ? null : answerTimeTs.getTime();
				Notification notification = new Notification(
						new FriendRequest(
								resultSet.getInt(1),
								user,
								requested, 
								FriendRequestState.valueOf(resultSet.getString(6)),
								resultSet.getBoolean(9),
								resultSet.getTimestamp(7).getTime(),
								answerTime));
				notifications.add(notification);
			}
			
			// User-hez tartoz� RequesterSeen bitek be�ll�t�sa olvasott �rt�kre
			if (friendRequestIds.size() > 0) {
				sqlMultipleParamsBuilder = new StringBuilder("?");
				for (int i = 0; i < friendRequestIds.size() - 1; i++) {
					sqlMultipleParamsBuilder.append(", ?");
				}
				sqlString = 
						"UPDATE catchupdb.friend "
						+ "SET RequesterSeen = 1 "
						+ "WHERE Id IN (" + sqlMultipleParamsBuilder.toString() + ")";
				statementRequestedUpdate = connection.prepareStatement(sqlString);
				for (int i = 0; i < friendRequestIds.size(); i++) {
					statementRequestedUpdate.setInt(i + 1, friendRequestIds.get(i));
				}
				statementRequestedUpdate.execute();
			}
			
			return new RestResponse<Boolean>(Boolean.TRUE);
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeStatement(
					statementRequesterQuery,
					statementRequesterUpdate,
					statementRequestedQuery,
					statementRequestedUpdate);
		}
	}
	
	/**
	 * Bar�t felk�r�s k�ld�se. 
	 * @return RestResponse<Boolean>
	 * 	A felk�r�s sikeress�ge.
	 */
	@POST
	@Path("/SendFriendRequest")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Boolean> sendFriendRequest(SendFriendRequestRequest request) {
		// DB kapcsolat l�trehoz�sa
		Date currentDate = new Date();
		boolean requestSuccess = false;
		String sqlString = null;
		PreparedStatement statementExistingRequest = null;
		PreparedStatement statementUpdateDeclinedRequest = null;
		PreparedStatement statementInsertNewRequest = null;
		PreparedStatement statementQueryTargetUser = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			// Ellen�rizz�k, hogy a h�v� �s h�vott felek l�teznek-e
			connection.setAutoCommit(false);
			RestResponse<Boolean> requesterExists = 
					checkUserExists(request.getUserId());
			RestResponse<Boolean> requestedExists = 
					checkUserExists(request.getRequestedId());
			if (requesterExists.getException() != null || !requesterExists.getResponse()) {
				return requesterExists;
			}
			if (requestedExists.getException() != null || !requesterExists.getResponse()) {
				return requestedExists;
			}
			// Ellen�rizz�k, hogy a h�v� �s h�vott felek k�l�nb�znek
			if (request.getUserId() == request.getRequestedId()) {
				Logger.Log(ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			
			// Lek�rdezz�k, a k�t User k�z�tt m�r megl�v� bar�t felk�r�st
			sqlString = 
					"SELECT f.Id, s.State "
					+ "FROM catchupdb.friend f "
					+ "INNER JOIN catchupdb.friendrequeststate s ON f.State = s.Id "
					+ "WHERE "
						+ "(f.RequesterId = ? AND f.RequestedId = ?) "
						+ "OR "
						+ "(f.RequestedId = ? AND f.RequesterId = ?)";
			statementExistingRequest = connection.prepareStatement(sqlString);
			statementExistingRequest.setInt(1, request.getUserId());
			statementExistingRequest.setInt(2, request.getRequestedId());
			statementExistingRequest.setInt(3, request.getUserId());
			statementExistingRequest.setInt(4, request.getRequestedId());
			ResultSet resultSet = statementExistingRequest.executeQuery();
			
			// Ha van k�z�tt�k felk�r�s bejegyz�s
			if (resultSet.next()) {
				// Ha a felk�r�s Pending, vagy Accepted �llapot�
				// akkor nem lehet elk�ldeni bar�t felk�r�st
				// mivel m�r bar�tok, vagy folyamatban van egy felk�r�s
				String state = resultSet.getString(2);
				if (!state.equals(FriendRequestState.Declined.toString())) {
					Logger.Log("sendFriendRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
					return new RestResponse<Boolean>(Boolean.FALSE);
				}
				// Ha Declined t�pus� a m�r megl�v� felk�r�s, akkor 
				// az �llapot�t megv�ltoztatjuk Pending-re �s a felk�r�s sikeres lesz
				else {
					sqlString = 
							"UPDATE catchupdb.friend "
							+ "SET "
								+ "RequesterId = ?, "
								+ "RequestedId = ?, "
								+ "State = ( "
									+ "SELECT Id "
									+ "FROM catchupdb.friendrequeststate "
									+ "WHERE State = ?), "
								+ "RequesterSeen = 0, " 
								+ "RequestedSeen = 0, "
								+ "SendDate = ?, "
								+ "AnswerDate = NULL "
							+ "WHERE Id = ?";
					statementUpdateDeclinedRequest = 
							connection.prepareStatement(sqlString);
					statementUpdateDeclinedRequest.setInt(1, request.getUserId());
					statementUpdateDeclinedRequest.setInt(2, request.getRequestedId());
					statementUpdateDeclinedRequest.setString(
							3, FriendRequestState.Pending.toString());
					statementUpdateDeclinedRequest.setTimestamp(
							4, new Timestamp(currentDate.getTime()));
					statementUpdateDeclinedRequest.setInt(5, resultSet.getInt(1));
					int updatedColumns = statementUpdateDeclinedRequest.executeUpdate();
					
					if (updatedColumns <= 0) {
						Logger.Log("sendFriendRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
						return new RestResponse<Boolean>(Boolean.FALSE);
					}
					else {
						// Ha az Update sikeres, akkor a jel�l�s is sikeres
						requestSuccess = true;
					}
				}
			}
			// Ha nem volt a k�t User k�z�tt megl�v� felk�r�s bejegyz�s
			// akkor beilleszt�nk egy �jjat az adatb�zisba
			else {
				sqlString = 
						"INSERT INTO catchupdb.friend "
						+ "(RequesterId, RequestedId, State) "
						+ "VALUES(?, ?, "
							+ "(SELECT Id "
					        + "FROM catchupdb.friendrequeststate " 
					        + "WHERE State = ?))";
				statementInsertNewRequest = connection.prepareStatement(sqlString);
				statementInsertNewRequest.setInt(1, request.getUserId());
				statementInsertNewRequest.setInt(2, request.getRequestedId());
				statementInsertNewRequest.setString(
						3, FriendRequestState.Pending.toString());
				int updatedColumns = statementInsertNewRequest.executeUpdate();
				if (updatedColumns <= 0) {
					Logger.Log("sendFriendRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
					return new RestResponse<Boolean>(Boolean.FALSE);
				}
				else {
					// Ha az INSERT siker�lt, akkor a jel�l�s is sikeres volt
					requestSuccess = true;
				}
			}
			
			// V�gezet�l, ha a felk�r�st sikeresen be�rtuk az adatb�zisba
			// akkor elk�ldj�k a sikeres v�laszt 
			if (requestSuccess) {
				// A Requester adatainak lek�r�se a GCM �zenet c�mz�shez
				sqlString = 
						"SELECT Id, Email, FirstName, LastName "
						+ "FROM catchupdb.user WHERE Id = ?";
				statementQueryTargetUser = connection.prepareStatement(sqlString);
				statementQueryTargetUser.setInt(1, request.getUserId());
				resultSet = statementQueryTargetUser.executeQuery();
				
				if (resultSet.next()) {
					// GCM �rtes�t�s k�ld�se ha van a user-hez tartoz� token
					Logger.Log("sendFriendRequest", ApiMessages.GCM_MESSAGE_PREPARING);
					String token = getGcmToken(request.getRequestedId());
					if (token != null) {
						User user = new User(
								resultSet.getInt(1),
								resultSet.getString(2),
								resultSet.getString(3),
								resultSet.getString(4));
						GcmSender.notifyFriendRequest(
								token, user, FriendRequestState.Pending);
					}
				}
				
				connection.commit();
				connection.setAutoCommit(true);
				
				Logger.Log("sendFriendRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.TRUE);
			}
			else {
				connection.commit();
				connection.setAutoCommit(true);
				Logger.Log("sendFriendRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(
					statementExistingRequest,
					statementInsertNewRequest,
					statementQueryTargetUser,
					statementUpdateDeclinedRequest);
		}
	}
	
	/**
	 * Megh�v�s esem�nyre. 
	 * @return RestResponse<Boolean>
	 * 	A megh�v�s sikeress�ge.
	 */
	@POST
	@Path("/SendEventInvite")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Boolean> sendEventInvite(SendEventInviteRequest request) {
		// DB kapcsolat l�trehoz�sa
		Date currentDate = new Date();
		String eventTitle = "";
		boolean requestSuccess = false;
		String sqlString = null;
		PreparedStatement statementExistingEvent = null;
		PreparedStatement statementExistingInvite = null;
		PreparedStatement statementUpdateNotGoingInvite = null;
		PreparedStatement statementInsertNewInvite = null;
		PreparedStatement statementQueryTargetUser = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			// Ellen�rizz�k, hogy a h�v� �s h�vott felek l�teznek-e
			connection.setAutoCommit(false);
			RestResponse<Boolean> ownerExists = 
					checkUserExists(request.getUserId());
			RestResponse<Boolean> invitedExists = 
					checkUserExists(request.getInvitedId());
			if (ownerExists.getException() != null || !ownerExists.getResponse()) {
				return ownerExists;
			}
			if (invitedExists.getException() != null || !invitedExists.getResponse()) {
				return invitedExists;
			}
			// Ellen�rizz�k, hogy a h�v� �s h�vott felek k�l�nb�znek
			if (request.getUserId() == request.getInvitedId()) {
				Logger.Log(ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			
			// Ellen�rizz�k, hogy a megh�v�nak val�ban van-e ilyen esem�nye
			sqlString =
					"SELECT Id, Title "
					+ "FROM catchupdb.event "
					+ "WHERE Id = ? AND OwnerId = ?";
			statementExistingEvent = connection.prepareStatement(sqlString);
			statementExistingEvent.setInt(1, request.getEventId());
			statementExistingEvent.setInt(2, request.getUserId());
			ResultSet resultSet = statementExistingEvent.executeQuery();
			
			if (resultSet.next()) {
				eventTitle = resultSet.getString(2);
			}
			else {
				Logger.Log("sendEventInviteRequest", 
						ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			
			// Lek�rdezz�k, a m�r megl�v� esem�ny megh�v�st
			sqlString = 
					"SELECT i.Id, s.State "
					+ "FROM catchupdb.eventinvite i "
					+ "INNER JOIN catchupdb.event e ON i.EventId = e.Id "
					+ "INNER JOIN catchupdb.eventinvitationstate s ON i.State = s.Id "
					+ "WHERE e.OwnerId = ?  AND e.Id = ? AND i.InvitedId = ?";
			statementExistingInvite = connection.prepareStatement(sqlString);
			statementExistingInvite.setInt(1, request.getUserId());
			statementExistingInvite.setInt(2, request.getEventId());
			statementExistingInvite.setInt(3, request.getInvitedId());
			resultSet = statementExistingInvite.executeQuery();
			
			// Ha volt esem�ny megh�v�s
			if (resultSet.next()) {
				// Ha a megl�v� megh�v�s nem "NotGoing" �llapot�
				// akkor nem lehet elk�ldeni a megh�v�st, mivel
				// a megh�vott f�l m�r elfogadta/m�g nem v�laszolt a megh�v�sra
				String state = resultSet.getString(2);
				if (!state.equals(EventInvitationState.NotGoing.toString())) {
					Logger.Log("sendEventInviteRequest", 
							ApiMessages.API_REQUEST_SUCCESSFUL);
					return new RestResponse<Boolean>(Boolean.FALSE);
				}
				// Ha NotGoing t�pus� a m�r megl�v� megh�v�s, akkor
				// az �llapot�t megv�ltoztatjuk Pending-re �s a megh�v�s sikeres lesz
				else {
					sqlString = 
							"UPDATE catchupdb.eventinvite "
							+ "SET "
								+ "State = ( "
									+ "SELECT Id "
							        + "FROM catchupdb.eventinvitationstate "
							        + "WHERE State = ?), "
								+ "InvitedSeen = 0, "
							    + "OwnerSeen = 0, "
							    + "SendDate = ?, "
							    + "AnswerDate = null "
							+ "WHERE Id = ?";
					statementUpdateNotGoingInvite = 
							connection.prepareStatement(sqlString);
					statementUpdateNotGoingInvite.setString(
							1, EventInvitationState.Pending.toString());
					statementUpdateNotGoingInvite.setTimestamp(
							2, new Timestamp(currentDate.getTime()));
					statementUpdateNotGoingInvite.setInt(3, resultSet.getInt(1));
					int updatedColumns = statementUpdateNotGoingInvite.executeUpdate();
					
					if (updatedColumns <= 0) {
						Logger.Log("sendEventInviteRequest", 
								ApiMessages.API_REQUEST_SUCCESSFUL);
						return new RestResponse<Boolean>(Boolean.FALSE);
					}
					else {
						// Ha az Update sikeres, akkor a megh�v�s is sikeres
						requestSuccess = true;
					}
				}
			}
			// Ha a User-t m�g nem h�vt�k meg el�z�leg az esem�nyre
			// akkor beilleszt�nk egy �j megh�v�st az adatb�zisba
			else {
				sqlString = 
						"INSERT INTO catchupdb.eventinvite "
						+ "(EventId, InvitedId) "
						+ "VALUES(?, ?)";
				statementInsertNewInvite = connection.prepareStatement(sqlString);
				statementInsertNewInvite.setInt(1, request.getEventId());
				statementInsertNewInvite.setInt(2, request.getInvitedId());
				int updatedColumns = statementInsertNewInvite.executeUpdate();
				
				if (updatedColumns <= 0) {
					Logger.Log("sendEventInviteRequest", 
							ApiMessages.API_REQUEST_SUCCESSFUL);
					return new RestResponse<Boolean>(Boolean.FALSE);
				}
				else {
					// Ha az INSERT siker�lt, akkor a megh�v�s is sikeres volt
					requestSuccess = true;
				}
			}
			
			// V�gezet�l, ha a megh�v�st sikeresen be�rtuk az adatb�zisba
			// akkor elk�ldj�k a sikeres v�laszt 
			if (requestSuccess) {
				// Az Owner User adatainak lek�r�se a GCM �zenet c�mz�shez
				sqlString = 
						"SELECT Id, Email, FirstName, LastName "
						+ "FROM catchupdb.user WHERE Id = ?";
				statementQueryTargetUser = connection.prepareStatement(sqlString);
				statementQueryTargetUser.setInt(1, request.getUserId());
				resultSet = statementQueryTargetUser.executeQuery();
				
				if (resultSet.next()) {
					// GCM �rtes�t�s k�ld�se ha van a user-hez tartoz� token
					String token = getGcmToken(request.getInvitedId());
					if (token != null) {
						User user = new User(
								resultSet.getInt(1),
								resultSet.getString(2),
								resultSet.getString(3),
								resultSet.getString(4));
						GcmSender.notifyEventInvite(
								token, 
								user, 
								EventInvitationState.Pending,
								eventTitle);
					}
				}
				
				connection.commit();
				connection.setAutoCommit(true);
				
				Logger.Log("sendEventInviteRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.TRUE);
			}
			else {
				connection.commit();
				connection.setAutoCommit(true);
				Logger.Log("sendEventInviteRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(
					statementExistingEvent,
					statementExistingInvite,
					statementInsertNewInvite,
					statementQueryTargetUser,
					statementUpdateNotGoingInvite);
		}
	}
	
	/**
	 * Bar�t felk�r�s elfogad�sa. 
	 * @return RestResponse<Boolean>
	 * 	Az elfogad�s sikeress�ge.
	 */
	@POST
	@Path("/AcceptFriendRequest")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Boolean> acceptFriendRequest(AnswerFriendRequestRequest request) {
		return answerFriendRequest(
				request.getRequesterId(), 
				request.getUserId(), 
				FriendRequestState.Accepted);
	}
	
	/**
	 * Bar�t felk�r�s elutas�t�sa. 
	 * @return RestResponse<Boolean>
	 * 	Az elutas�t�sa sikeress�ge.
	 */
	@POST
	@Path("/DeclineFriendRequest")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Boolean> declineFriendRequest(AnswerFriendRequestRequest request) {
		return answerFriendRequest(
				request.getRequesterId(), 
				request.getUserId(), 
				FriendRequestState.Declined);
	}
	
	/**
	 * Esem�ny megh�v�sra v�laszol�s. 
	 * @return RestResponse<Boolean>
	 * 	Az v�laszol�s sikeress�ge.
	 */
	@POST
	@Path("/AnswerEventInvite")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<Boolean> answerEventInvite(AnswerEventInviteRequest request) {
		// DB kapcsolat l�trehoz�sa
		Date currentDate = new Date();
		String eventTitle = "";
		User user;
		String sqlString = null;
		PreparedStatement statementQueryEvent = null;
		PreparedStatement statementQueryInvitedUser = null;
		PreparedStatement statementUpdateEventInvite = null;
		PreparedStatement statementQueryTargetUser = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			// Ellen�rizz�k, hogy a User l�tezik-e
			connection.setAutoCommit(false);
			RestResponse<Boolean> userExists = checkUserExists(request.getUserId());
			if (userExists.getException() != null || !userExists.getResponse()) {
				return userExists;
			}
			
			// Ellen�rizz�k, hogy l�tezik-e a hivatkozott esem�ny
			sqlString =
					"SELECT Title "
					+ "FROM catchupdb.event "
					+ "WHERE Id = ?";
			statementQueryEvent = connection.prepareStatement(sqlString);
			statementQueryEvent.setInt(1, request.getEventId());
			ResultSet resultSet = statementQueryEvent.executeQuery();
			
			if (resultSet.next()) {
				eventTitle = resultSet.getString(1);
			}
			else {
				Logger.Log("answerEventInvite", 
						ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			
			// Ellen�rizz�k, hogy a v�lasz nem "Pending"
			if (request.getAnswer() == EventInvitationState.Pending) {
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			
			// Lek�rj�k a megh�vott User adatait
			sqlString = 
					"SELECT Id, Email, FirstName, LastName "
					+ "FROM catchupdb.user "
					+ "WHERE Id = ?";
			statementQueryInvitedUser = connection.prepareStatement(sqlString);
			statementQueryInvitedUser.setInt(1, request.getUserId());
			resultSet = statementQueryInvitedUser.executeQuery();
			
			if (resultSet.next()) {
				user = new User(
						resultSet.getInt(1),
						resultSet.getString(2),
						resultSet.getString(3),
						resultSet.getString(4));
			}
			else {
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			
			// Megpr�b�ljuk Updatelni a hivatkozott esem�ny megh�v�st az adatb�zisban
			sqlString = 
					"UPDATE catchupdb.eventinvite "
					+ "SET "
						+ "State = ( "
							+ "SELECT Id "
					        + "FROM catchupdb.eventinvitationstate "
					        + "WHERE State = ?), "
					    + "AnswerDate = ? "
					+ "WHERE "
						+ "EventId = ? "
					    + "AND InvitedId = ? "
					    + "AND State = ( "
							+ "SELECT Id "
					        + "FROM catchupdb.eventinvitationstate "
					        + "WHERE State = ?)";
			statementUpdateEventInvite = connection.prepareStatement(sqlString);
			statementUpdateEventInvite.setString(1, request.getAnswer().toString());
			statementUpdateEventInvite.setTimestamp(
					2, new Timestamp(currentDate.getTime()));
			statementUpdateEventInvite.setInt(3, request.getEventId());
			statementUpdateEventInvite.setInt(4, request.getUserId());
			statementUpdateEventInvite.setString(
					5, EventInvitationState.Pending.toString());
			int updatedColumns = statementUpdateEventInvite.executeUpdate();
			
			// Ha a m�dos�tott rekordok sz�ma nagyobb, mint nulla, akkor a
			// v�laszol�s sikeres volt, k�l�nben sikertelen
			if (updatedColumns <= 0) {
				connection.commit();
				connection.setAutoCommit(true);
				Logger.Log("answerEventInvite", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			else {
				// Az �rtes�tend�(Esem�ny Owner) azonos�t�j�nak lek�r�se 
				// a GCM �zenet c�mz�shez
				sqlString = 
						"SELECT u.Id "
						+ "FROM catchupdb.user u "
						+ "INNER JOIN catchupdb.event e ON u.Id = e.OwnerId "
						+ "WHERE e.Id = ?";
				statementQueryTargetUser = connection.prepareStatement(sqlString);
				statementQueryTargetUser.setInt(1, request.getEventId());
				resultSet = statementQueryTargetUser.executeQuery();
				
				if (resultSet.next()) {
					// GCM �rtes�t�s k�ld�se ha van a user-hez tartoz� token
					Logger.Log("answerEventInvite", ApiMessages.GCM_MESSAGE_PREPARING);
					String token = getGcmToken(resultSet.getInt(1));
					if (!resultSet.wasNull()) {
						GcmSender.notifyEventInvite(
								token, user, request.getAnswer(), eventTitle);
					}
				}
				
				connection.commit();
				connection.setAutoCommit(true);
				Logger.Log("answerEventInvite", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.TRUE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statementUpdateEventInvite, statementQueryTargetUser);
		}
	}
	
	/**
	 * Bar�t felk�r�s elfogad�sa/elutas�t�sa. 
	 * @return RestResponse<Boolean>
	 * 	Az elfogad�s/elutas�t�s sikeress�ge.
	 */
	private RestResponse<Boolean> answerFriendRequest(int requesterId, int requestedId, FriendRequestState answer) {
		// DB kapcsolat l�trehoz�sa
		Date currentDate = new Date();
		String sqlString = null;
		PreparedStatement statementUpdateFriendRequest = null;
		PreparedStatement statementQueryTargetUser = null;
		
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			// Ellen�rizz�k, hogy a User l�tezik-e
			connection.setAutoCommit(false);
			RestResponse<Boolean> requesterExists = checkUserExists(requesterId);
			RestResponse<Boolean> requestedExists = checkUserExists(requestedId);
			if (requesterExists.getException() != null || !requesterExists.getResponse()) {
				return requesterExists;
			}
			if (requestedExists.getException() != null || !requestedExists.getResponse()) {
				return requestedExists;
			}
			
			// Megpr�b�ljuk Updatelni a hivatkozott bar�t felk�r�st az adatb�zisban
			sqlString = 
					"UPDATE catchupdb.friend "
					+ "SET State = "
						+ "(SELECT Id "
						+ "FROM catchupdb.friendrequeststate "
						+ "WHERE State = ?), "
						+ "AnswerDate = ? "
					+ "WHERE RequesterId = ? " 
					+ "AND RequestedId = ? "
					+ "AND State = "
						+ "(SELECT Id "
						+ "FROM catchupdb.friendrequeststate "
						+ "WHERE State = ?)";
			statementUpdateFriendRequest = connection.prepareStatement(sqlString);
			statementUpdateFriendRequest.setString(1, answer.toString());
			statementUpdateFriendRequest.setTimestamp(
					2, new Timestamp(currentDate.getTime()));
			statementUpdateFriendRequest.setInt(3, requesterId);
			statementUpdateFriendRequest.setInt(4, requestedId);
			statementUpdateFriendRequest.setString(
					5, FriendRequestState.Pending.toString());
			int updatedColumns = statementUpdateFriendRequest.executeUpdate();
			
			// Ha a m�dos�tott rekordok sz�ma nagyobb, mint nulla, akkor az
			// elfogad�s/elutas�t�s sikeres volt, k�l�nben sikertelen
			if (updatedColumns <= 0) {
				connection.commit();
				connection.setAutoCommit(true);
				Logger.Log("answerFriendRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.FALSE);
			}
			else {
				// Az Requested User adatainak lek�r�se a GCM �zenet c�mz�shez
				sqlString = 
						"SELECT Id, Email, FirstName, LastName "
						+ "FROM catchupdb.user WHERE Id = ?";
				statementQueryTargetUser = connection.prepareStatement(sqlString);
				statementQueryTargetUser.setInt(1, requestedId);
				ResultSet resultSet = statementQueryTargetUser.executeQuery();
				
				if (resultSet.next()) {
					// GCM �rtes�t�s k�ld�se ha van a user-hez tartoz� token
					Logger.Log("answerFriendRequest", ApiMessages.GCM_MESSAGE_PREPARING);
					String token = getGcmToken(requesterId);
					if (token != null) {
						User user = new User(
								resultSet.getInt(1),
								resultSet.getString(2),
								resultSet.getString(3),
								resultSet.getString(4));
						GcmSender.notifyFriendRequest(
								token, user, answer);
						System.out.println("GCM ELKULDVE!!!");;
					}
				}
				
				connection.commit();
				connection.setAutoCommit(true);
				Logger.Log("answerFriendRequest", ApiMessages.API_REQUEST_SUCCESSFUL);
				return new RestResponse<Boolean>(Boolean.TRUE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statementUpdateFriendRequest, statementQueryTargetUser);
		}
	}
	
	/**
	 * User lek�rdez�se. 
	 * @return RestResponse<User>
	 * 	A User objektum.
	 */
	@POST
	@Path("/GetUser")
	@Produces(MediaType.APPLICATION_JSON)
	public RestResponse<User> getUser(AuthRequest request) {
		// DB kapcsolat l�trehoz�sa
		PreparedStatement statementQueryUser = null;
		connection = CreateDbConnection();
		if (connection == null) {
			return new RestResponse<User>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_CONNECTING_TO_DATABASE));
		}
		
		try {
			String sqlString = 
					"SELECT Id, Email, FirstName, LastName "
					+ " FROM catchupdb.user "
					+ "WHERE Id = ?";
			statementQueryUser = connection.prepareStatement(sqlString);
			statementQueryUser.setInt(1, request.getUserId());
			ResultSet resultSet = statementQueryUser.executeQuery();
			
			if (resultSet.next()) {
				return new RestResponse<User>(new User(
						resultSet.getInt(1),
						resultSet.getString(2),
						resultSet.getString(3),
						resultSet.getString(4)));
			}
			else {
				Logger.Log(ApiMessages.NON_EXISTENT_USER);
				return new RestResponse<User>(
						new ApiException(
								ApiExceptionType.NonexistentUserId, 
								ApiMessages.NON_EXISTENT_USER));
			}
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<User>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeDb(statementQueryUser);
		}
	}
	
	// Visszaadja, hogy a megadott userId szerepel-e az adatb�zisban.
	private RestResponse<Boolean> checkUserExists(int id) {
		PreparedStatement statement = null;
		try {
			String userQueryString = "SELECT Id FROM catchupdb.user WHERE Id = ?";
			statement = connection.prepareStatement(userQueryString);
			statement.setInt(1, id);
			ResultSet resultSet = statement.executeQuery();
			
			if (resultSet.next()) {
				return new RestResponse<Boolean>(Boolean.TRUE);
			}
			else {
				Logger.Log(ApiMessages.NON_EXISTENT_USER);
				return new RestResponse<Boolean>(
						new ApiException(
								ApiExceptionType.NonexistentUserId, 
								ApiMessages.NON_EXISTENT_USER));
			}
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return new RestResponse<Boolean>(
					new ApiException(
							ApiExceptionType.Database, 
							ApiMessages.ERROR_DB));
		} finally {
			closeStatement(statement);
		}
	}
	
	// Visszaadja, a felhaszn�l�hoz tartoz� GCM tokent
	// Ha nincs hozz� tartoz� token akkor null-t ad vissza.
	private String getGcmToken(int userId) {
		PreparedStatement statement = null;
		try {
			String gcmQueryString = 
					"SELECT GcmRegistrationToken "
					+ "FROM catchupdb.user "
					+ "WHERE Id = ?";
			statement = connection.prepareStatement(gcmQueryString);
			statement.setInt(1, userId);
			ResultSet resultSet = statement.executeQuery();
			
			if (resultSet.next()) {
				String token = resultSet.getString(1);
				if (resultSet.wasNull()) {
					token = null;
				}
				return token;
			}
			else {
				return null;
			}
		} catch (SQLException e) {
			Logger.Log(ApiMessages.ERROR_DB);
			return null;
		} finally {
			closeStatement(statement);
		}
	}
} 