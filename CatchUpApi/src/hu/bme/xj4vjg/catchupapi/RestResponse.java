package hu.bme.xj4vjg.catchupapi;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RestResponse<T> {
	private T response;
	private ApiException exception;
	
	public RestResponse() { }
	
	public RestResponse(T response, ApiException exception) {
		this.response = response;
		this.exception = exception;
	}
	
	public RestResponse(T response) {
		this.response = response;
		this.exception = null;
	}
	
	public RestResponse(ApiException exception) {
		this.response = null;
		this.exception = exception;
	}

	public T getResponse() {
		return response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	public ApiException getException() {
		return exception;
	}

	public void setException(ApiException exception) {
		this.exception = exception;
	}
}
