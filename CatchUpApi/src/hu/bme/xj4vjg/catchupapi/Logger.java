package hu.bme.xj4vjg.catchupapi;

/**
 * Napl�z� oszt�ly a szerver tev�kenys�geinek nyomomk�vet�s�re.
 */
public class Logger {
	public static void Log(String message) {
		System.out.println(message);
	}
	
	public static void Log(String source, String message) {
		System.out.println(source + ": " + message);
	}
}
