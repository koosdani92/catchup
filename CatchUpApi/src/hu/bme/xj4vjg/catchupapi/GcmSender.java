package hu.bme.xj4vjg.catchupapi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;

import hu.bme.xj4vjg.catchupapi.param.EventInvitationState;
import hu.bme.xj4vjg.catchupapi.param.FriendRequestState;
import hu.bme.xj4vjg.catchupapi.param.NotificationType;
import hu.bme.xj4vjg.catchupapi.param.User;

public class GcmSender {
	private static final String NOTIFICATION_TYPE = "NotificationType";
	private static final String FRIEND_REQUEST_STATE = "FriendRequestState";
	private static final String EVENT_INVITATION_STATE = "EventInvitationState";
	private static final String EVENT_TITLE = "EventTitle";
	private static final String GCM_API_KEY = "AIzaSyCZss3pCiVcXAmEK0VLhb6K9vcFx-3PtHA";
	private static final int GCM_RETRIES = 5;
	
	public static boolean notifyFriendRequest(String token, User source, FriendRequestState state) {
		Map<String, String> data = new HashMap<String, String>();
		data.put(NOTIFICATION_TYPE, NotificationType.FriendRequest.toString());
		data.put(User.ID_KEY, Integer.toString(source.getId()));
		data.put(User.EMAIL_KEY, source.getEmail());
		data.put(User.FIRST_NAME_KEY, source.getFirstName());
		data.put(User.LAST_NAME_KEY, source.getLastName());
		data.put(FRIEND_REQUEST_STATE, state.toString());
		try {
			sendGcmMessage(token, data);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean notifyEventInvite(String token, User source, 
			EventInvitationState state, String title) {
		Map<String, String> data = new HashMap<String, String>();
		data.put(NOTIFICATION_TYPE, NotificationType.EventInvite.toString());
		data.put(EVENT_TITLE, title);
		data.put(User.ID_KEY, Integer.toString(source.getId()));
		data.put(User.EMAIL_KEY, source.getEmail());
		data.put(User.FIRST_NAME_KEY, source.getFirstName());
		data.put(User.LAST_NAME_KEY, source.getLastName());
		data.put(EVENT_INVITATION_STATE, state.toString());
		
		try {
			sendGcmMessage(token, data);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	private static void sendGcmMessage(String regId, Map<String, String> data) 
			throws IOException, InvalidRequestException {
		try {
			Sender sender = new Sender(GCM_API_KEY);
			Message.Builder builder = new Message.Builder();
			for (String key : data.keySet()) {
				builder.addData(key, data.get(key));
			}
			sender.send(builder.build(), regId, GCM_RETRIES);
			Logger.Log(ApiMessages.GCM_MESSAGE_SENT);
		} catch (Exception e) {
			Logger.Log(ApiMessages.ERROR_SENDING_GCM_MESSAGE);
			throw e;
		}
	}
}
