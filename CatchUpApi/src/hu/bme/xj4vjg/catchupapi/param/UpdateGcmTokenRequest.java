package hu.bme.xj4vjg.catchupapi.param;

public class UpdateGcmTokenRequest extends AuthRequest {
	public String gcmToken;

	public UpdateGcmTokenRequest() {}
	
	public UpdateGcmTokenRequest(int userId, String gcmToken) {
		super(userId);
		this.gcmToken = gcmToken;
	}

	public String getGcmToken() {
		return gcmToken;
	}

	public void setGcmToken(String gcmToken) {
		this.gcmToken = gcmToken;
	}
}
