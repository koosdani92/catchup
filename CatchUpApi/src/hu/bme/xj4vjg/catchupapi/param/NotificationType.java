package hu.bme.xj4vjg.catchupapi.param;

public enum NotificationType {
	FriendRequest, EventInvite
}
