package hu.bme.xj4vjg.catchupapi.param;

public class InvitedUser {
	private User user;
	private EventInvitationState invitationState;
	
	public InvitedUser() { }
	
	public InvitedUser(User user, EventInvitationState invitationState) {
		this.user = user;
		this.invitationState = invitationState;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EventInvitationState getInvitationState() {
		return invitationState;
	}

	public void setInvitationState(EventInvitationState invitationState) {
		this.invitationState = invitationState;
	}
}
