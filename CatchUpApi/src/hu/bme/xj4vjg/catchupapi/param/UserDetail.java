package hu.bme.xj4vjg.catchupapi.param;

public class UserDetail {
	private User user;
	private boolean friend;
	private boolean pendingFriend;
	
	public UserDetail() { }
	
	public UserDetail(User user, boolean friend, boolean pendingFriend) {
		this.user = user;
		this.friend = friend;
		this.pendingFriend = pendingFriend;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isFriend() {
		return friend;
	}

	public void setFriend(boolean friend) {
		this.friend = friend;
	}

	public boolean isPendingFriend() {
		return pendingFriend;
	}

	public void setPendingFriend(boolean pendingFriend) {
		this.pendingFriend = pendingFriend;
	}
}
