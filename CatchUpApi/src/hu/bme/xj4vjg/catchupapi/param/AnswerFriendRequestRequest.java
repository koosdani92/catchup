package hu.bme.xj4vjg.catchupapi.param;

public class AnswerFriendRequestRequest extends AuthRequest {
	private int requesterId;
	
	public AnswerFriendRequestRequest(){ }
	
	public AnswerFriendRequestRequest(int userId, int requesterId) {
		super(userId);
		this.requesterId = requesterId;
	}

	public int getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(int requesterId) {
		this.requesterId = requesterId;
	}
}
