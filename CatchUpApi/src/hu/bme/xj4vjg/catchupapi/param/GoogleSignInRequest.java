package hu.bme.xj4vjg.catchupapi.param;

/**
 * A googleSignIn API h�v�s param�ter le�r�ja.
 */
public class GoogleSignInRequest {
	public String googleId;
	public String email;
	public String firstName;
	public String lastName;

	public GoogleSignInRequest() {}
	
	public GoogleSignInRequest(String googleId, String email, String firstName, String lastName) {
		this.googleId = googleId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}