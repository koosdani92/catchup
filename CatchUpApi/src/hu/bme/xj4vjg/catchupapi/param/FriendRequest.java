package hu.bme.xj4vjg.catchupapi.param;

import java.util.Date;

public class FriendRequest {
	private int id;
	private User requester;
	private User requested;
	private FriendRequestState state;
	private boolean seen;
	private Long sendDate;
	private Long answerDate;
	
	public FriendRequest() { }
	
	public FriendRequest(int requestId, User requester, 
			User requested, FriendRequestState state, boolean seen,
			Long sendDate, Long answerDate) {
		this.id = requestId;
		this.requester = requester;
		this.requested = requested;
		this.state = state;
		this.seen = seen;
		this.sendDate = sendDate;
		this.answerDate = answerDate;
	}
	
	public FriendRequest(int requestId, User requester, 
			User requested, FriendRequestState state,
			Long sendDate, Long answerDate) {
		this.id = requestId;
		this.requester = requester;
		this.requested = requested;
		this.state = state;
		this.seen = true;
		this.sendDate = sendDate;
		this.answerDate = answerDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getRequester() {
		return requester;
	}

	public void setRequester(User requester) {
		this.requester = requester;
	}

	public User getRequested() {
		return requested;
	}

	public void setRequested(User requested) {
		this.requested = requested;
	}

	public FriendRequestState getState() {
		return state;
	}

	public void setState(FriendRequestState state) {
		this.state = state;
	}
	
	public boolean getSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	
	public Long getSendDate() {
		return sendDate;
	}

	public void setSendDate(Long sendDate) {
		this.sendDate = sendDate;
	}

	public Long getAnswerDate() {
		return answerDate;
	}

	public void setAnswerDate(Long answerDate) {
		this.answerDate = answerDate;
	}
}
