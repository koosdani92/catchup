package hu.bme.xj4vjg.catchupapi.param;

public class EventInvite {
	private int id;
	private Event event;
	private User invited;
	private EventInvitationState state;
	private boolean seen;
	private Long sendDate;
	private Long answerDate;
	
	public EventInvite() { }
	
	public EventInvite(int id, Event event, User invited, EventInvitationState state,
			boolean seen, Long sendDate, Long answerDate) {
		this.id = id;
		this.event = event;
		this.invited = invited;
		this.state = state;
		this.seen = seen;
		this.sendDate = sendDate;
		this.answerDate = answerDate;
	}
	
	public EventInvite(int id, Event event, User invited, EventInvitationState state,
			Long sendDate, Long answerDate) {
		this.id = id;
		this.event = event;
		this.invited = invited;
		this.state = state;
		this.seen = true;
		this.sendDate = sendDate;
		this.answerDate = answerDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public User getInvited() {
		return invited;
	}

	public void setInvited(User invited) {
		this.invited = invited;
	}

	public EventInvitationState getState() {
		return state;
	}

	public void setState(EventInvitationState state) {
		this.state = state;
	}
	
	public boolean getSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	public Long getSendDate() {
		return sendDate;
	}

	public void setSendDate(Long sendDate) {
		this.sendDate = sendDate;
	}

	public Long getAnswerDate() {
		return answerDate;
	}

	public void setAnswerDate(Long answerDate) {
		this.answerDate = answerDate;
	}	
}
