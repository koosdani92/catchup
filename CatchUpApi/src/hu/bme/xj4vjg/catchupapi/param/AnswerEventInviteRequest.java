package hu.bme.xj4vjg.catchupapi.param;

public class AnswerEventInviteRequest extends AuthRequest {
	private int eventId;
	private EventInvitationState answer;
	
	public AnswerEventInviteRequest(){ }
	
	public AnswerEventInviteRequest(int userId, int eventId, EventInvitationState answer) {
		super(userId);
		this.eventId = eventId;
		this.answer = answer;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	public EventInvitationState getAnswer() {
		return answer;
	}

	public void setAnswer(EventInvitationState answer) {
		this.answer = answer;
	}
}
