package hu.bme.xj4vjg.catchupapi.param;

import java.util.List;

public class Event {
	private int id;
	private User owner;
	private List<InvitedUser> invitedUsers;
	private String title;
	private String description;
	private Long date;
	private String location;
	
	public Event() { }
	public Event(int id, User owner, List<InvitedUser> invitedUsers, String title, 
			String description, Long date, String location) {
		this.id = id;
		this.owner = owner;
		this.invitedUsers = invitedUsers;
		this.title = title;
		this.description = description;
		this.date = date;
		this.location = location;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public List<InvitedUser> getInvitedUsers() {
		return invitedUsers;
	}
	public void setInvitedUsers(List<InvitedUser> invitedUsers) {
		this.invitedUsers = invitedUsers;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
