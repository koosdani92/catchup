package hu.bme.xj4vjg.catchupapi.param;

public enum FriendRequestState {
	Pending, Accepted, Declined;
	
	public static FriendRequestState getValueFromId(int id) {
		switch (id) {
		case 1:
			return FriendRequestState.Pending;
		case 2:
			return FriendRequestState.Accepted;
		case 3:
			return FriendRequestState.Declined;
		default:
			return null;
		}
	}
}
