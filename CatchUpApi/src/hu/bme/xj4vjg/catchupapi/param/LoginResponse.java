package hu.bme.xj4vjg.catchupapi.param;

/**
 * A bejelentkez�st v�gz� API h�v�sok visszat�r�si �rt�ke.
 */
public class LoginResponse {
	public boolean loginResult;
	public int userId;
	public User user;

	public LoginResponse() {}

	public LoginResponse(boolean loginResult) {
		this(loginResult, -1, null);
	}
	
	public LoginResponse(boolean loginResult, int userId) {
		this(loginResult, -1, null);
	}
	
	public LoginResponse(boolean loginResult, int userId, User user) {
		this.loginResult = loginResult;
		this.userId = userId;
		this.user = user;
	}
	
	public boolean getLoginResult() {
		return loginResult;
	}

	public void setLoginResult(boolean loginResult) {
		this.loginResult = loginResult;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}