package hu.bme.xj4vjg.catchupapi.param;

public class AuthRequest {
	protected int userId;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public AuthRequest() { }
	
	public AuthRequest(int userId) {
		this.userId = userId;
	}
}
