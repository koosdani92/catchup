package hu.bme.xj4vjg.catchupapi.param;

/**
 * A facebookSignIn API h�v�s param�ter le�r�ja.
 */
public class FacebookSignInRequest {
	public String facebookId;
	public String email;
	public String firstName;
	public String lastName;

	public FacebookSignInRequest() {}
	
	public FacebookSignInRequest(String facebookId, String email, String firstName, String lastName) {
		this.facebookId = facebookId;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFacebbokId() {
		return facebookId;
	}

	public void setFacebbokId(String facebbokId) {
		this.facebookId = facebbokId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}