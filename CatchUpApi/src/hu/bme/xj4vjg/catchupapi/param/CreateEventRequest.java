package hu.bme.xj4vjg.catchupapi.param;

public class CreateEventRequest extends AuthRequest{
	private int ownerId;
	private String title;
	private String description;
	private Long date;
	private String location;
	
	public CreateEventRequest() { }
	public CreateEventRequest(int userId, int ownerId, String title, 
			String description, Long date, String location) {
		super(userId);
		this.ownerId = ownerId;
		this.title = title;
		this.description = description;
		this.date = date;
		this.location = location;
	}
	
	public int getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
