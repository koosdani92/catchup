package hu.bme.xj4vjg.catchupapi.param;

public enum EventInvitationState {
	Pending, Going, NotGoing;
	
	public static EventInvitationState getValueFromId(int id) {
		switch (id) {
		case 1:
			return EventInvitationState.Pending;
		case 2:
			return EventInvitationState.Going;
		case 3:
			return EventInvitationState.NotGoing;
		default:
			return null;
		}
	}
}
