package hu.bme.xj4vjg.catchupapi.param;

public class SendFriendRequestRequest extends AuthRequest {
	private int requestedId;
	
	public SendFriendRequestRequest() { }
	
	public SendFriendRequestRequest(int userId, int requestedId) {
		super(userId);
		this.requestedId = requestedId;
	}

	public int getRequestedId() {
		return requestedId;
	}

	public void setRequestedId(int requestedId) {
		this.requestedId = requestedId;
	}
}
