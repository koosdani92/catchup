package hu.bme.xj4vjg.catchupapi.param;

public class Notification {
	private NotificationType type;
	private FriendRequest friendRequest;
	private EventInvite eventInvite;
	
	public Notification() { }
	
	public Notification(FriendRequest friendRequest) {
		this.type = NotificationType.FriendRequest;
		this.friendRequest = friendRequest;
		this.eventInvite = null;
	}
	
	public Notification(EventInvite eventInvite) {
		this.type = NotificationType.EventInvite;
		this.friendRequest = null;
		this.eventInvite = eventInvite;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public FriendRequest getFriendRequest() {
		return friendRequest;
	}

	public void setFriendRequest(FriendRequest friendRequest) {
		this.friendRequest = friendRequest;
	}
	
	public EventInvite getEventInvite() {
		return eventInvite;
	}

	public void setEventInvite(EventInvite eventInvite) {
		this.eventInvite = eventInvite;
	}
}
