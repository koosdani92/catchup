package hu.bme.xj4vjg.catchup.apihelper;

import java.util.List;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.AuthRequest;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetNewNotificationCountTask extends AbstractApiTask {
    public interface GetNewNotificationCountListener  {
        void onGetNewNotificationCountFinished(Integer response, ApiException exception);
    }

    private GetNewNotificationCountListener  listener;

    public GetNewNotificationCountTask (GetNewNotificationCountListener listener) {
        this.listener = listener;
    }

    public void run(int userId) {
        Callback<RestResponse<Integer>> callback = new Callback<RestResponse<Integer>>() {
            @Override
            public void success(RestResponse<Integer> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.getNewNotificationCount(new AuthRequest(userId), callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(Integer response, ApiException exception) {
        if (listener != null) {
            listener.onGetNewNotificationCountFinished(response, exception);
        }
    }
}