package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.CreateEventRequest;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.SendFriendRequestRequest;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CreateEventTask extends AbstractApiTask {
    public interface CreateEventListener {
        void onCreateEventFinished(Event response, ApiException exception);
    }

    private CreateEventListener listener;

    public CreateEventTask(CreateEventListener listener) {
        this.listener = listener;
    }

    public void run(int userId, int ownerId, String title,
                    String description, Long date, String location) {
        Callback<RestResponse<Event>> callback = new Callback<RestResponse<Event>>() {
            @Override
            public void success(RestResponse<Event> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.createEvent(
                new CreateEventRequest(userId, ownerId, title, description, date, location),
                callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(Event response, ApiException exception) {
        if (listener != null) {
            listener.onCreateEventFinished(response, exception);
        }
    }
}
