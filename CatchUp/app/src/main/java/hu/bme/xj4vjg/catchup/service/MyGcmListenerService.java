package hu.bme.xj4vjg.catchup.service;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.activity.MainActivity;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.FriendRequestState;
import hu.bme.xj4vjg.catchup.api.NotificationType;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;

public class MyGcmListenerService extends GcmListenerService {
    private static final String TAG = "MyGcmListenerService";
    private static final String NOTIFICATION_TYPE_MESSAGE_KEY = "NotificationType";
    private static final String FRIEND_REQUEST_STATE_MESSAGE_KEY = "FriendRequestState";
    private static final String EVENT_INVITATION_STATE_MESSAGE_KEY = "EventInvitationState";
    private static final String EVENT_TITLE_MESSAGE_KEY = "EventTitle";
    public static final int GCM_INTENT_REQUEST_CODE = 100;

    public static int notificationId = 1;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        // UI szálon való futtatás engedélyezése
        Looper.prepare();
        notificationId++;
        Log.d("debug_key", "GCM MESSAGE RECEIVED IN LISTENER");
        // Üzenet feldolgozása
        NotificationType notificationType = null;
        if (data.containsKey(NOTIFICATION_TYPE_MESSAGE_KEY)) {

            notificationType = NotificationType.valueOf(
                    data.getString(NOTIFICATION_TYPE_MESSAGE_KEY));
        }
        else {
            return;
        }
        User user;
        if (data.containsKey(User.ID_KEY) && data.containsKey(User.EMAIL_KEY)
                && data.containsKey(User.FIRST_NAME_KEY) && data.containsKey(User.LAST_NAME_KEY) ) {
            user = new User(
                    data.getInt(User.ID_KEY),
                    data.getString(User.EMAIL_KEY),
                    data.getString(User.FIRST_NAME_KEY),
                    data.getString(User.LAST_NAME_KEY)
            );
        }
        else {
            return;
        }
        FriendRequestState friendRequestState = null;
        EventInvitationState eventInvitationState = null;
        String eventTitle = "";
        if (notificationType == NotificationType.FriendRequest) {
            if (data.containsKey(FRIEND_REQUEST_STATE_MESSAGE_KEY)) {
                friendRequestState = FriendRequestState.valueOf(
                        data.getString(FRIEND_REQUEST_STATE_MESSAGE_KEY)
                );
            }
            else {
                return;
            }
        } else if (notificationType == NotificationType.EventInvite) {
            if (data.containsKey(EVENT_INVITATION_STATE_MESSAGE_KEY)
                    && data.containsKey(EVENT_TITLE_MESSAGE_KEY)) {
                eventInvitationState = EventInvitationState.valueOf(
                        data.getString(EVENT_INVITATION_STATE_MESSAGE_KEY)
                );
                eventTitle = data.getString(EVENT_TITLE_MESSAGE_KEY);
            }
            else {
                return;
            }
        } else {
            return;
        }

        // Ha az alkalmazásunk meg van nyitva, akkor megpróbáljuk értesíteni a TabActivity-t
        // a Notification érkezéséről
        if (CatchUpApplication.isApplicationVisible()) {
            Log.d("debug_key", "SENDING INTENT TO RECEIVER");
            Intent receiverIntent = new Intent(
                    getString(R.string.catchupapi_message_received_action));
            LocalBroadcastManager.getInstance(this).sendBroadcast(receiverIntent);
        }
        // Ha nincs előtérben az alkalmazás, akkor Android Notification-t küldünk az értesítésről
        else {
            Log.d("debug_key", "SENDING NOTIFICATION");
            String title = (notificationType == NotificationType.FriendRequest)
                    ? getString(R.string.notification_friend_request)
                    : getString(R.string.notification_event_invite);
            String text = null;
            String answer = null;
            if (notificationType == NotificationType.FriendRequest) {
                if (friendRequestState == FriendRequestState.Pending) {
                    text = user.getFullName() + " "
                        + getString(R.string.notification_requested_for_friend);
                }
                else {
                    answer = (friendRequestState == FriendRequestState.Accepted)
                            ? getString(R.string.notification_accepted)
                            : getString(R.string.notification_declined);

                    text = user.getFullName() + " "
                            + answer + " "
                            + getString(R.string.notification_answered_friend_request);
                }
            }
            else {
                if (eventInvitationState == EventInvitationState.Pending) {
                    text = user.getFullName() + " "
                            + getString(R.string.notification_invited_to) + " '"
                            + eventTitle + "' "
                            + getString(R.string.notification_to_event);
                }
                else {
                    answer = (eventInvitationState == EventInvitationState.Going)
                            ? getString(R.string.notification_accepted)
                            : getString(R.string.notification_declined);

                    text = user.getFullName() + " "
                            + answer + " "
                            + getString(R.string.notification_answered_to_event_invite_for)
                            + " '" + eventTitle + "'.";
                }
            }

            sendNotification(title, text);
        }

        Looper.loop();
    }

    // Android Notification küldése az eseményről
    private void sendNotification(String title, String text) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                GCM_INTENT_REQUEST_CODE,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cast_ic_notification_0)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}