package hu.bme.xj4vjg.catchup.api;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.auth.api.Auth;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

/**
 * A CatchUp szerver(API) interfésze Retrofit keretrendszer használatával.
 */
public interface ICatchUpApi {
    // Az API default IP címe
    public static final String DEFAULT_IP = "192.168.0.13";
    // Az API port száma
    public static final String PORT = "8080";
    // Az API domain-en belüli elérési útvonala
    public static final String API_URI = "/CatchUpApi";

    @GET("/Users")
    public void listUserEmails(Callback<RestResponse<List<String>>> callback);

    @POST("/BasicLogin")
    public void basicLogin(@Body BasicLoginRequest request, Callback<RestResponse<LoginResponse>> callback);

    @PUT("/BasicRegister")
    public void basicRegister(@Body BasicRegisterRequest request, Callback<RestResponse<Boolean>> callback);

    @POST("/FacebookLogin")
    public void facebookLogin(@Body FacebookLoginRequest request, Callback<RestResponse<LoginResponse>> callback);

    @POST("/GoogleLogin")
    public void googleLogin(@Body GoogleLoginRequest request, Callback<RestResponse<LoginResponse>> callback);

    @PUT("/FacebookSignIn")
    public void facebookSignIn(@Body FacebookSignInRequest request, Callback<RestResponse<LoginResponse>> callback);

    @PUT("/GoogleSignIn")
    public void googleSignIn(@Body GoogleSignInRequest request, Callback<RestResponse<LoginResponse>> callback);

    @POST("/UpdateGcmToken")
    public void updateGcmToken(@Body UpdateGcmTokenRequest request, Callback<RestResponse<Boolean>> callback);

    @POST("/GetFriends")
    public void getFriends(@Body AuthRequest request, Callback<RestResponse<List<User>>> callback);

    @POST("/GetEvent")
    public void getEvent(@Body GetEventRequest request, Callback<RestResponse<Event>> callback);

    @POST("/GetEvents")
    public void getEvents(@Body AuthRequest request, Callback<RestResponse<List<Event>>> callback);

    @POST("/CreateEvent")
    public void createEvent(@Body CreateEventRequest request, Callback<RestResponse<Event>> callback);

    @POST("/SearchUser")
    public void searchUser(@Body SearchUserRequest request, Callback<RestResponse<List<UserDetail>>> callback);

    @POST("/GetNewNotificationCount")
    public void getNewNotificationCount(@Body AuthRequest request, Callback<RestResponse<Integer>> callback);

    @POST("/GetNotifications")
    public void getNotifications(@Body AuthRequest request, Callback<RestResponse<List<Notification>>> callback);

    @POST("/SendFriendRequest")
    public void sendFriendRequest(@Body SendFriendRequestRequest request, Callback<RestResponse<Boolean>> callback);

    @POST("/SendEventInvite")
    public void sendEventInvite(@Body SendEventInviteRequest request, Callback<RestResponse<Boolean>> callback);

    @POST("/AcceptFriendRequest")
    public void acceptFriendRequest(@Body AnswerFriendRequestRequest request, Callback<RestResponse<Boolean>> callback);

    @POST("/DeclineFriendRequest")
    public void declineFriendRequest(@Body AnswerFriendRequestRequest request, Callback<RestResponse<Boolean>> callback);

    @POST("/AnswerEventInvite")
    public void answerEventInvite(@Body AnswerEventInviteRequest request, Callback<RestResponse<Boolean>> callback);

    @POST("/GetUser")
    public void getUser(@Body AuthRequest request, Callback<RestResponse<User>> callback);
}
