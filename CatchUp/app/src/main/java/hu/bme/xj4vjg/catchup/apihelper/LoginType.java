package hu.bme.xj4vjg.catchup.apihelper;

/**
 * Created by Koós on 2015.10.11..
 */
public enum LoginType {
    Basic, Google, Facebook
}
