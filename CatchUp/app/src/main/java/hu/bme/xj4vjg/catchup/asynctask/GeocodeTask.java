package hu.bme.xj4vjg.catchup.asynctask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;

public class GeocodeTask extends AsyncTask<Void, Void, List<Address>> {
    public static final String TAG = "GeocodeTask";

    public interface GeocodeListener {
        void onGeocodeFinished(List<Address> addresses, boolean drawMarker);
    }

    GeocodeListener listener;
    Context context;
    String locationString;
    boolean drawMarker = false;
    int maxResults = 1;

    public GeocodeTask(Context context, String locationString, boolean drawMarker, int maxResults) {
        this.context = context;
        this.locationString = locationString;
        this.drawMarker = drawMarker;
        this.maxResults = maxResults;
    }

    public GeocodeTask(Context context, String locationString, boolean drawMarker) {
        this.context = context;
        this.locationString = locationString;
        this.drawMarker = drawMarker;
    }

    public void setListener(GeocodeListener listener) {
        this.listener = listener;
    }

    @Override
    protected List<Address> doInBackground(Void... params) {
        List<Address> addresses = new ArrayList<Address>();

        Geocoder geocoder = new Geocoder(context);
        try {
            addresses.addAll(geocoder.getFromLocationName(locationString, maxResults));
        } catch (IOException e) {
            Log.d(TAG, context.getString(R.string.geocode_error));
        }

        return addresses;
    }

    @Override
    protected void onPostExecute(List<Address> addresses) {
        if (listener != null) {
            listener.onGeocodeFinished(addresses, drawMarker);
        }
    }
}
