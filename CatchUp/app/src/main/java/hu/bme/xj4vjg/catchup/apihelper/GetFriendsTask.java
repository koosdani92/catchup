package hu.bme.xj4vjg.catchup.apihelper;

import java.util.List;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.AuthRequest;
import hu.bme.xj4vjg.catchup.api.BasicRegisterRequest;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.User;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class GetFriendsTask extends AbstractApiTask {
    public interface GetFriendsListener {
        void onGetFriendsFinished(List<User> response, ApiException exception);
    }

    private GetFriendsListener listener;

    public GetFriendsTask(GetFriendsListener listener) {
        this.listener = listener;
    }

    public void run(int userId) {
        Callback<RestResponse<List<User>>> callback = new Callback<RestResponse<List<User>>>() {
            @Override
            public void success(RestResponse<List<User>> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.getFriends(new AuthRequest(userId), callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(List<User> response, ApiException exception) {
        if (listener != null) {
            listener.onGetFriendsFinished(response, exception);
        }
    }
}