package hu.bme.xj4vjg.catchup.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.Notification;
import hu.bme.xj4vjg.catchup.api.User;

// Adapter osztály a RecyclerView használatához
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
    public enum NotificationAction { Accept, Decline };
    public interface OnItemClickListener {
        void onNotificationAction(Notification notification, NotificationAction action);
    };

    // A megjelenítendő adathalmaz
    private List<Notification> data;
    private OnItemClickListener listener;

    private Context context;
    private User user;

    public class NotificationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView cardView;
        public LinearLayout notificationContentLayout;
        public LinearLayout notificationInteractionLayout;
        public TextView messageTextView;
        public TextView dateTextView;
        public ImageView okImageView;
        public ImageView noImageView;
        public NotificationViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            notificationContentLayout =
                    (LinearLayout) itemView.findViewById(R.id.notification_content_layout);
            notificationInteractionLayout =
                    (LinearLayout) itemView.findViewById(R.id.notification_interaction_layout);
            messageTextView = (TextView) itemView.findViewById(R.id.message_text_view);
            dateTextView = (TextView) itemView.findViewById(R.id.date_text_view);
            okImageView = (ImageView) itemView.findViewById(R.id.ok_image_view);
            noImageView = (ImageView) itemView.findViewById(R.id.no_image_view);;
            okImageView.setOnClickListener(this);
            noImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                NotificationAction action;
                if (v.getId() == R.id.ok_image_view) {
                    action = NotificationAction.Accept;
                }
                else {
                    action = NotificationAction.Decline;
                }
                listener.onNotificationAction(data.get(getAdapterPosition()), action);
            }
        }
    }

    public NotificationAdapter(Context context, User user, List<Notification> data){
        this.context = context;
        this.user = user;
        this.data = data;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext())
                .inflate(R.layout.notification_row, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        Notification notification = data.get(position);
        String message = notification.toString(context, user.getId());
        String date = notification.getDateString(user.getId());

        boolean notificationSeen = (notification.getFriendRequest() != null)
                ? notification.getFriendRequest().getSeen()
                : notification.getEventInvite().getSeen();
        if (!notificationSeen) {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cardview_highlight));
        }
        else {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cardview_normal));
        }

        if (notification.isPending()) {
            holder.notificationContentLayout.setWeightSum(100);
            holder.notificationInteractionLayout.setVisibility(View.VISIBLE);
        }
        else {
            holder.notificationContentLayout.setWeightSum(0);
            holder.notificationInteractionLayout.setVisibility(View.GONE);
        }

        holder.messageTextView.setText(message);
        holder.dateTextView.setText(date);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

