package hu.bme.xj4vjg.catchup.apihelper;

import android.app.Activity;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.GoogleLoginRequest;
import hu.bme.xj4vjg.catchup.api.GoogleSignInRequest;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.LoginResponse;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A Google bejelentkezési folyamatot végző objektum.
 */
public class GoogleLoginTask extends AbstractApiTask
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private LoginTaskListener listener;
    // A bejelentkezéshez tartozó Activity
    private Activity activity;
    // GoogleApi objektum
    private GoogleApiClient googleApiClient;

    // Google bejelentkezési folyamat változói
    private static final int RC_SIGN_IN = 0;
    private boolean mIsResolving = false;
    private boolean mShouldResolve = false;

    public GoogleLoginTask(Activity activity, LoginTaskListener listener) {
        this.activity = activity;
        this.listener = listener;
        this.googleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .build();;
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    // Google bejelentkezés indítása
    public void run() {
        if (googleApiClient.isConnected()) {
            logoutFromGoogle();
        }

        mShouldResolve = true;
        googleApiClient.connect();
    }

    public void logout() {
        logoutFromGoogle();
    }

    // Kijelentkezés
    private void logoutFromGoogle() {
        Log.d("debug_key", "X");
        if (googleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(googleApiClient);
            googleApiClient.disconnect();
        }
    }

    // Sikeres bejelentkezés a Google fiókba
    @Override
    public void onConnected(Bundle bundle) {
        mShouldResolve = false;
        Log.d("debug_key", "A");
        if (Plus.PeopleApi.getCurrentPerson(googleApiClient) != null) {
            Person person = Plus.PeopleApi.getCurrentPerson(googleApiClient);
            tryLoginToApi(person.getId());
        }
        else {
            logoutFromGoogle();
            notifyListener(
                    LoginType.Google,
                    null,
                    new ApiException(ApiException.ApiExceptionType.Unknown));
        }
    }

    @Override
    public void onConnectionSuspended(int i) { }

    // Sikertelen bejelentkezés Google fiókba
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("debug_key", "C");
        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(activity, RC_SIGN_IN);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    Log.e(this.getClass().toString(), "Could not resolve ConnectionResult.", e);
                    mIsResolving = false;
                    googleApiClient.connect();
                }
            } else {
                notifyListener(
                        LoginType.Google,
                        null,
                        new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        // A User megszakította a Google bejelentkezést
        } else {
            notifyListener(LoginType.Google, new LoginResponse(false, -1), null);
        }
    }

    // Sikeres Google fiók bejelentkezés után itt próbálunk meg belépni a kapott Google azonosítóval
    // az API-ba
    private void tryLoginToApi(final String googleId) {
        // GoogleId elmentése SharedPreferences-be az AutoLogin funkció miatt
        PreferencesHelper.putString(activity, PreferencesHelper.GOOGLE_LOGIN_ID, googleId);

        Callback<RestResponse<LoginResponse>> callback = new Callback<RestResponse<LoginResponse>>() {
            @Override
            public void success(RestResponse<LoginResponse> result, Response response) {
                // API exception volt
                if (result.getException() != null) {
                    logoutFromGoogle();
                    notifyListener(
                            LoginType.Google,
                            null,
                            result.getException());
                }
                // Nem volt API exception
                else
                {
                    // Sikerült bejelentkezni Google-el
                    if (result.getResponse().getLoginResult()) {
                        notifyListener(LoginType.Google, result.getResponse(), null);
                    }
                    // Nem sikerült bejelentkezni google-el (regisztrálni kell a google id-val)
                    else {
                        // Elérhetők a felhasználó Google adatai
                        if (Plus.PeopleApi.getCurrentPerson(googleApiClient) != null) {
                            Person person = Plus.PeopleApi.getCurrentPerson(googleApiClient);
                            GoogleSignInRequest googleData =
                                    new GoogleSignInRequest(
                                            person.getId(),
                                            Plus.AccountApi.getAccountName(googleApiClient),
                                            person.getName().getGivenName(),
                                            person.getName().getFamilyName());
                            trySignInToApi(googleData);
                        }
                        // Nem érhetők el a Google adatok
                        else {
                            logoutFromGoogle();
                            notifyListener(
                                    LoginType.Google,
                                    null,
                                    new ApiException(ApiException.ApiExceptionType.Unknown));
                        }
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logoutFromGoogle();
                notifyListener(
                        LoginType.Google,
                        null,
                        new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.googleLogin(new GoogleLoginRequest(googleId), callback);
    }

    // Ha nem sikerül a bejelentkezés, akkor regisztrálunk Google fiókkal
    private void trySignInToApi(GoogleSignInRequest googleData) {
        Callback<RestResponse<LoginResponse>> callback = new Callback<RestResponse<LoginResponse>>() {
            @Override
            public void success(RestResponse<LoginResponse> result, Response response) {
                // API exception volt
                if (result.getException() != null) {
                    logoutFromGoogle();
                    notifyListener(
                            LoginType.Google,
                            null,
                            result.getException());
                }
                // Nem volt API exception és sikeres volt a facebook-os regisztráció
                else if (result.getResponse().getLoginResult()) {
                    notifyListener(LoginType.Google, result.getResponse(), null);
                } else {
                    logoutFromGoogle();
                    notifyListener(
                            LoginType.Google,
                            result.getResponse(),
                            null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logoutFromGoogle();
                notifyListener(
                        LoginType.Google,
                        new LoginResponse(false, -1),
                        new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.googleSignIn(googleData, callback);
    }

    // Activity hívja meg az onActivityResult-ban.
    // Itt értesülünk a bejelentkezés visszatérési értékéről
    public void onActivityResult(int requestCode, int resultCode) {
        Log.d("debug_key", "E");
        if (requestCode == RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (resultCode != activity.RESULT_OK) {
                mShouldResolve = false;
            }

            mIsResolving = false;
            googleApiClient.connect();
        }
    }

    // Listener értesítése a regisztráció eredményéről
    protected void notifyListener(LoginType type, LoginResponse response, ApiException exception) {
        if (listener != null) {
            listener.onLoginFinished(type, response, exception);
        }
    }
}
