package hu.bme.xj4vjg.catchup.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.activity.MainActivity;
import hu.bme.xj4vjg.catchup.activity.SearchUsersActivity;
import hu.bme.xj4vjg.catchup.adapter.FriendAdapter;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.GetFriendsTask;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;

public class FriendsFragment extends Fragment implements
        GetFriendsTask.GetFriendsListener,
        TabFragment {
    private static final String USER_ARG = "User";

    private User user;
    // API objektum
    private ICatchUpApi catchUpApi;
    // A barátok listája
    private List<User> friends;
    // A barátok listáját kezelő adapter
    private FriendAdapter friendAdapter;

    private GetFriendsTask getFriendsTask;

    // UI vezérlők
    private RecyclerView recyclerView;
    private FloatingActionButton addFriendButton;

    public static FriendsFragment newInstance(User userArg) {
        FriendsFragment fragment = new FriendsFragment();
        Bundle args = new Bundle();
        args.putParcelable(USER_ARG, userArg);
        fragment.setArguments(args);
        return fragment;
    }

    public FriendsFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(USER_ARG);
        }

        getFriendsTask = new GetFriendsTask(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        // UI vezérlők elmentése
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        addFriendButton = (FloatingActionButton) view.findViewById(R.id.add_friend_button);

        // RecycleView beállítása
        friends = new ArrayList<User>();
        friendAdapter = new FriendAdapter(friends);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(friendAdapter);

        // FloatingActionButton onclicklistener
        addFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddFriendClick();
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        catchUpApi = CatchUpApiGenerator.generateApi(getContext());
        getFriendsTask.setApi(catchUpApi);

        // Ha a fragmens előtérbe kerül, akkor frissítjüka  tartalmát
        getFriendsTask.run(user.getId());
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CatchUpApplication) getActivity().getApplication()).trackScreen(R.string.analytics_screen_friends);
    }

    // GetFriends task callback
    @Override
    public void onGetFriendsFinished(List<User> response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this.getContext());
        }

        // Barátok frissítése
        if (response != null) {
            friends.clear();
            friends.addAll(response);
            friendAdapter.notifyDataSetChanged();
        }
    }

    // AddFriendButton eseménykezelő
    private void onAddFriendClick() {
        Intent intent = new Intent(getContext(), SearchUsersActivity.class);
        intent.putExtra(SearchUsersActivity.USER_EXTRA_KEY, user);
        startActivity(intent);
    }

    // Adatok frissítése
    @Override
    public void updateContent() {
        if (friends != null && friendAdapter != null && recyclerView != null && getFriendsTask != null) {
            getFriendsTask.run(user.getId());
        }
    }
}
