package hu.bme.xj4vjg.catchup.api;

public class ApiException {
	public enum ApiExceptionType {
		Database, 
		Unknown,
		NonexistentUserId
	}
	
	private ApiExceptionType type;
	private String msg;
	
	public ApiException() {
		this(null, "");
	}
	
	public ApiException(ApiExceptionType type) {
		this(type, "");
	}
	
	public ApiException(ApiExceptionType type, String msg) {
		this.type = type;
		this.msg = msg;
	}

	public ApiExceptionType getType() {
		return type;
	}

	public void setType(ApiExceptionType type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
