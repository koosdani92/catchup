package hu.bme.xj4vjg.catchup.apihelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.FacebookLoginRequest;
import hu.bme.xj4vjg.catchup.api.FacebookSignInRequest;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.LoginResponse;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A Facebook bejelentkezési folyamatot végző objektum.
 */
public class FacebookLoginTask extends AbstractApiTask {
    private LoginTaskListener listener;
    // A bejelentkezéshez tartozó Activity
    private Activity activity;
    // Az OAUTH2 access token
    private AccessToken accessToken;
    // Login folyamatot kezelő CallbackManager
    private CallbackManager callbackManager;

    public FacebookLoginTask(Activity activity, LoginTaskListener listener) {
        this.activity = activity;
        this.listener = listener;
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookLoginCallback());
    }

    // FacebookLoginCallback implementáció, amely a bejelentkezés eredményét kezeli
    private class FacebookLoginCallback implements FacebookCallback<LoginResult> {
        @Override
        public void onSuccess(LoginResult loginResult) {
            accessToken = loginResult.getAccessToken();
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                            try {
                                tryLoginToApi(jsonObject.getString("id"));
                            } catch (JSONException e) {
                                logoutFromFacebook();
                                notifyListener(
                                        LoginType.Facebook,
                                        null,
                                        new ApiException(ApiException.ApiExceptionType.Unknown)
                                );
                            }
                        }
                    }
            );
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            notifyListener(LoginType.Facebook, new LoginResponse(false, -1), null);
        }

        @Override
        public void onError(FacebookException e) {
            notifyListener(
                    LoginType.Facebook,
                    null,
                    new ApiException(ApiException.ApiExceptionType.Unknown));
        }
    }

    // A Facebook azonosító segítségével megpróbál bejelentkezni az API-ba
    private void tryLoginToApi(final String facebookId) {
        // FacebookId elmentése SharedPreferences-be az AutoLogin funkció miatt
        PreferencesHelper.putString(activity, PreferencesHelper.FACEBOOK_LOGIN_ID, facebookId);

        Callback<RestResponse<LoginResponse>> callback = new Callback<RestResponse<LoginResponse>>() {
            @Override
            public void success(RestResponse<LoginResponse> result, Response response) {
                // API exception volt
                if (result.getException() != null) {
                    logoutFromFacebook();
                    notifyListener(
                            LoginType.Facebook,
                            null,
                            result.getException());
                }
                // Nem volt API exception
                else
                {
                    // Sikerült bejelentkezni facebook-al
                    if (result.getResponse().getLoginResult()) {
                        notifyListener(LoginType.Facebook, result.getResponse(), null);
                    }
                    // Nem sikerült bejelentkezni facebookal (regisztrálni kell a facebook id-val)
                    else {
                        GraphRequest request = GraphRequest.newMeRequest(
                                accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                                        try {
                                            FacebookSignInRequest facebookData =
                                                    new FacebookSignInRequest(
                                                            jsonObject.getString("id"),
                                                            jsonObject.getString("email"),
                                                            jsonObject.getString("first_name"),
                                                            jsonObject.getString("last_name"));
                                            trySignInToApi(facebookData);
                                        } catch (JSONException e) {
                                            logoutFromFacebook();
                                            notifyListener(
                                                    LoginType.Facebook,
                                                    null,
                                                    new ApiException(
                                                            ApiException.ApiExceptionType.Unknown));
                                        }
                                    }
                                }
                        );
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,email,first_name,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logoutFromFacebook();
                notifyListener(
                        LoginType.Facebook,
                        null,
                        new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.facebookLogin(new FacebookLoginRequest(facebookId), callback);
    }

    // A Facebook azonosító segítségével megpróbál regisztrálni az API-ba
    private void trySignInToApi(FacebookSignInRequest facebookData) {
        Callback<RestResponse<LoginResponse>> callback = new Callback<RestResponse<LoginResponse>>() {
            @Override
            public void success(RestResponse<LoginResponse> result, Response response) {
                // API exception volt
                if (result.getException() != null) {
                    logoutFromFacebook();
                    notifyListener(
                            LoginType.Facebook,
                            null,
                            new ApiException(ApiException.ApiExceptionType.Unknown));
                }
                // Nem volt API exception és sikeres volt a facebook-os regisztráció
                else {
                    notifyListener(LoginType.Facebook, result.getResponse(), null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                logoutFromFacebook();
                notifyListener(
                        LoginType.Facebook,
                        null,
                        new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.facebookSignIn(facebookData, callback);
    }

    // Facebook login folyamat indítása
    public void run() {
        LoginManager.getInstance().logInWithReadPermissions(
                activity, Arrays.asList("public_profile", "email"));
    }

    // Kijelentkezés a Facebook fiókból
    public void logout() {
        logoutFromFacebook();
    }

    private void logoutFromFacebook() {
        LoginManager.getInstance().logOut();
    }

    // Activity hívja meg az onActivityResult-ban.
    // Itt értesülünk a bejelentkezés visszatérési értékéről
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // Listener értesítése a regisztráció eredményéről
    protected void notifyListener(LoginType type, LoginResponse response, ApiException exception) {
        if (listener != null) {
            listener.onLoginFinished(type, response, exception);
        }
    }
}
