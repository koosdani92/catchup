package hu.bme.xj4vjg.catchup.api;

/**
 * A facebookLogin API h�v�s param�ter le�r�ja.
 */
public class FacebookLoginRequest {
	public String facebookId;
	
	public FacebookLoginRequest() {}
	
	public FacebookLoginRequest(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
}
