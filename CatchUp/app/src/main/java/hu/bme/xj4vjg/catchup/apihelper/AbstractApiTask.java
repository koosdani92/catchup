package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.ICatchUpApi;

public abstract class AbstractApiTask {
    protected ICatchUpApi catchUpApi;

    public void setApi(ICatchUpApi catchUpApi) {
        this.catchUpApi = catchUpApi;
    }
}
