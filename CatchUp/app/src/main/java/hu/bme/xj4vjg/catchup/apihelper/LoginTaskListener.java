package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.LoginResponse;

public interface LoginTaskListener {
    void onLoginFinished(LoginType type, LoginResponse response, ApiException exception);
}
