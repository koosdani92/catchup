package hu.bme.xj4vjg.catchup.apihelper;

import java.util.List;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.AuthRequest;
import hu.bme.xj4vjg.catchup.api.Notification;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetNotificationsTask extends AbstractApiTask {
    public interface GetNotificationsListener {
        void onGetNotificationsFinished(List<Notification> response, ApiException exception);
    }

    private GetNotificationsListener listener;

    public GetNotificationsTask(GetNotificationsListener listener) {
        this.listener = listener;
    }

    public void run(int userId) {
        Callback<RestResponse<List<Notification>>> callback = new Callback<RestResponse<List<Notification>>>() {
            @Override
            public void success(RestResponse<List<Notification>> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.getNotifications(new AuthRequest(userId), callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(List<Notification> response, ApiException exception) {
        if (listener != null) {
            listener.onGetNotificationsFinished(response, exception);
        }
    }
}

