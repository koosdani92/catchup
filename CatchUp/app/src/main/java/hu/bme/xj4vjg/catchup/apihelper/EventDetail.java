package hu.bme.xj4vjg.catchup.apihelper;

import android.location.Address;

import hu.bme.xj4vjg.catchup.api.Event;

public class EventDetail {
    private int id;
    private Address address;
    private long date;

    public EventDetail(int id, Address address, long date) {
        this.id = id;
        this.address = address;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
