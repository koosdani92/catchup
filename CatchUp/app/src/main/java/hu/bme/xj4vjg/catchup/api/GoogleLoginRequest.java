package hu.bme.xj4vjg.catchup.api;

/**
 * A googleLogin API h�v�s param�ter le�r�ja.
 */
public class GoogleLoginRequest {
	public String googleId;
	
	public GoogleLoginRequest() {}
	
	public GoogleLoginRequest(String googleId) {
		this.googleId = googleId;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}
}