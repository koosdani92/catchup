package hu.bme.xj4vjg.catchup.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.fragment.EventsFragment;
import hu.bme.xj4vjg.catchup.fragment.FriendsFragment;
import hu.bme.xj4vjg.catchup.fragment.NotificationsFragment;

public class TabPagerAdapter extends FragmentPagerAdapter {
    private Context context;
    private User user;

    private final List<String> titles = new ArrayList<>();
    private final List<Fragment> fragments = new ArrayList<>();

    private FriendsFragment friendsFragment = null;
    private EventsFragment eventsFragment = null;
    private NotificationsFragment notificationsFragment = null;

    public TabPagerAdapter(Context context, User user, FragmentManager manager) {
        super(manager);
        this.context = context;
        this.user = user;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        title = titles.get(position);

        return title;
    }

    public void initializeFragments() {
        titles.add(context.getString(R.string.tab_friends_title));
        titles.add(context.getString(R.string.tab_events_title));
        titles.add(context.getString(R.string.tab_notifications_title));
        friendsFragment = FriendsFragment.newInstance(user);
        eventsFragment = EventsFragment.newInstance(user);
        notificationsFragment = NotificationsFragment.newInstance(user);
        fragments.add(friendsFragment);
        fragments.add(eventsFragment);
        fragments.add(notificationsFragment);
    }

    public void updateFragments() {
        updateFriendsFragment();
        updateEventsFragment();
        updateNotificationsFragment();
    }

    public void updateFriendsFragment() {
        if (friendsFragment != null) {
            friendsFragment.updateContent();
        }
    }

    public void updateEventsFragment() {
        if (eventsFragment != null) {
            eventsFragment.updateContent();
        }
    }

    public void updateNotificationsFragment() {
        if (notificationsFragment != null) {
            notificationsFragment.updateContent();
        }
    }

    public FriendsFragment getFriendsFragment() {
        return friendsFragment;
    }

    public EventsFragment getEventsFragment() {
        return eventsFragment;
    }

    public NotificationsFragment getNotificationsFragment() { return notificationsFragment; }
}