package hu.bme.xj4vjg.catchup.apihelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

// API objektum létrehozását segítő objektum
public class CatchUpApiGenerator {
    // API objektum létrehozása
    public static ICatchUpApi generateApi(Context context) {
        String ip = getApiIpFromSharedPref(context);
        String apiUrl = "http://" + ip + ":" + ICatchUpApi.PORT + ICatchUpApi.API_URI;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(apiUrl)
                .setClient(new OkClient(new OkHttpClient()))
                .build();
        return restAdapter.create(ICatchUpApi.class);
    }

    // API IP cím lekérése a SharedPreferences-ből
    public static String getApiIpFromSharedPref(Context context) {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(context.getString(R.string.api_ip_key), ICatchUpApi.DEFAULT_IP);
    }

    // API IP cím módosítása a SharedPreferences-ben
    public static void setApiIpFromSharedPref(Context context, String newApiIp) {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.api_ip_key), newApiIp);
        editor.commit();
    }
}
