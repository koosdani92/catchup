package hu.bme.xj4vjg.catchup.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.activity.CreateEventActivity;
import hu.bme.xj4vjg.catchup.activity.EventDetailsActivity;
import hu.bme.xj4vjg.catchup.activity.MainActivity;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.GetEventTask;
import hu.bme.xj4vjg.catchup.apihelper.GetUserTask;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;

public class GeofenceTransitionsIntentService extends IntentService implements
        GetEventTask.GetEventListener,
        GetUserTask.GetUserListener {
    public static final String TAG = "GeofenceTransitionsIS";
    public static final int GEOFENCE_INTENT_REQUEST_CODE = 200;

    public static int notificationId = 1;
    private ICatchUpApi catchUpApi;
    private GetEventTask getEventTask;
    private GetUserTask getUserTask;
    private final int eventAlertTimeInMillis = 1 *60 * 60 * 1000;
    private Event event;
    private User user;

    public GeofenceTransitionsIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        getEventTask = new GetEventTask(this);
        getUserTask = new GetUserTask(this);
    }

    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            return;
        }

        catchUpApi = CatchUpApiGenerator.generateApi(this);
        getEventTask.setApi(catchUpApi);
        getUserTask.setApi(catchUpApi);

        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER &&
                !geofencingEvent.getTriggeringGeofences().isEmpty()) {
            Geofence geofence = geofencingEvent.getTriggeringGeofences().get(0);

            Log.d("debug_key", "Geofence captured from event: " + geofence.getRequestId());
            if (PreferencesHelper.contains(this, PreferencesHelper.LOGGED_IN_USERID_KEY)) {
                getEventTask.run(
                        PreferencesHelper.getInt(
                                this, PreferencesHelper.LOGGED_IN_USERID_KEY, -1),
                        Integer.parseInt(geofence.getRequestId()));
            }
        }
    }

    @Override
    public void onGetEventFinished(Event response, ApiException exception) {
        if (exception != null || response == null) {
            return;
        }

        long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();

        if (response.getDate() - currentTimeInMillis <= eventAlertTimeInMillis) {
            event = response;
            getUserTask.run(PreferencesHelper.getInt(this, PreferencesHelper.LOGGED_IN_USERID_KEY, -1));
        }
    }

    @Override
    public void onGetUserFinished(User response, ApiException exception) {
        if (exception != null || response == null) {
            return;
        }
        user = response;

        if (event != null && user != null) {
            sendNotification();
        }
    }

    private void sendNotification() {
        Log.d("debug_key", "Sending notification for event: " + event.getId());
        notificationId++;
        Intent intent = new Intent(this, EventDetailsActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EventDetailsActivity.USER_EXTRA_KEY, user);
        intent.putExtra(EventDetailsActivity.EVENT_ID_EXTRA_KEY, event.getId());
        intent.putExtra(EventDetailsActivity.IS_NEARBY_EVENT_EXTRA_KEY, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                GEOFENCE_INTENT_REQUEST_CODE,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.cast_ic_notification_0)
                .setContentTitle(getString(R.string.notification_nearby_event_title))
                .setContentText(getString(R.string.notification_nearby_event_text))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    public static String getErrorString(Context context, int errorCode) {
        Resources mResources = context.getResources();
        switch (errorCode) {
            case GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE:
                return mResources.getString(R.string.geofence_not_available);
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES:
                return mResources.getString(R.string.geofence_too_many_geofences);
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS:
                return mResources.getString(R.string.geofence_too_many_pending_intents);
            default:
                return mResources.getString(R.string.unknown_geofence_error);
        }
    }
}
