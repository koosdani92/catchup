 package hu.bme.xj4vjg.catchup.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;

 /**
  * Beállítások Activity az API elérésének konfigurálására.
  */
 public class ApiSettingsActivity extends AppCompatActivity {
    TextView currentApiIpText;
    EditText newApiIpText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentApiIpText = (TextView) findViewById(R.id.api_current_ip_address_text);
        newApiIpText = (EditText) findViewById(R.id.api_new_ip_address_text);
        currentApiIpText.setText(CatchUpApiGenerator.getApiIpFromSharedPref(this));
        newApiIpText.setText(CatchUpApiGenerator.getApiIpFromSharedPref(this));
        findViewById(R.id.save_settings_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSettings();
            }
        });
    }

     @Override
     protected void onResume() {
         super.onResume();
         ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_settings);
         CatchUpApplication.setApplicationVisible(true);
     }

     @Override
     protected void onPause() {
         super.onPause();
         CatchUpApplication.setApplicationVisible(false);
     }

     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         int id = item.getItemId();

         if (id == android.R.id.home) {
             onBackPressed();
             return true;
         }

         return super.onOptionsItemSelected(item);
     }

     // Beállítások mentése
     private void saveSettings() {
         if (newApiIpText.getText() != null && !newApiIpText.getText().toString().isEmpty()) {
             ((CatchUpApplication) getApplication())
                     .trackActionEvent(R.string.analytics_event_api_ip_change);

             CatchUpApiGenerator.setApiIpFromSharedPref(this, newApiIpText.getText().toString());
             currentApiIpText.setText(CatchUpApiGenerator.getApiIpFromSharedPref(this));
             Toast.makeText(
                     getApplicationContext(),
                     R.string.api_ip_updated,
                     Toast.LENGTH_SHORT).show();
         }
     }
}
