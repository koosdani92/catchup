package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.GetEventRequest;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetEventTask extends AbstractApiTask {
    public interface GetEventListener {
        void onGetEventFinished(Event response, ApiException exception);
    }

    private GetEventListener listener;

    public GetEventTask(GetEventListener listener) {
        this.listener = listener;
    }

    public void run(int userId, int eventId) {
        Callback<RestResponse<Event>> callback = new Callback<RestResponse<Event>>() {
            @Override
            public void success(RestResponse<Event> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.getEvent(new GetEventRequest(userId, eventId), callback);
    }

    protected void notifyListener(Event response, ApiException exception) {
        if (listener != null) {
            listener.onGetEventFinished(response, exception);
        }
    }
}
