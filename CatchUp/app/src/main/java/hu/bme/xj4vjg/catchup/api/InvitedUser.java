package hu.bme.xj4vjg.catchup.api;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Koós on 2015.11.01..
 */
public class InvitedUser implements Parcelable {
    public static final String USER_KEY = "user";
    public static final String INVITATION_STATE_KEY = "invitationState";

    private User user;
    private EventInvitationState invitationState;

    public InvitedUser() { }

    public InvitedUser(User user, EventInvitationState invitationState) {
        this.user = user;
        this.invitationState = invitationState;
    }

    private InvitedUser(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        invitationState = EventInvitationState.valueOf(in.readString());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public EventInvitationState getInvitationState() {
        return invitationState;
    }

    public void setInvitationState(EventInvitationState invitationState) {
        this.invitationState = invitationState;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(user, flags);
        out.writeString(invitationState.toString());
    }

    public static final Parcelable.Creator<InvitedUser> CREATOR =
            new Parcelable.Creator<InvitedUser>() {
                @Override
                public InvitedUser createFromParcel(Parcel in) {
                    return new InvitedUser(in);
                }

                @Override
                public InvitedUser[] newArray(int size) {
                    return new InvitedUser[size];
                }
            };
}
