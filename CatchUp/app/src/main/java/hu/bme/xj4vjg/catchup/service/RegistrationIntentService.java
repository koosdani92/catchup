package hu.bme.xj4vjg.catchup.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;
import hu.bme.xj4vjg.catchup.apihelper.UpdateGcmTokenTask;


public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    public static final String LOGGING_IN_GCM_TOKEN_KEY = "LoggingInGcmTokenKey";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("debug_key", "RETRIEVING GCM TOKEN FROM GOOGLE");
        try {
            // Registration Token igénylése, elmentése sharedpref-be
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.sender_id),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            PreferencesHelper.putString(getApplicationContext(), PreferencesHelper.GCM_TOKEN_KEY, token);
            // Feliratkozás topic csatornákra
            subscribeTopics(token);
        } catch (Exception e) {
            // Ha bármi hiba volt, akkor töröljük a gcm token értékét
            PreferencesHelper.remove(getApplicationContext(), PreferencesHelper.GCM_TOKEN_KEY);
        }

        Bundle extras = intent.getExtras();
        if (extras.containsKey(LOGGING_IN_GCM_TOKEN_KEY)) {
            // Ha login folyamat miatt igényelünk tokent, akkor értesítjük a broadcastreceivert
            // a token megszerzéséről
            if (extras.getBoolean(LOGGING_IN_GCM_TOKEN_KEY)) {
                Intent receiverIntent = new Intent(
                        getString(R.string.gcm_registration_complete_action));
                LocalBroadcastManager.getInstance(this).sendBroadcast(receiverIntent);
            }
            // Ha nem login folyamat miatt frissült a token, és van jelenleg bejelentkezett
            // felhasználó az eszközön, akkor frissítjük a tokenjét a szerveren
            else if (PreferencesHelper.contains(getApplicationContext(), PreferencesHelper.LOGGED_IN_USERID_KEY)) {
                UpdateGcmTokenTask updateGcmTokenTask = new UpdateGcmTokenTask(null);
                updateGcmTokenTask.run(
                        PreferencesHelper.getInt(
                                getApplicationContext(), PreferencesHelper.LOGGED_IN_USERID_KEY, -1),
                        PreferencesHelper.getString(
                                getApplicationContext(), PreferencesHelper.GCM_TOKEN_KEY, ""));
            }
        }
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}
