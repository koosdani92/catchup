package hu.bme.xj4vjg.catchup.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.CreateEventTask;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;
import hu.bme.xj4vjg.catchup.asynctask.GeocodeTask;
import hu.bme.xj4vjg.catchup.asynctask.ReverseGeocodeTask;
import hu.bme.xj4vjg.catchup.fragment.CustomMapFragment;
import hu.bme.xj4vjg.catchup.fragment.DatePickerFragment;
import hu.bme.xj4vjg.catchup.fragment.TimePickerFragment;
import info.hoang8f.widget.FButton;

public class CreateEventActivity extends AppCompatActivity implements
        CreateEventTask.CreateEventListener,
        DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener,
        OnMapReadyCallback,
        GeocodeTask.GeocodeListener,
        ReverseGeocodeTask.ReverseGeocodeListener {
    public static final String USER_EXTRA_KEY = "User";

    private User user = null;
    private Calendar eventDateTime;
    private GoogleMap googleMap;
    private Marker marker;
    private float googleMapDefaultZoomLevel = 6;
    private float googleMapMarkerZoomLevel = 15;

    private ICatchUpApi catchUpApi;
    private CreateEventTask createEventTask;

    private ScrollView scrollView;
    private EditText eventTitleEditText;
    private EditText eventDescriptionEditText;
    private EditText eventDateEditText;
    private EditText eventTimeEditText;
    private EditText eventLocationEditText;
    private CustomMapFragment mapFragment;
    private FButton createEventButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        eventDateTime = new GregorianCalendar();
        createEventTask = new CreateEventTask(this);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        eventTitleEditText = (EditText) findViewById(R.id.event_title_text);
        eventDescriptionEditText = (EditText) findViewById(R.id.event_description_text);
        eventDateEditText = (EditText) findViewById(R.id.event_date_text);
        eventTimeEditText = (EditText) findViewById(R.id.event_time_text);
        eventLocationEditText = (EditText) findViewById(R.id.event_location_text);
        mapFragment = (CustomMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        createEventButton = (FButton) findViewById(R.id.event_create_button);

        mapFragment.getMapAsync(this);
        mapFragment.setListener(new CustomMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                scrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
        eventDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateEditTextClick();
            }
        });
        eventTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimeEditTextClick();
            }
        });
        eventLocationEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputManager =
                            (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(
                            getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    onLocationTextModified();
                    handled = true;
                }

                return  handled;
            }
        });
        createEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateEventClick();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle extras = (getIntent() == null || getIntent().getExtras() == null)
                ? new Bundle() : getIntent().getExtras();
        if (extras.containsKey(USER_EXTRA_KEY)) {
            user = extras.getParcelable(USER_EXTRA_KEY);
        }
        else if (user == null) {
            MainActivity.sendApplicationError(this);
            return;
        }

        catchUpApi = CatchUpApiGenerator.generateApi(this);
        createEventTask.setApi(catchUpApi);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_create_event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                return;
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                return;
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                onGoogleMapMarkerDragged(marker);
            }
        });

        String currentLocationString = getResources().getConfiguration().locale.getCountry();
        GeocodeTask geocodeTask = new GeocodeTask(this, currentLocationString, false);
        geocodeTask.setListener(this);
        geocodeTask.execute();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        eventDateTime.set(Calendar.YEAR, year);
        eventDateTime.set(Calendar.MONTH, monthOfYear);
        eventDateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        eventDateEditText.setText(dateFormat.format(eventDateTime.getTime()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        eventDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        eventDateTime.set(Calendar.MINUTE, minute);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        eventTimeEditText.setText(dateFormat.format(eventDateTime.getTime()));
    }

    private void onDateEditTextClick() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), DatePickerFragment.TAG);
    }

    private void onTimeEditTextClick() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), TimePickerFragment.TAG);
    }

    private void onLocationTextModified() {
        String locationString = eventLocationEditText.getText().toString();
        if (!locationString.isEmpty()) {
            GeocodeTask geocodeTask = new GeocodeTask(this, locationString, true);
            geocodeTask.setListener(this);
            geocodeTask.execute();
        }
    }

    @Override
    public void onGeocodeFinished(List<Address> addresses, boolean drawMarker) {
        if (googleMap != null) {
            if (addresses.isEmpty()) {
                Toast.makeText(this, R.string.location_geocode_failed, Toast.LENGTH_SHORT).show();
            }
            else {
                Address address = addresses.get(0);
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

                float zoomLevel;
                if (drawMarker) {
                    zoomLevel = googleMapMarkerZoomLevel;
                    if (marker == null) {
                        marker = googleMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(getString(R.string.event_location))
                                .draggable(true));
                    }
                    else {
                        marker.setPosition(latLng);
                    }
                }
                else {
                    zoomLevel = googleMapDefaultZoomLevel;
                }

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
            }
        }
    }

    private void onGoogleMapMarkerDragged(Marker marker) {
        ReverseGeocodeTask reverseGeocodeTask =
                new ReverseGeocodeTask(
                        this,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude);
        reverseGeocodeTask.setListener(this);
        reverseGeocodeTask.execute();
    }

    @Override
    public void onReverseGeocodeFinished(List<Address> addresses) {
        if (addresses.isEmpty()) {
            Toast.makeText(this, R.string.location_reverse_geocode_failed, Toast.LENGTH_SHORT).show();
        }
        else {
            Address address = addresses.get(0);
            StringBuilder addressBuilder = new StringBuilder();
            if (address.getPostalCode() != null) {
                addressBuilder.append(address.getPostalCode()).append(" ");
            }
            if (address.getLocality() != null) {
                addressBuilder.append(address.getLocality()).append(", ");
            }
            if (address.getThoroughfare() != null) {
                addressBuilder.append(address.getThoroughfare()).append(" ");
                if (address.getSubThoroughfare() != null) {
                    addressBuilder.append(address.getSubThoroughfare());
                }
            }

            eventLocationEditText.setText(addressBuilder.toString());
        }
    }

    private void onCreateEventClick() {
        if (checkEventDetails()) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_create_event);

            String description =
                    (eventDescriptionEditText.getText().toString().trim().isEmpty())
                    ? null : eventDescriptionEditText.getText().toString();
            Long date =
                    (eventDateEditText.getText().toString().trim().isEmpty())
                            ? null : eventDateTime.getTimeInMillis();
            String location =
                    (eventLocationEditText.getText().toString().trim().isEmpty())
                            ? null : eventLocationEditText.getText().toString();
            createEventTask.run(
                    user.getId(),
                    user.getId(),
                    eventTitleEditText.getText().toString(),
                    description,
                    date,
                    location
            );
        }
    }

    private boolean checkEventDetails() {
        if (eventTitleEditText.getText() == null || eventTitleEditText.getText().toString().isEmpty()) {
            Toast.makeText(
                    CreateEventActivity.this,
                    getString(R.string.event_empty_title),
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onCreateEventFinished(Event response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this);
            return;
        }

        if (response == null) {
            Toast.makeText(
                    CreateEventActivity.this,
                    getString(R.string.event_create_error),
                    Toast.LENGTH_SHORT).show();
        }
        else {
            Intent intent = new Intent(this, EventDetailsActivity.class);
            intent.putExtra(EventDetailsActivity.USER_EXTRA_KEY, user);
            intent.putExtra(EventDetailsActivity.EVENT_ID_EXTRA_KEY, response.getId());
            startActivity(intent);
            finish();
        }
    }
}
