package hu.bme.xj4vjg.catchup.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.User;

// Adapter osztály a RecyclerView használatához
public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.FriendViewHolder> {
    // A megjelenítendő adathalmaz
    private List<User> data;

    public class FriendViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public TextView nameTextView;
        public TextView emailTextView;
        public FriendViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            emailTextView = (TextView) itemView.findViewById(R.id.email_text_view);
        }
    }

    public FriendAdapter(List<User> data) {
        this.data = data;
    }

    @Override
    public FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext())
                .inflate(R.layout.friend_row, parent, false);
        return new FriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendViewHolder holder, int position) {
        holder.nameTextView.setText(data.get(position).getFullName());
        holder.emailTextView.setText(data.get(position).getEmail());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
