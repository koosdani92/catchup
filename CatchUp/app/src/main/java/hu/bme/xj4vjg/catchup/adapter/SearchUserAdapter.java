package hu.bme.xj4vjg.catchup.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.api.UserDetail;

/**
 * Created by Koós on 2015.10.31..
 */
public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.SearchUserViewHolder> {
    public interface OnItemClickListener {
        void onAddFriend(int friendId);
    };

    private List<UserDetail> users;
    private OnItemClickListener listener;

    public class SearchUserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView searchUserCardView;
        public TextView nameTextView;
        public TextView emailTextView;
        public ImageView friendImageView;
        public ImageView pendingFriendImageView;
        public ImageView addFriendImageView;
        public SearchUserViewHolder(View itemView) {
            super(itemView);
            searchUserCardView = (CardView) itemView.findViewById(R.id.search_user_card_view);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            emailTextView = (TextView) itemView.findViewById(R.id.email_text_view);
            friendImageView = (ImageView) itemView.findViewById(R.id.friend_image_view);
            pendingFriendImageView = (ImageView) itemView.findViewById(R.id.pending_friend_image_view);
            addFriendImageView = (ImageView) itemView.findViewById(R.id.add_friend_image_view);
            addFriendImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null && !users.isEmpty()) {
                listener.onAddFriend(users.get(getAdapterPosition()).getUser().getId());
            }
        }
    }

    public SearchUserAdapter(List<UserDetail> data) {
        this.users = data;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public SearchUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext())
                .inflate(R.layout.search_user_row, parent, false);
        return new SearchUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchUserViewHolder holder, int position) {
        UserDetail currentUserDetail = users.get(position);
        holder.nameTextView.setText(currentUserDetail.getUser().getFullName());
        holder.emailTextView.setText(currentUserDetail.getUser().getEmail());

        holder.friendImageView.setVisibility(View.GONE);
        holder.pendingFriendImageView.setVisibility(View.GONE);
        holder.addFriendImageView.setVisibility(View.GONE);
        if (!currentUserDetail.isFriend() && !currentUserDetail.isPendingFriend()) {
            holder.addFriendImageView.setVisibility(View.VISIBLE);
        } else if (currentUserDetail.isFriend()) {
            holder.friendImageView.setVisibility(View.VISIBLE);
        } else if (currentUserDetail.isPendingFriend()) {
            holder.pendingFriendImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}