package hu.bme.xj4vjg.catchup.api;

public class SendEventInviteRequest extends AuthRequest {
	private int eventId;
	private int invitedId;
	
	public SendEventInviteRequest() { }
	
	public SendEventInviteRequest(int userId, int eventId, int invitedId) {
		super(userId);
		this.eventId = eventId;
		this.invitedId = invitedId;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	public int getInvitedId() {
		return invitedId;
	}

	public void setInvitedId(int invitedId) {
		this.invitedId = invitedId;
	}
}
