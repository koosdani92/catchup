package hu.bme.xj4vjg.catchup.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.apihelper.BasicRegisterTask;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;
import info.hoang8f.widget.FButton;

/**
 * Az egyedi regisztrációs felületet megjelenítő Activity.
 */
public class RegisterActivity extends AppCompatActivity
        implements BasicRegisterTask.BasicRegisterListener {
    // API objektum
    ICatchUpApi catchUpApi;
    // Regisztrációt végző objektum
    BasicRegisterTask basicRegisterTask;

    // UI vezérlők
    private FButton registerButton;
    private EditText emailInput;
    private EditText passwordInput;
    private EditText lastNameInput;
    private EditText firstNameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        basicRegisterTask = new BasicRegisterTask(this);

        emailInput = (EditText) findViewById(R.id.email_text);
        passwordInput = (EditText) findViewById(R.id.password_text);
        lastNameInput = (EditText) findViewById(R.id.last_name_text);
        firstNameInput = (EditText) findViewById(R.id.first_name_text);
        registerButton = (FButton) findViewById(R.id.basic_register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRegisterClick();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_register);
        CatchUpApplication.setApplicationVisible(true);

        // Api inicializálás és átadása a RegisterProcessornak
        catchUpApi = CatchUpApiGenerator.generateApi(this);
        basicRegisterTask.setApi(catchUpApi);
    }

    @Override
    protected void onPause() {
        super.onPause();
        CatchUpApplication.setApplicationVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Regisztrációs gomb eseménykezelője
    private void onRegisterClick() {
        if (emailInput.getText() == null || emailInput.getText().toString().isEmpty()) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.login_error_empty_email,
                    Toast.LENGTH_SHORT).show();
        } else if (passwordInput.getText() == null || passwordInput.getText().toString().isEmpty()) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.login_error_empty_password,
                    Toast.LENGTH_SHORT).show();
        } else if (lastNameInput.getText() == null || lastNameInput.getText().toString().isEmpty()) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.login_error_empty_last_name,
                    Toast.LENGTH_SHORT).show();
        } else if (firstNameInput.getText() == null || firstNameInput.getText().toString().isEmpty()) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.login_error_empty_first_name,
                    Toast.LENGTH_SHORT).show();
        } else {
            showRegisterInProgress();
            disableUi();
            basicRegisterTask.run(
                    emailInput.getText().toString(),
                    passwordInput.getText().toString(),
                    lastNameInput.getText().toString(),
                    firstNameInput.getText().toString());
        }
    }

    // Regisztrációs folyamat jelzése a felhasználónak
    private void showRegisterInProgress() {
        Toast.makeText(
                getApplicationContext(),
                R.string.register_progress,
                Toast.LENGTH_SHORT).show();
    }

    // Register gomb blokkolás a regisztráció idejére
    private void disableUi() {
        registerButton.setEnabled(false);
    }

    // Register gomb engedélyezése a regisztráció végeztével.
    private void enableUi() {
        registerButton.setEnabled(true);
    }

    // RegisterProcessListener implementáció.
    // Ebben a függvényben értesülünk, ha a regisztrációs folyamat az Activity számára érdekes
    //  állapotba került.
    @Override
    public void onBasicRegisterFinished(Boolean response, ApiException exception) {
        enableUi();
        // Hibakezelés
        if (exception != null) {
            MainActivity.sendApplicationError(this);
            return;
        }

        if (!response) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_register_fail);

            Toast.makeText(
                    getApplicationContext(),
                    R.string.register_error_existing_email,
                    Toast.LENGTH_SHORT).show();
        }
        // Sikeres regisztráció
        else {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_register_success);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
