package hu.bme.xj4vjg.catchup.api;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;
import java.util.List;

public class Event implements Parcelable {
	public static final String ID_KEY = "id";
	public static final String OWNER_KEY = "owner";
	public static final String INVITED_USERS_KEY = "invitedUsers";
	public static final String TITLE_KEY = "title";
	public static final String DESCRIPTION_KEY = "description";
	public static final String DATE_KEY = "date";
	public static final String LOCATION_KEY = "location";

	private int id;
	private User owner;
	private List<InvitedUser> invitedUsers;
	private String title;
	private String description;
	private Long date;
	private String location;
	
	public Event() { }
	public Event(int id, User owner, List<InvitedUser> invitedUsers, String title,
				 String description, Long date, String location) {
		this.id = id;
		this.owner = owner;
		this.invitedUsers = invitedUsers;
		this.title = title;
		this.description = description;
		this.date = date;
		this.location = location;
	}

	private Event(Parcel in) {
		id = in.readInt();
		owner = in.readParcelable(User.class.getClassLoader());
		invitedUsers = in.createTypedArrayList(InvitedUser.CREATOR);
		title = in.readString();
		description = in.readString();
		if (description.isEmpty()) {
			description = null;
		}
		date = in.readLong();
		if (date < 0) {
			date = null;
		}
		location = in.readString();
		if (location.isEmpty()) {
			location = null;
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public List<InvitedUser> getInvitedUsers() {
		return invitedUsers;
	}
	public void setInvitedUsers(List<InvitedUser> invitedUsers) {
		this.invitedUsers = invitedUsers;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeParcelable(owner, flags);
		out.writeTypedList(invitedUsers);
		out.writeString(title);
		if (description == null) {
			out.writeString("");
		}
		else {
			out.writeString(description);
		}
		if (date == null) {
			out.writeLong(-1);
		}
		else {
			out.writeLong(date);
		}
		if (location == null) {
			out.writeString("");
		}
		else {
			out.writeString(location);
		}
	}

	public static final Parcelable.Creator<Event> CREATOR =
			new Parcelable.Creator<Event>() {
				@Override
				public Event createFromParcel(Parcel in) {
					return new Event(in);
				}

				@Override
				public Event[] newArray(int size) {
					return new Event[size];
				}
			};
}
