package hu.bme.xj4vjg.catchup.api;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
	public static final String ID_KEY = "id";
	public static final String EMAIL_KEY = "email";
	public static final String FIRST_NAME_KEY = "firstName";
	public static final String LAST_NAME_KEY = "lastName";
	
	private int id;
	private String email;
	private String firstName;
	private String lastName;
	
	public User() { }
	
	public User(int id, String email, String firstName, String lastName) {
		this.id = id;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	private User(Parcel in) {
		id = in.readInt();
		email = in.readString();
		firstName = in.readString();
		lastName = in.readString();
	}

    public String getFullName() {
        return lastName + " " + firstName;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeString(email);
		out.writeString(firstName);
		out.writeString(lastName);
	}

	public static final Parcelable.Creator<User> CREATOR =
			new Parcelable.Creator<User>() {
		@Override
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		@Override
		public User[] newArray(int size) {
			return new User[size];
		}
	};
}
