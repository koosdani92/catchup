package hu.bme.xj4vjg.catchup.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.LoginResponse;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.BasicLoginTask;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.FacebookLoginTask;
import hu.bme.xj4vjg.catchup.apihelper.GoogleLoginTask;
import hu.bme.xj4vjg.catchup.apihelper.LoginTaskListener;
import hu.bme.xj4vjg.catchup.apihelper.LoginType;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;
import hu.bme.xj4vjg.catchup.apihelper.UpdateGcmTokenTask;
import hu.bme.xj4vjg.catchup.service.RegistrationIntentService;
import info.hoang8f.widget.FButton;

/**
 * A induló Activity. Ezen keresztül tud a felhasználó bejelentkezni.
 */
public class MainActivity extends AppCompatActivity implements
        LoginTaskListener,
        UpdateGcmTokenTask.UpdateGcmTokenListener {

    // Intent extraként érkezéső boolean érték, amely jelzi, hogy a User kijelentkezett
    public static final String USER_LOGGED_OUT_EXTRA_KEY = "UserLoggedOutKey";
    // Intent extraként érkezéső String érték, amely jelzi, hogy a User-t milyen módszerrel kell
    // kijelentkeztetni(LoginType)
    public static final String LOGOUT_TYPE_EXTRA_KEY = "LogoutType";
    // Intent extraként érkezéső boolean érték, amely jelzi, alkalmazás hiba miatt jelent meg az
    // Activity
    public static final String APP_ERROR_EXTRA_KEY = "AppErrorKey";

    // A Bejelentkezett felhasználó adatai és a felhasznált bejelentkezési mód típusa
    private User user = null;
    private LoginType loginType = null;

    // API objektum
    private ICatchUpApi catchUpApi;
    // Login folyamatokat elvégző objektumok
    private BasicLoginTask basicLoginTask;
    private FacebookLoginTask facebookLoginTask;
    private GoogleLoginTask googleLoginTask;
    private UpdateGcmTokenTask updateGcmTokenTask;

    // BroadcastReceiver a GCM token megszerzésére
    private BroadcastReceiver gcmRegistrationBroadcastReceiver;

    // UI vezérlők
    private FButton basicLoginButton;
    private FButton googleSignInButton;
    private FButton facebookSignInButton;
    private FButton registerButton;
    private EditText emailInput;
    private EditText passwordInput;
    private CheckBox autoLoginCheckbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Facebook SDK inicializálás
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        // Bejelentkezést vezérlő osztályok inicializálása
        basicLoginTask = new BasicLoginTask(this);
        facebookLoginTask = new FacebookLoginTask(this, this);
        googleLoginTask = new GoogleLoginTask(this, this);
        updateGcmTokenTask = new UpdateGcmTokenTask(this);

        // GCM BR létrehozása
        gcmRegistrationBroadcastReceiver = new GcmRegistrationBroadcastReceiver();

        // Vezérlők és onClickListenerek beállítása
        emailInput = (EditText) findViewById(R.id.email_text);
        passwordInput = (EditText) findViewById(R.id.password_text);
        basicLoginButton = (FButton) findViewById(R.id.basic_login_button);
        basicLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBasicLoginClick();
            }
        });
        googleSignInButton = (FButton) findViewById(R.id.google_sign_in_button);
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGoogleLoginClick();
            }
        });
        facebookSignInButton = (FButton) findViewById(R.id.facebook_sign_in_button);
        facebookSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFacebookLoginClick();
            }
        });
        autoLoginCheckbox = (CheckBox) findViewById(R.id.autologin_checkbox);
        registerButton = (FButton) findViewById(R.id.register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRegisterClick();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Bundle extras = (getIntent() == null || getIntent().getExtras() == null)
                ? new Bundle() : getIntent().getExtras();

        // API inicializálása
        catchUpApi = CatchUpApiGenerator.generateApi(this);

        // API objektum átadása a LoginProcessor-oknak
        basicLoginTask.setApi(catchUpApi);
        googleLoginTask.setApi(catchUpApi);
        facebookLoginTask.setApi(catchUpApi);
        updateGcmTokenTask.setApi(catchUpApi);

        // Ellenőrizzük, hogy alkalmazás hiba miatt jelent-e meg az Activity
        if (extras.getBoolean(APP_ERROR_EXTRA_KEY, false)) {
            onApplicationError();
            return;
        }

        // Megvizsgáljuk az Intent-et, hogy a User kijelentkezett-e,
        // és ezért került előtérbe az Activity
        if (extras.getBoolean(USER_LOGGED_OUT_EXTRA_KEY, false)) {
            autoLoginCheckbox.setChecked(false);
            PreferencesHelper.clearAutoLoginData(this);
            PreferencesHelper.remove(this, PreferencesHelper.LOGGED_IN_USERID_KEY);

            // Ha a User SocialLogin-al jelentkezett be, akkor kijelentkeztetjük az adott hálózatból
            if (extras.getString(LOGOUT_TYPE_EXTRA_KEY, "").equals(LoginType.Facebook)) {
                facebookLoginTask.logout();
            } else if (extras.getString(LOGOUT_TYPE_EXTRA_KEY, "").equals(LoginType.Google)) {
                googleLoginTask.logout();
            } else if (!extras.getString(LOGOUT_TYPE_EXTRA_KEY, "").equals(LoginType.Basic)) {
                facebookLoginTask.logout();
                googleLoginTask.logout();
            }
        }

        // Ha be van kapcsolva az Autologin funkció, akkor bejelentkeztetjük a felhasználót
        boolean autologinError = false;
        if (PreferencesHelper.getBoolean(this, PreferencesHelper.AUTO_LOGIN_ENABLED, false)) {
            disableUi();
            String loginType = PreferencesHelper.getString(
                    this,
                    PreferencesHelper.AUTO_LOGIN_TYPE,
                    "");

            if (loginType.equals(LoginType.Facebook.toString())) {
                String facebookId = PreferencesHelper.getString(
                        this, PreferencesHelper.FACEBOOK_LOGIN_ID, null);
                if (facebookId != null) {
                    facebookLoginTask.run();
                }
                else {
                    autologinError = true;
                }
            } else if (loginType.equals(LoginType.Google.toString())) {
                String googleId = PreferencesHelper.getString(
                        this, PreferencesHelper.GOOGLE_LOGIN_ID, null);
                if (googleId != null) {
                    googleLoginTask.run();
                }
                else {
                    autologinError = true;
                }
            } else if (loginType.equals(LoginType.Basic.toString())) {
                String email = PreferencesHelper.getString(
                        this, PreferencesHelper.BASIC_LOGIN_EMAIL, null);
                String password = PreferencesHelper.getString(
                        this, PreferencesHelper.BASIC_LOGIN_PASSWORD, null);
                if (email != null && password != null) {
                    basicLoginTask.run(email, password);
                }
                else {
                    autologinError = true;
                }
            } else {
                autologinError = true;
            }
        }

        if (autologinError) {
            enableUi();
            PreferencesHelper.clearAutoLoginData(this);
            Toast.makeText(
                    getApplicationContext(),
                    R.string.auto_login_error,
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_login);
        CatchUpApplication.setApplicationVisible(true);

        // GCM regisztrációs folyamatot figyeli BR regisztrálása
        LocalBroadcastManager.getInstance(this).registerReceiver(gcmRegistrationBroadcastReceiver,
                new IntentFilter(getString(R.string.gcm_registration_complete_action)));
    }

    @Override
    protected void onPause() {
        CatchUpApplication.setApplicationVisible(false);

        // GCM regisztrációs folyamatot figyeli BR kiregisztrálása
        LocalBroadcastManager.getInstance(this).unregisterReceiver(gcmRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, ApiSettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    // Alkalmazás hibát kezelő függvény.
    private void onApplicationError() {
        ((CatchUpApplication) getApplication())
                .trackErrorEvent(R.string.analytics_event_global_application_error);

        PreferencesHelper.clearAutoLoginData(this);
        PreferencesHelper.remove(this, PreferencesHelper.LOGGED_IN_USERID_KEY);
        enableUi();
        Toast.makeText(
                getApplicationContext(),
                R.string.server_error,
                Toast.LENGTH_SHORT).show();
    }

    // Ezt a függvényt hívja a többi alkalmazás komponens ha bármi hiba történik a rendszerben.
    // Indít egy MainActivity-t és jelzi a hibát a felhasználónak
    public static void sendApplicationError(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(APP_ERROR_EXTRA_KEY, true);
        context.startActivity(intent);
    }

    // A beépített felhasználónév/jelszó párosra épülő bejelentkezést indíto gomb eseménykezelője
    private void onBasicLoginClick() {
        if (emailInput.getText() == null || emailInput.getText().toString().isEmpty()) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.login_error_empty_email,
                    Toast.LENGTH_SHORT).show();
        } else if (passwordInput.getText() == null || passwordInput.getText().toString().isEmpty()) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.login_error_empty_password,
                    Toast.LENGTH_SHORT).show();
        } else {
            showLoggingIn();
            disableUi();
            basicLoginTask.run(
                    emailInput.getText().toString(),
                    passwordInput.getText().toString());
        }
    }

    // A Google alapú bejelentkezést indíto gomb eseménykezelője
    private void onGoogleLoginClick() {
        showLoggingIn();
        disableUi();
        googleLoginTask.run();
    }

    // A Google alapú bejelentkezést indíto gomb eseménykezelője
    private void onFacebookLoginClick() {
        showLoggingIn();
        disableUi();
        facebookLoginTask.run();
    }

    // A regisztrációs felületet előhozó gomb eseménykezelője
    private void onRegisterClick() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    // Bejelentkezési folyamat jelzése a felhasználónak
    private void showLoggingIn() {
        Toast.makeText(
                getApplicationContext(),
                R.string.login_progress,
                Toast.LENGTH_SHORT).show();
    }

    // UI gombok blokkolása a bejelentkezés idejére
    private void disableUi() {
        emailInput.setEnabled(false);
        passwordInput.setEnabled(false);
        basicLoginButton.setEnabled(false);
        googleSignInButton.setEnabled(false);
        facebookSignInButton.setEnabled(false);
        autoLoginCheckbox.setEnabled(false);
        registerButton.setEnabled(false);
    }

    // UI gombok engedélyezése a bejelentkezés végeztével
    private void enableUi() {
        emailInput.setEnabled(true);
        passwordInput.setEnabled(true);
        basicLoginButton.setEnabled(true);
        googleSignInButton.setEnabled(true);
        facebookSignInButton.setEnabled(true);
        autoLoginCheckbox.setEnabled(true);
        registerButton.setEnabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(this.getClass().toString(), "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        // Érteítjük a Google, illetve Facebook login processorokat az esetleges bejelentkezési adatokról
        facebookLoginTask.onActivityResult(requestCode, resultCode, data);
        googleLoginTask.onActivityResult(requestCode, resultCode);
    }

    // LoginTaskListener implementáció.
    // Ebben a függvényben értesülünk, ha egy bejelentkezési folyamat az Activity számára érdekes
    //  állapotba került.
    @Override
    public void onLoginFinished(LoginType type, LoginResponse response, ApiException exception) {
        // Ha nem várt API exception volt akkor megjelenítjük a hibát
        if (exception != null) {
            PreferencesHelper.clearAutoLoginData(this);
            enableUi();
            Toast.makeText(
                    getApplicationContext(),
                    R.string.server_error,
                    Toast.LENGTH_SHORT).show();
        // Ha nem volt API exception
        }
        else {
            // Ha nem sikerült a bejelentkezés
            if (!response.getLoginResult()) {
                if (type == LoginType.Basic) {
                    ((CatchUpApplication) getApplication())
                            .trackActionEvent(R.string.analytics_event_basic_login_fail);
                } else if (type == LoginType.Google) {
                    ((CatchUpApplication) getApplication())
                            .trackActionEvent(R.string.analytics_event_google_login_fail);
                } else if (type == LoginType.Facebook) {
                    ((CatchUpApplication) getApplication())
                            .trackActionEvent(R.string.analytics_event_facebook_login_fail);
                }

                PreferencesHelper.clearAutoLoginData(this);
                enableUi();
                // Ha BasicLogin folyamat miatt nem sikerült a bejelentkezés akkor rossz felhasználói
                // adatok lettek megadva
                // Közösségi hálózatok esetén Cancel történt, amit nem kell külön lekezelni
                if (type == LoginType.Basic) {
                    Toast.makeText(
                            getApplicationContext(),
                            R.string.login_error_wrong_credentials,
                            Toast.LENGTH_SHORT).show();
                }
            }
            // Ha sikerült a bejelentkezés
            else {
                // Elmentjük a User és a bejelentkezés adatait
                this.user = response.getUser();
                this.loginType = type;

                // Beállítjuk az Autologin funkcióhoz a Login típust
                PreferencesHelper.putString(this, PreferencesHelper.AUTO_LOGIN_TYPE, type.toString());

                // Ha van GCM token az eszközön, akkor hozzárendeljük a felhasználóhoz
                // és elmentjük az azonosíját
                PreferencesHelper.putInt(
                        getApplicationContext(),
                        PreferencesHelper.LOGGED_IN_USERID_KEY, response.getUserId());
                if (PreferencesHelper.contains(getApplicationContext(), PreferencesHelper.GCM_TOKEN_KEY)) {
                    updateGcmTokenTask.run(
                            response.getUserId(),
                            PreferencesHelper.getString(
                                    getApplicationContext(), PreferencesHelper.GCM_TOKEN_KEY, ""));

                }
                // Ha még nincs az eszközön GCM token, akkor indítunk egy Service-t ami lekéri,
                // egy BroadcastReceiver pedig értesülni fog a token megszerzéséről
                else {
                    Intent intent = new Intent(this, RegistrationIntentService.class);
                    intent.putExtra(RegistrationIntentService.LOGGING_IN_GCM_TOKEN_KEY, true);
                    startService(intent);
                }
            }
        }
    }

    private class GcmRegistrationBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Ha sikerült gcm tokent szerezni, akkor frissítjük a szerveren
            if (PreferencesHelper.contains(getApplicationContext(), PreferencesHelper.GCM_TOKEN_KEY)) {
                updateGcmTokenTask.run(
                        PreferencesHelper.getInt(
                                getApplicationContext(), PreferencesHelper.LOGGED_IN_USERID_KEY, -1),
                        PreferencesHelper.getString(
                                getApplicationContext(), PreferencesHelper.GCM_TOKEN_KEY, ""));
            }
            // Ha nem sikerült tokent szerezni, akkor hibát jelzünk
            else {
                enableUi();
                PreferencesHelper.remove(getApplicationContext(), PreferencesHelper.LOGGED_IN_USERID_KEY);
                Toast.makeText(
                        getApplicationContext(),
                        R.string.server_error,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    // UpdateGcmTokenListener implementáció
    // Ez a függvény a bejelentkezési folyamat vége, itt kell indítani a következő Activity-t
    @Override
    public void onUpdateGcmTokenFinished(Boolean response, ApiException exception) {
        enableUi();
        // Hibakezelés
        if (exception != null || !response) {
            onApplicationError();
            return;
        }

        // Sikeres volt a bejelentkezési folyamat, a felhasználóhoz hozzá van rendelve az eszközhöz
        // tartozó GCM token
        // A FELHASZNÁLÓ TOVÁBB LÉPHET A KÖVETKEZŐ ACTIVITY-RE

        // Ha a felhasználó bekapcsolta az Autologint, akkor elmentjük a legközelebbi
        // bejelentkezéséhez szükséges adatokat a SharedPreferences-be
        if (autoLoginCheckbox.isChecked()) {
            PreferencesHelper.putBoolean(this, PreferencesHelper.AUTO_LOGIN_ENABLED, true);
            if (PreferencesHelper.getString(this, PreferencesHelper.AUTO_LOGIN_TYPE, "")
                    .equals(LoginType.Basic.toString())) {
                PreferencesHelper.putString(this,
                        PreferencesHelper.BASIC_LOGIN_EMAIL,
                        emailInput.getText().toString());
                PreferencesHelper.putString(this,
                        PreferencesHelper.BASIC_LOGIN_PASSWORD,
                        passwordInput.getText().toString());
            }
        }
        else {
            PreferencesHelper.clearAutoLoginData(this);
        }

        // TabActivity megnyitása a User adataival
        if (user != null && loginType != null) {
            navigateToTabActivity();
        }
        else {
            onApplicationError();
        }
    }

    private void navigateToTabActivity() {
        if (loginType == LoginType.Basic) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_basic_login_success);
        } else if (loginType == LoginType.Google) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_google_login_success);
        } else if (loginType == LoginType.Facebook) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_facebook_login_success);
        }

        Intent intent = new Intent(this, TabActivity.class);
        intent.putExtra(TabActivity.USER_EXTRA_KEY, user);
        intent.putExtra(TabActivity.LOGIN_TYPE_EXTRA_KEY, loginType.toString());
        startActivity(intent);
        finish();
    }
}