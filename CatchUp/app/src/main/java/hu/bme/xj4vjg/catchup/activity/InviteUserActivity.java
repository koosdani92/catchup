package hu.bme.xj4vjg.catchup.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.adapter.InviteUserAdapter;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.InvitedUser;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.GetFriendsTask;
import hu.bme.xj4vjg.catchup.apihelper.SendEventInviteTask;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;

public class InviteUserActivity extends AppCompatActivity implements SendEventInviteTask.SendEventInviteListener, InviteUserAdapter.OnItemClickListener, GetFriendsTask.GetFriendsListener {
    public static final String USER_EXTRA_KEY = "User";
    public static final String EVENT_ID_EXTRA_KEY = "EventId";
    public static final String INVITED_USERS_EXTRA_KEY = "Friends";

    private User user = null;
    private int eventId = -1;
    private List<InvitedUser> invitedUsers;
    private List<InvitedUser> friends;
    private InviteUserAdapter inviteUserAdapter;

    private ICatchUpApi catchUpApi;
    private SendEventInviteTask sendEventInviteTask;
    private GetFriendsTask getFriendsTask;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        friends = new ArrayList<InvitedUser>();
        sendEventInviteTask = new SendEventInviteTask(this);
        getFriendsTask = new GetFriendsTask(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        inviteUserAdapter = new InviteUserAdapter(friends);
        inviteUserAdapter.setListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(inviteUserAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle extras = (getIntent() == null || getIntent().getExtras() == null)
                ? new Bundle() : getIntent().getExtras();
        if (extras.containsKey(USER_EXTRA_KEY)
                && extras.containsKey(EVENT_ID_EXTRA_KEY)
                && extras.containsKey(INVITED_USERS_EXTRA_KEY)) {
            user = extras.getParcelable(USER_EXTRA_KEY);
            eventId = extras.getInt(EVENT_ID_EXTRA_KEY);
            invitedUsers = extras.getParcelableArrayList(INVITED_USERS_EXTRA_KEY);
        }
        else if (user == null || friends == null) {
            MainActivity.sendApplicationError(this);
            return;
        }

        catchUpApi = CatchUpApiGenerator.generateApi(this);
        sendEventInviteTask.setApi(catchUpApi);
        getFriendsTask.setApi(catchUpApi);

        getFriendsTask.run(user.getId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_invite_users);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGetFriendsFinished(List<User> response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this);
            return;
        }

        friends.addAll(invitedUsers);
        for (User friend : response) {
            boolean friendIsInvited = false;
            for (int i = 0; i < invitedUsers.size(); i++) {
                if (invitedUsers.get(i).getUser().getId() == friend.getId()) {
                    friendIsInvited = true;
                    break;
                }
            }

            if (!friendIsInvited) {
                friends.add(new InvitedUser(friend, null));
            }
        }

        inviteUserAdapter.notifyDataSetChanged();
    }

    @Override
    public void onInviteUser(int userId) {
        sendEventInviteTask.run(user.getId(), eventId, userId);
    }

    @Override
    public void onSendEventInviteFinished(Boolean response, int invitedId, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this);
        }

        if (response != null && response) {
            int i = 0;
            while (i < friends.size()) {
                if (friends.get(i).getUser().getId() == invitedId) {
                    friends.get(i).setInvitationState(EventInvitationState.Pending);
                    break;
                }
                i++;
            }
            inviteUserAdapter.notifyDataSetChanged();
        }
        else {
            Toast.makeText(this,
                    getString(R.string.event_invite_not_sent),
                    Toast.LENGTH_SHORT).show();
        }
    }
}
