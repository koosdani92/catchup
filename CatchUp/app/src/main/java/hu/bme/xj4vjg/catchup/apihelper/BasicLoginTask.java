package hu.bme.xj4vjg.catchup.apihelper;

import android.util.Log;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.BasicLoginRequest;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.LoginResponse;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A beépített bejelentkezési folyamatot végző objektum.
 */
public class BasicLoginTask extends AbstractApiTask {
    private LoginTaskListener listener;

    public BasicLoginTask(LoginTaskListener listener) {
        this.listener = listener;
    }

    // Bejelentkezés indítása az API-nál
    public void run(String email, String password) {
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            notifyListener(
                    LoginType.Basic,
                    new LoginResponse(false, -1),
                    new ApiException(ApiException.ApiExceptionType.Unknown));
            return;
        }

        Callback<RestResponse<LoginResponse>> callback = new Callback<RestResponse<LoginResponse>>() {
            @Override
            public void success(RestResponse<LoginResponse> result, Response response) {
                Log.d("debug_key", "Retro OK");
                notifyListener(LoginType.Basic, result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("debug_key", "Retro error");
                notifyListener(
                        LoginType.Basic,
                        null,
                        new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.basicLogin(new BasicLoginRequest(email, password), callback);
    }

    // Listener értesítése a regisztráció eredményéről
    protected void notifyListener(LoginType type, LoginResponse response, ApiException exception) {
        Log.d("debug_key", "BasicLogin Notify listener");
        if (listener != null) {
            listener.onLoginFinished(type, response, exception);
        }
    }
}
