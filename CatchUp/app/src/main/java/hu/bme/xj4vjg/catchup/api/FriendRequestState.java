package hu.bme.xj4vjg.catchup.api;

public enum FriendRequestState {
	Pending, Accepted, Declined
}
