package hu.bme.xj4vjg.catchup.asynctask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.apihelper.EventDetail;

public class MultipleGeocodeTask extends AsyncTask<Void, Void, List<EventDetail>> {
    public static final String TAG = "MultipleGeocodeTask";

    public interface MultipleGeocodeListener {
        void onMultipleGeocodeFinished(List<EventDetail> eventDetails);
    }

    private MultipleGeocodeListener listener;
    private Context context;
    private List<Event> events;
    private int maxResult = 1;

    public MultipleGeocodeTask(Context context, List<Event> events) {
        this.context = context;
        this.events = events;
    }

    public void setListener(MultipleGeocodeListener listener) {
        this.listener = listener;
    }

    @Override
    protected List<EventDetail> doInBackground(Void... params) {
        List<EventDetail> eventDetails = new ArrayList<EventDetail>();

        Geocoder geocoder = new Geocoder(context);
        try {
            for (Event event : events) {
                Address address = null;
                List<Address> currentEventAddresses =
                        geocoder.getFromLocationName(event.getLocation(), maxResult);
                if (!currentEventAddresses.isEmpty()) {
                    address = currentEventAddresses.get(0);
                }
                eventDetails.add(new EventDetail(event.getId(), address, event.getDate()));
            }
        } catch (IOException e) {
            Log.d(TAG, context.getString(R.string.geocode_error));
        }

        return eventDetails;
    }

    @Override
    protected void onPostExecute(List<EventDetail> eventDetails) {
        if (listener != null) {
            listener.onMultipleGeocodeFinished(eventDetails);
        }
    }
}