package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.AuthRequest;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.GetEventRequest;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.User;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetUserTask extends AbstractApiTask {
    public interface GetUserListener {
        void onGetUserFinished(User response, ApiException exception);
    }

    private GetUserListener listener;

    public GetUserTask(GetUserListener listener) {
        this.listener = listener;
    }

    public void run(int userId) {
        Callback<RestResponse<User>> callback = new Callback<RestResponse<User>>() {
            @Override
            public void success(RestResponse<User> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.getUser(new AuthRequest(userId), callback);
    }

    protected void notifyListener(User response, ApiException exception) {
        if (listener != null) {
            listener.onGetUserFinished(response, exception);
        }
    }
}
