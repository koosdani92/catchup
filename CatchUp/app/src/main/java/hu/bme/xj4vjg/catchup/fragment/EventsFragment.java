package hu.bme.xj4vjg.catchup.fragment;

import android.app.PendingIntent;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.activity.CreateEventActivity;
import hu.bme.xj4vjg.catchup.activity.EventDetailsActivity;
import hu.bme.xj4vjg.catchup.activity.MainActivity;
import hu.bme.xj4vjg.catchup.adapter.EventAdapter;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.EventDetail;
import hu.bme.xj4vjg.catchup.apihelper.GetEventsTask;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;
import hu.bme.xj4vjg.catchup.asynctask.MultipleGeocodeTask;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;
import hu.bme.xj4vjg.catchup.service.GeofenceTransitionsIntentService;

public class EventsFragment extends Fragment implements
        EventAdapter.OnItemClickListener,
        TabFragment,
        GetEventsTask.GetEventsListener,
        MultipleGeocodeTask.MultipleGeocodeListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final String USER_ARG = "User";
    private static final int GEOFENCE_RADIUS_IN_METERS = 1000;

    private User user;
    private ICatchUpApi catchUpApi;
    private List<Event> events;
    private EventAdapter eventAdapter;
    private GoogleApiClient googleApiClient;

    private GetEventsTask getEventsTask;
    private MultipleGeocodeTask multipleGeocodeTask;

    private RecyclerView recyclerView;
    private FloatingActionButton createEventButton;

    public static EventsFragment newInstance(User userArg) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putParcelable(USER_ARG, userArg);
        fragment.setArguments(args);
        return fragment;
    }

    public EventsFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(USER_ARG);
        }

        getEventsTask = new GetEventsTask(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);

        // UI vezérlők elmentése
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        createEventButton = (FloatingActionButton) view.findViewById(R.id.create_event_button);

        // RecycleView beállítása
        events = new ArrayList<Event>();
        eventAdapter = new EventAdapter(this.getContext(), user, events);
        eventAdapter.setListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(eventAdapter);

        // FloatingActionButton onclicklistener
        createEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateEventClick();
            }
        });

        return view;
    }

    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this.getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }

        catchUpApi = CatchUpApiGenerator.generateApi(getContext());
        getEventsTask.setApi(catchUpApi);

        getEventsTask.run(user.getId());
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CatchUpApplication) getActivity().getApplication()).trackScreen(R.string.analytics_screen_events);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    // GetEvents task callback
    @Override
    public void onGetEventsFinished(List<Event> response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this.getContext());
        }

        if (response != null) {
            if (googleApiClient == null) {
                buildGoogleApiClient();
                googleApiClient.connect();
            }
            else if (googleApiClient.isConnected()) {
                manageGeofences();
            }
            else if (!googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }

            events.clear();
            events.addAll(response);
            eventAdapter.notifyDataSetChanged();
        }
    }

    private void manageGeofences() {
        removeExpiredGeofences();
        registerNewGeofences();
    }

    private void removeExpiredGeofences() {
        Set<String> geofencedEventIds = PreferencesHelper.getGeofenceEventSet(this.getContext());
        Set<String> removableEventIds = new HashSet<String>();
        long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();

        for (String geofencedEventId : geofencedEventIds) {
            boolean geofencedEventExists = false;
            for (Event event : events) {
                if (event.getId() == Integer.parseInt(geofencedEventId)) {
                    geofencedEventExists = true;
                    if (currentTimeInMillis > event.getDate()) {
                        removableEventIds.add(geofencedEventId);
                    }
                }
            }
            if (!geofencedEventExists) {
                removableEventIds.add(geofencedEventId);
            }
        }

        if (!removableEventIds.isEmpty()) {
            geofencedEventIds.removeAll(removableEventIds);
            PreferencesHelper.putGeofenceEventSet(this.getContext(), geofencedEventIds);

            LocationServices.GeofencingApi.removeGeofences(
                    googleApiClient,
                    new ArrayList<String>(removableEventIds));
        }
    }

    private void registerNewGeofences() {
        Set<String> geofencedEventIds = PreferencesHelper.getGeofenceEventSet(this.getContext());
        List<Event> notGeofencedEvents = new ArrayList<Event>();
        long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();

        for (Event event : events) {
            boolean eventIsGeofenced = false;
            for (String geofencedEventId : geofencedEventIds) {
                if (event.getId() == Integer.parseInt(geofencedEventId)) {
                    eventIsGeofenced = true;
                }
            }
            if (!eventIsGeofenced) {
                if (event.getLocation() != null
                        && event.getDate() != null
                        && currentTimeInMillis < event.getDate()) {
                    notGeofencedEvents.add(event);
                }
            }
        }

        for (Event event : notGeofencedEvents) {
            geofencedEventIds.add(Integer.toString(event.getId()));
        }
        if (!notGeofencedEvents.isEmpty()) {
            PreferencesHelper.putGeofenceEventSet(this.getContext(), geofencedEventIds);

            multipleGeocodeTask = new MultipleGeocodeTask(this.getContext(), notGeofencedEvents);
            multipleGeocodeTask.setListener(this);
            multipleGeocodeTask.execute();
        }
    }

    @Override
    public void onMultipleGeocodeFinished(List<EventDetail> eventDetails) {
        long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();

        for (EventDetail eventDetail : eventDetails) {
            if (eventDetail.getAddress() == null) {
                eventDetails.remove(eventDetail);
            }
        }

        List<Geofence> geofences = new ArrayList<Geofence>();
        for (EventDetail eventDetail : eventDetails) {
            double latitude = eventDetail.getAddress().getLatitude();
            double longitude = eventDetail.getAddress().getLongitude();
            geofences.add(new Geofence.Builder()
                    .setRequestId(Integer.toString(eventDetail.getId()))
                    .setCircularRegion(
                            latitude,
                            longitude,
                            GEOFENCE_RADIUS_IN_METERS
                    )
                    .setExpirationDuration(eventDetail.getDate() - currentTimeInMillis)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .build());

            Log.d("debug_key", "Geofence registered for lat: " + latitude + " long: " + longitude);
        }

        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofences);
        GeofencingRequest geofencingRequest =  builder.build();

        Intent intent = new Intent(this.getContext(), GeofenceTransitionsIntentService.class);
        PendingIntent pendingIntent = PendingIntent.getService(
                this.getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        LocationServices.GeofencingApi.addGeofences(
                googleApiClient,
                geofencingRequest,
                pendingIntent);
    }

    @Override
    public void onConnected(Bundle bundle) {
        manageGeofences();
    }

    @Override
    public void onConnectionSuspended(int i) { }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        MainActivity.sendApplicationError(this.getContext());
    }

    @Override
    public void onEventSelected (Event selectedEvent) {
        ((CatchUpApplication) getActivity().getApplication())
                .trackActionEvent(R.string.analytics_event_view_event_details);

        Intent intent = new Intent(this.getContext(), EventDetailsActivity.class);
        intent.putExtra(EventDetailsActivity.USER_EXTRA_KEY, user);
        intent.putExtra(EventDetailsActivity.EVENT_ID_EXTRA_KEY, selectedEvent.getId());
        startActivity(intent);
    }

    // CreateEventButton eseménykezelő
    private void onCreateEventClick() {
        Intent intent = new Intent(this.getContext(), CreateEventActivity.class);
        intent.putExtra(CreateEventActivity.USER_EXTRA_KEY, user);
        startActivity(intent);
    }

    // Adatok frissítése
    @Override
    public void updateContent() {
        if (events != null && eventAdapter != null && recyclerView != null && getEventsTask != null) {
            getEventsTask.run(user.getId());
        }
    }
}