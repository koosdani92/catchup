package hu.bme.xj4vjg.catchup.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.InvitedUser;
import hu.bme.xj4vjg.catchup.api.UserDetail;

public class InviteUserAdapter extends RecyclerView.Adapter<InviteUserAdapter.InviteUserViewHolder> {
    public interface OnItemClickListener {
        void onInviteUser(int userId);
    };

    private List<InvitedUser> users;
    private OnItemClickListener listener;

    public class InviteUserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView inviteUserCardView;
        public TextView nameTextView;
        public ImageView goingImageView;
        public ImageView pendingImageView;
        public ImageView inviteUserImageView;
        public InviteUserViewHolder(View itemView) {
            super(itemView);
            inviteUserCardView = (CardView) itemView.findViewById(R.id.invite_user_card_view);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            goingImageView = (ImageView) itemView.findViewById(R.id.going_image_view);
            pendingImageView = (ImageView) itemView.findViewById(R.id.pending_image_view);
            inviteUserImageView = (ImageView) itemView.findViewById(R.id.invite_user_image_view);
            inviteUserImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null && !users.isEmpty()) {
                listener.onInviteUser(users.get(getAdapterPosition()).getUser().getId());
            }
        }
    }

    public InviteUserAdapter(List<InvitedUser> data) {
        this.users = data;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public InviteUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext())
                .inflate(R.layout.invite_user_row, parent, false);
        return new InviteUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InviteUserViewHolder holder, int position) {
        InvitedUser currentUser = users.get(position);
        holder.nameTextView.setText(currentUser.getUser().getFullName());

        holder.goingImageView.setVisibility(View.GONE);
        holder.pendingImageView.setVisibility(View.GONE);
        holder.inviteUserImageView.setVisibility(View.GONE);
        if (currentUser.getInvitationState() == null
                || currentUser.getInvitationState() == EventInvitationState.NotGoing) {
            holder.inviteUserImageView.setVisibility(View.VISIBLE);
        } else if (currentUser.getInvitationState() == EventInvitationState.Going) {
            holder.goingImageView.setVisibility(View.VISIBLE);
        } else if (currentUser.getInvitationState() == EventInvitationState.Pending) {
            holder.pendingImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}