package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.BasicLoginRequest;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.LoginResponse;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.UpdateGcmTokenRequest;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A beépített bejelentkezési folyamatot végző objektum.
 */
public class UpdateGcmTokenTask extends AbstractApiTask {
    public interface UpdateGcmTokenListener {
        void onUpdateGcmTokenFinished(Boolean response, ApiException exception);
    }

    private UpdateGcmTokenListener listener;

    public UpdateGcmTokenTask(UpdateGcmTokenListener listener) {
        this.listener = listener;
    }

    // GCM token frissítése az API-nál
    public void run(int userId, String gcmToken) {
        Callback<RestResponse<Boolean>> callback = new Callback<RestResponse<Boolean>>() {
            @Override
            public void success(RestResponse<Boolean> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.updateGcmToken(new UpdateGcmTokenRequest(userId, gcmToken), callback);
    }

    // Listener értesítése a frissítés eredményéről
    protected void notifyListener(Boolean response, ApiException exception) {
        if (listener != null) {
            listener.onUpdateGcmTokenFinished(response, exception);
        }
    }
}
