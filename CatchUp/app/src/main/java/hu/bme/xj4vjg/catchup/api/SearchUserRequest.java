package hu.bme.xj4vjg.catchup.api;

public class SearchUserRequest extends AuthRequest {
	private String searchString;
	
	public SearchUserRequest() { }
	
	public SearchUserRequest(int userId, String searchString) {
		super(userId);
		this.searchString = searchString;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
}
