package hu.bme.xj4vjg.catchup.activity;

import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.InvitedUser;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.GetEventTask;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;
import hu.bme.xj4vjg.catchup.asynctask.GeocodeTask;
import hu.bme.xj4vjg.catchup.fragment.CustomMapFragment;
import hu.bme.xj4vjg.catchup.fragment.InvitedUsersFragment;

public class EventDetailsActivity extends AppCompatActivity implements
        GetEventTask.GetEventListener,
        OnMapReadyCallback,
        LocationListener,
        GeocodeTask.GeocodeListener {
    public static final String USER_EXTRA_KEY = "UserKey";
    public static final String EVENT_ID_EXTRA_KEY = "EventIdKey";
    public static final String IS_NEARBY_EVENT_EXTRA_KEY = "IsNearbyEvent";
    private static final int LOCATION_REFRESH_TIME_IN_MILLIS = 5 * 1000;
    private static final int LOCATION_REFRESH_DISTANCE_IN_METERS = 5;

    private ICatchUpApi catchUpApi;
    private GetEventTask getEventTask;

    private User user = null;
    private int eventId = -1;
    private Event event = null;
    private LatLng eventLatLng = null;
    private GoogleMap googleMap;
    private Marker eventMarker;
    private Marker myLocationMarker;
    private Polyline eventLine;
    private float googleMapMarkerZoomLevel = 15;
    private boolean isNearbyEvent = false;
    private LocationManager locationManager;

    private ScrollView scrollView;
    private TextView eventTitleTextView;
    private TextView eventOwnerTextView;
    private TextView eventDescriptionTextView;
    private TextView eventDateTextView;
    private TextView eventDateDashTextView;
    private TextView eventTimeTextView;
    private TextView eventLocationTextView;
    private CustomMapFragment mapFragment;
    private CardView invitedUsersCardView;
    private TextView invitedUsersTextView;
    private TextView goingUsersTextView;
    private TextView pendingUsersTextView;
    private TextView notGoingUsersTextView;
    private FloatingActionButton inviteUsersButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        getEventTask = new GetEventTask(this);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        eventTitleTextView = (TextView) findViewById(R.id.event_title_text);
        eventOwnerTextView = (TextView) findViewById(R.id.event_owner_text);
        eventDescriptionTextView = (TextView) findViewById(R.id.event_description_text);
        eventDateTextView = (TextView) findViewById(R.id.event_date_text);
        eventDateDashTextView = (TextView) findViewById(R.id.event_date_dash_text);
        eventTimeTextView = (TextView) findViewById(R.id.event_time_text);
        eventLocationTextView = (TextView) findViewById(R.id.event_location_text);
        mapFragment = (CustomMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        invitedUsersCardView = (CardView) findViewById(R.id.invited_users_card_view);
        invitedUsersTextView = (TextView) findViewById(R.id.invited_users_text);
        goingUsersTextView = (TextView) findViewById(R.id.going_users_text);
        pendingUsersTextView = (TextView) findViewById(R.id.pending_users_text);
        notGoingUsersTextView = (TextView) findViewById(R.id.not_going_users_text);
        inviteUsersButton = (FloatingActionButton) findViewById(R.id.invite_users_button);

        mapFragment.setListener(new CustomMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                scrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
        invitedUsersCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInvitedUsersClick();
            }
        });
        inviteUsersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onInviteUsersClick();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle extras = (getIntent() == null || getIntent().getExtras() == null)
                ? new Bundle() : getIntent().getExtras();
        if (extras.containsKey(USER_EXTRA_KEY) && extras.containsKey(EVENT_ID_EXTRA_KEY)) {
            user = extras.getParcelable(USER_EXTRA_KEY);
            eventId = extras.getInt(EVENT_ID_EXTRA_KEY);
        }
        else if (user == null || eventId == -1) {
            MainActivity.sendApplicationError(this);
            return;
        }
        isNearbyEvent = extras.getBoolean(IS_NEARBY_EVENT_EXTRA_KEY, false);

        if (isNearbyEvent) {
            Log.d("debug_key", "Event is nearbyEvent");
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    LOCATION_REFRESH_TIME_IN_MILLIS,
                    LOCATION_REFRESH_DISTANCE_IN_METERS,
                    this);
        }

        catchUpApi = CatchUpApiGenerator.generateApi(this);
        getEventTask.setApi(catchUpApi);

        getEventTask.run(user.getId(), eventId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_event_details);
    }

    private void fillEventDetails() {
        eventTitleTextView.setText(event.getTitle());
        eventOwnerTextView.setText(event.getOwner().getFullName());
        if (event.getDescription() == null) {
            eventDescriptionTextView.setText("");
        }
        else {
            eventDescriptionTextView.setText(event.getDescription());
        }
        if (event.getDate() == null) {
            eventDateTextView.setText("");
            eventDateDashTextView.setVisibility(View.INVISIBLE);
            eventTimeTextView.setText("");
        }
        else {
            String dateString = null;
            String timeString = null;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(event.getDate());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormate = new SimpleDateFormat("HH:mm");
            dateString = dateFormat.format(calendar.getTime());
            timeString = timeFormate.format(calendar.getTime());

            eventDateTextView.setText(dateString);
            eventDateDashTextView.setVisibility(View.VISIBLE);
            eventTimeTextView.setText(timeString);
        }
        if (event.getLocation() == null) {
            eventLocationTextView.setText("");
        }
        else {
            eventLocationTextView.setText(event.getLocation());
        }
        invitedUsersTextView.setText(
                event.getInvitedUsers().size()
                        + " "
                        + getString(R.string.event_detail_invited));
        int goingUsers = 0;
        int pendingUsers = 0;
        int notGoingUsers = 0;
        for (InvitedUser invitedUser : event.getInvitedUsers()) {
            if (invitedUser.getInvitationState() == EventInvitationState.Going) {
                goingUsers++;
            } else if (invitedUser.getInvitationState() == EventInvitationState.Pending) {
                pendingUsers++;
            } else if (invitedUser.getInvitationState() != EventInvitationState.NotGoing) {
                notGoingUsers++;
            }
        }
        goingUsersTextView.setText(Integer.toString(goingUsers));
        pendingUsersTextView.setText(Integer.toString(pendingUsers));
        notGoingUsersTextView.setText(Integer.toString(notGoingUsers));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (event.getLocation() != null) {
            GeocodeTask geocodeTask = new GeocodeTask(this, event.getLocation(), true);
            geocodeTask.setListener(this);
            geocodeTask.execute();
        }
    }

    @Override
    public void onGeocodeFinished(List<Address> addresses, boolean drawMarker) {
        if (googleMap != null) {
            if (addresses.isEmpty()) {
                Toast.makeText(this, R.string.location_geocode_failed, Toast.LENGTH_SHORT).show();
            }
            else {
                Address address = addresses.get(0);
                eventLatLng = new LatLng(address.getLatitude(), address.getLongitude());

                if (drawMarker) {
                    if (eventMarker == null) {
                        eventMarker = googleMap.addMarker(new MarkerOptions()
                                .position(eventLatLng)
                                .title(getString(R.string.event_location))
                                .draggable(false));
                    }
                    else {
                        eventMarker.setPosition(eventLatLng);
                    }
                }

                googleMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(eventLatLng, googleMapMarkerZoomLevel));
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("debug_key", "Location changed");
        if (isNearbyEvent && eventLatLng != null) {
            LatLng myPosition = new LatLng(location.getLatitude(), location.getLongitude());
            if (myLocationMarker == null) {
                if (googleMap != null) {
                    myLocationMarker = googleMap.addMarker(new MarkerOptions()
                            .position(myPosition )
                            .title(getString(R.string.my_location))
                            .draggable(false));
                }
            }
            else {
                myLocationMarker.setPosition(myPosition );
            }

            PolylineOptions polylineOptions = new PolylineOptions()
                    .add(eventLatLng)
                    .add(new LatLng(location.getLatitude(), location.getLongitude()))
                    .color(Color.BLUE)
                    .width(20);
            if (eventLine == null) {
                eventLine = googleMap.addPolyline(polylineOptions);
            }
            else {
                eventLine.setPoints(polylineOptions.getPoints());
            }

            double centerLatitude =
                    eventLatLng.latitude - (eventLatLng.latitude - location.getLatitude()) / 2;
            double centerLongitude =
                    eventLatLng.longitude - (eventLatLng.longitude - location.getLongitude()) / 2;
            LatLng centerPosition = new LatLng(centerLatitude, centerLongitude);
            googleMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(centerPosition, googleMapMarkerZoomLevel));
        }
    }

    @Override
    public void onGetEventFinished(Event response, ApiException exception) {
        if (exception != null || response == null) {
            MainActivity.sendApplicationError(this);
            return;
        }

        event = response;
        if (user.getId() == event.getOwner().getId()) {
            inviteUsersButton.setVisibility(View.VISIBLE);
        }
        else {
            inviteUsersButton.setVisibility(View.GONE);
        }

        if (event.getLocation() != null) {
            mapFragment.getView().setVisibility(View.VISIBLE);
            mapFragment.getMapAsync(this);
        }
        else {
            mapFragment.getView().setVisibility(View.GONE);
        }

        fillEventDetails();
    }

    private void onInvitedUsersClick() {
        ((CatchUpApplication) getApplication())
                .trackActionEvent(R.string.analytics_event_view_invited_users_for_event);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prevInvitedUsersFragment =
                getSupportFragmentManager().findFragmentByTag(InvitedUsersFragment.TAG);
        if (prevInvitedUsersFragment != null) {
            fragmentTransaction.remove(prevInvitedUsersFragment);
        }
        fragmentTransaction.addToBackStack(null);

        DialogFragment invitedUsersFragment = InvitedUsersFragment.newInstance(event.getInvitedUsers());
        invitedUsersFragment.show(fragmentTransaction, InvitedUsersFragment.TAG);
    }

    private void onInviteUsersClick() {
        if (user.getId() == event.getOwner().getId()) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_invite_user_for_event);

            Intent intent = new Intent(this, InviteUserActivity.class);
            intent.putExtra(InviteUserActivity.USER_EXTRA_KEY, user);
            intent.putExtra(InviteUserActivity.EVENT_ID_EXTRA_KEY, event.getId());
            intent.putParcelableArrayListExtra(
                    InviteUserActivity.INVITED_USERS_EXTRA_KEY,
                    new ArrayList<InvitedUser>(event.getInvitedUsers()));

            startActivity(intent);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
    @Override
    public void onProviderEnabled(String provider) { }
    @Override
    public void onProviderDisabled(String provider) { }
}
