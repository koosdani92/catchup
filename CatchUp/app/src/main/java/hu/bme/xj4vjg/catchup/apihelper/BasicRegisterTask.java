package hu.bme.xj4vjg.catchup.apihelper;

import android.util.Log;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.BasicLoginRequest;
import hu.bme.xj4vjg.catchup.api.BasicRegisterRequest;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.LoginResponse;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A beépített regisztrációs folyamatot végző objektum.
 */
public class BasicRegisterTask extends AbstractApiTask {
    public interface BasicRegisterListener {
        void onBasicRegisterFinished(Boolean response, ApiException exception);
    }

    private BasicRegisterListener listener;

    public BasicRegisterTask(BasicRegisterListener listener) {
        this.listener = listener;
    }

        // Regisztráció indítása az API-nál
    public void run(String email, String password, String lastName, String firstName) {
        if (email == null || email.isEmpty() || password == null || password.isEmpty()
                || lastName == null || lastName.isEmpty() || firstName == null || firstName.isEmpty()) {
            notifyListener(Boolean.FALSE, null);
            return;
        }

        Callback<RestResponse<Boolean>> callback = new Callback<RestResponse<Boolean>>() {
            @Override
            public void success(RestResponse<Boolean> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.basicRegister(
                new BasicRegisterRequest(email, password, firstName, lastName),
                callback);
    }

    // Listener értesítése a regisztráció eredményéről
    protected void notifyListener(Boolean response, ApiException exception) {
        if (listener != null) {
            listener.onBasicRegisterFinished(response, exception);
        }
    }
}