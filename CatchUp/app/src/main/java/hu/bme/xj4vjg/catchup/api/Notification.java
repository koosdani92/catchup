package hu.bme.xj4vjg.catchup.api;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import hu.bme.xj4vjg.catchup.R;

public class Notification {
	private NotificationType type;
	private FriendRequest friendRequest;
	private EventInvite eventInvite;
	
	public Notification() { }
	
	public Notification(FriendRequest friendRequest) {
		this.type = NotificationType.FriendRequest;
		this.friendRequest = friendRequest;
		this.eventInvite = null;
	}
	
	public Notification(EventInvite eventInvite) {
		this.type = NotificationType.EventInvite;
		this.friendRequest = null;
		this.eventInvite = eventInvite;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public FriendRequest getFriendRequest() {
		return friendRequest;
	}

	public void setFriendRequest(FriendRequest friendRequest) {
		this.friendRequest = friendRequest;
	}
	
	public EventInvite getEventInvite() {
		return eventInvite;
	}

	public void setEventInvite(EventInvite eventInvite) {
		this.eventInvite = eventInvite;
	}

    // Az értesítés szöveges formába konvertálása
    public String toString(Context context, int userId) {
        StringBuilder builder = new StringBuilder();
        if (type == NotificationType.FriendRequest) {
            // Ha válaszoltak egy felkérésemre
            if (friendRequest.getRequester().getId() == userId) {
                builder.append(friendRequest.getRequested().getFullName());
                builder.append(" ");
                if (friendRequest.getState() == FriendRequestState.Accepted) {
                    builder.append(context.getString(R.string.notification_accepted));
                }
                else {
                    builder.append(context.getString(R.string.notification_declined));
                }
                builder.append(" ")
                        .append(context.getString(R.string.notification_answered_friend_request));
            }
            // Ha én kaptam a felkérést
            else {
                builder.append(friendRequest.getRequester().getFullName())
                        .append(" ")
                        .append(context.getString(R.string.notification_requested_for_friend));
            }
        }
        else {
            // Ha válaszoltak egy meghívásomra
            if (eventInvite.getEvent().getOwner().getId() == userId) {
                builder.append(eventInvite.getInvited().getFullName());
                builder.append(" ");
                if (eventInvite.getState() == EventInvitationState.Going) {
                    builder.append(context.getString(R.string.notification_accepted));
                }
                else {
                    builder.append(context.getString(R.string.notification_declined));
                }
                builder.append(" ")
                        .append(context.getString(R.string.notification_answered_to_event_invite_for))
                        .append(" '")
                        .append(eventInvite.getEvent().getTitle())
                        .append("'.");
            }
            // Ha én kaptam a meghívást
            else {
                builder.append(eventInvite.getEvent().getOwner().getFullName())
                        .append(" ")
                        .append(context.getString(R.string.notification_invited_to))
                        .append(" '")
                        .append(getEventInvite().getEvent().getTitle())
                        .append("' ")
                        .append(context.getString(R.string.notification_to_event));
            }
        }

        return builder.toString();
    }

    public long getDate(int userId) {
        long date = 0;
        if (type == NotificationType.FriendRequest) {
            // Ha válaszoltak
            if (friendRequest.getRequester().getId() == userId) {
                date = friendRequest.getAnswerDate();
            }
            // Ha felkértek
            else {
                date = friendRequest.getSendDate();
            }
        }
        else {
            // Ha válaszoltak
            if (eventInvite.getEvent().getOwner().getId() == userId) {
                date = eventInvite.getAnswerDate();
            }
            // Ha meghívtak
            else {
                date = eventInvite.getSendDate();
            }
        }

        return date;
    }

    public String getDateString(int userId) {
        long date = getDate(userId);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(calendar.getTime());
    }

    public boolean isPending() {
        if (type == NotificationType.FriendRequest
                && friendRequest.getState() == FriendRequestState.Pending) {
            return true;
        }
        if (type == NotificationType.EventInvite
                && eventInvite.getState() == EventInvitationState.Pending) {
            return true;
        }
        return false;
    }
}
