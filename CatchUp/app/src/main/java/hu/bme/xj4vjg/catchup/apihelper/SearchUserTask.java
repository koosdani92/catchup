package hu.bme.xj4vjg.catchup.apihelper;

import java.util.List;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.AuthRequest;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.SearchUserRequest;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.api.UserDetail;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchUserTask extends AbstractApiTask {
    public interface SearchUserListener {
        void onSearchUserFinished(List<UserDetail> response, ApiException exception);
    }

    private SearchUserListener listener;

    public SearchUserTask(SearchUserListener listener) {
        this.listener = listener;
    }

    public void run(int userId, String searchString) {
        Callback<RestResponse<List<UserDetail>>> callback = new Callback<RestResponse<List<UserDetail>>>() {
            @Override
            public void success(RestResponse<List<UserDetail>> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.searchUser(new SearchUserRequest(userId, searchString), callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(List<UserDetail> response, ApiException exception) {
        if (listener != null) {
            listener.onSearchUserFinished(response, exception);
        }
    }
}