package hu.bme.xj4vjg.catchup.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.activity.MainActivity;
import hu.bme.xj4vjg.catchup.adapter.FriendAdapter;
import hu.bme.xj4vjg.catchup.adapter.NotificationAdapter;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.EventInvite;
import hu.bme.xj4vjg.catchup.api.FriendRequest;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.Notification;
import hu.bme.xj4vjg.catchup.api.NotificationType;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.AcceptFriendRequestTask;
import hu.bme.xj4vjg.catchup.apihelper.AnswerEventInviteTask;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.DeclineFriendRequestTask;
import hu.bme.xj4vjg.catchup.apihelper.GetNotificationsTask;
import hu.bme.xj4vjg.catchup.apihelper.NotificationComparator;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;

public class NotificationsFragment extends Fragment implements
        GetNotificationsTask.GetNotificationsListener,
        AcceptFriendRequestTask.AcceptFriendRequestListener,
        DeclineFriendRequestTask.DeclineFriendRequestListener,
        AnswerEventInviteTask.AnswerEventInviteListener,
        TabFragment,
        NotificationAdapter.OnItemClickListener {
    public interface NotificationActionListener {
        void onNotificationAction(NotificationType notificationType);
    }

    private static final String USER_ARG = "User";

    private User user;
    // API objektum
    private ICatchUpApi catchUpApi;
    // Az értesítések listája
    private List<Notification> notifications;
    // Az értesítések listáját kezelő adapter
    private NotificationAdapter notificationAdapter;

    // Az értesítendő objektum, ha egy értesítés akció történik.
    private NotificationActionListener listener;

    private GetNotificationsTask getNotificationsTask;
    private AcceptFriendRequestTask acceptFriendRequestTask;
    private DeclineFriendRequestTask declineFriendRequestTask;
    private AnswerEventInviteTask answerEventInviteTask;

    // UI vezérlők
    private RecyclerView recyclerView;

    public static NotificationsFragment newInstance(User userArg) {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle args = new Bundle();
        args.putParcelable(USER_ARG, userArg);
        fragment.setArguments(args);
        return fragment;
    }

    public NotificationsFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(USER_ARG);
        }

        getNotificationsTask = new GetNotificationsTask(this);
        acceptFriendRequestTask = new AcceptFriendRequestTask(this);
        declineFriendRequestTask = new DeclineFriendRequestTask(this);
        answerEventInviteTask = new AnswerEventInviteTask(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        // UI vezérlők elmentése
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        // RecycleView beállítása
        notifications = new ArrayList<Notification>();
        notificationAdapter = new NotificationAdapter(this.getContext(), user, notifications);
        notificationAdapter.setListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(notificationAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        catchUpApi = CatchUpApiGenerator.generateApi(getContext());
        getNotificationsTask.setApi(catchUpApi);
        acceptFriendRequestTask.setApi(catchUpApi);
        declineFriendRequestTask.setApi(catchUpApi);
        answerEventInviteTask.setApi(catchUpApi);

        getNotificationsTask.run(user.getId());
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CatchUpApplication) getActivity().getApplication()).trackScreen(R.string.analytics_screen_notifications);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (NotificationActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NotificationActionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // GetNotifications task callback
    @Override
    public void onGetNotificationsFinished(List<Notification> response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this.getContext());
        }

        if (response != null) {
            notifications.clear();
            notifications.addAll(response);
            Collections.sort(notifications, new NotificationComparator(user.getId()));
            notificationAdapter.notifyDataSetChanged();
        }
    }

    // Notification interakció kezelése
    @Override
    public void onNotificationAction(Notification selectedNotification,
                                     NotificationAdapter.NotificationAction action) {
        ((CatchUpApplication) getActivity().getApplication())
                .trackActionEvent(R.string.analytics_event_notification_operation);

        int userId = user.getId();
        if (selectedNotification.getType() == NotificationType.FriendRequest) {
            FriendRequest friendRequest = selectedNotification.getFriendRequest();
            int requesterId = friendRequest.getRequester().getId();
            if (action == NotificationAdapter.NotificationAction.Accept) {
                acceptFriendRequestTask.run(userId, requesterId);
            }
            else {
                declineFriendRequestTask.run(userId, requesterId);
            }
        }
        else {
            EventInvite eventInvite = selectedNotification.getEventInvite();
            int eventId = eventInvite.getEvent().getId();
            EventInvitationState answer;
            if (action == NotificationAdapter.NotificationAction.Accept) {
                answer = EventInvitationState.Going;
            }
            else {
                answer = EventInvitationState.NotGoing;
            }
            answerEventInviteTask.run(userId, eventId, answer);
        }
    }

    // Adatok frissítése
    @Override
    public void updateContent() {
        if (notifications != null && notificationAdapter != null && recyclerView != null
                && getNotificationsTask != null) {
            getNotificationsTask.run(user.getId());
        }
    }

    // AcceptFriendRequest Task callback
    @Override
    public void onAcceptFriendRequestFinished(Boolean response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this.getContext());
        }

        if (response) {
            updateContent();
            if (listener != null) {
                listener.onNotificationAction(NotificationType.FriendRequest);
            }
        }
    }

    // DeclineFriendRequest Task callback
    @Override
    public void onDeclineFriendRequestFinished(Boolean response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this.getContext());
        }

        if (response) {
            updateContent();
            if (listener != null) {
                listener.onNotificationAction(NotificationType.FriendRequest);
            }
        }
    }

    // AnswerEventInvite Task callback
    @Override
    public void onAnswerEventInviteFinished(Boolean response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this.getContext());
        }

        if (response) {
            updateContent();
            if (listener != null) {
                listener.onNotificationAction(NotificationType.EventInvite);
            }
        }
    }
}
