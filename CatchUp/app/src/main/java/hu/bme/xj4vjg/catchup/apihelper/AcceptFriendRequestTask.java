package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.AnswerEventInviteRequest;
import hu.bme.xj4vjg.catchup.api.AnswerFriendRequestRequest;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AcceptFriendRequestTask extends AbstractApiTask {
    public interface AcceptFriendRequestListener {
        void onAcceptFriendRequestFinished(Boolean response, ApiException exception);
    }

    private AcceptFriendRequestListener listener;

    public AcceptFriendRequestTask(AcceptFriendRequestListener listener) {
        this.listener = listener;
    }

    public void run(int userId, int requesterId) {
        Callback<RestResponse<Boolean>> callback = new Callback<RestResponse<Boolean>>() {
            @Override
            public void success(RestResponse<Boolean> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.acceptFriendRequest(
                new AnswerFriendRequestRequest(userId, requesterId),
                callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(Boolean response, ApiException exception) {
        if (listener != null) {
            listener.onAcceptFriendRequestFinished(response, exception);
        }
    }
}