package hu.bme.xj4vjg.catchup.application;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import hu.bme.xj4vjg.catchup.R;

public class CatchUpApplication extends Application {
    private static boolean applicationVisible = false;
    private Tracker tracker;

    public static boolean isApplicationVisible() {
        return applicationVisible;
    }

    public static void setApplicationVisible(boolean visibility) {
        applicationVisible = visibility;
    }

    synchronized public Tracker getDefaultTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            tracker = analytics.newTracker(R.xml.global_tracker);
        }
        return tracker;
    }

    public void trackScreen(int screenResId) {
        tracker = getDefaultTracker();
        tracker.setScreenName(getString(screenResId));
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void trackActionEvent(int eventResId) {
        trackEvent(R.string.analytics_event_category_action, eventResId);
    }

    public void trackErrorEvent(int eventResId) {
        trackEvent(R.string.analytics_event_category_error, eventResId);
    }

    private void trackEvent(int categoryResId, int eventResId) {
        tracker = getDefaultTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(getString(categoryResId))
                .setAction(getString(eventResId))
                .build());
    }
}
