package hu.bme.xj4vjg.catchup.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.activity.MainActivity;
import hu.bme.xj4vjg.catchup.adapter.InvitedUserAdapter;
import hu.bme.xj4vjg.catchup.api.InvitedUser;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;

public class InvitedUsersFragment extends DialogFragment {
    public static final String TAG = "InvitedUsersFragment";
    public static final String INVITED_USERS_ARG_KEY = "InvitedUsers";

    private List<InvitedUser> invitedUsers = null;
    private InvitedUserAdapter invitedUserAdapter;

    private RecyclerView recyclerView;

    public static InvitedUsersFragment newInstance(List<InvitedUser> invitedUsers) {
        InvitedUsersFragment invitedUsersFragment = new InvitedUsersFragment();

        Bundle args = new Bundle();
        invitedUsersFragment.setArguments(args);
        args.putParcelableArrayList(INVITED_USERS_ARG_KEY, new ArrayList<InvitedUser>(invitedUsers));

        return invitedUsersFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            invitedUsers = getArguments().getParcelableArrayList(INVITED_USERS_ARG_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invited_users, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        if (invitedUsers == null) {
            MainActivity.sendApplicationError(this.getContext());
            return view;
        }

        invitedUserAdapter = new InvitedUserAdapter(invitedUsers);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(invitedUserAdapter);

        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CatchUpApplication) getActivity().getApplication()).trackScreen(R.string.analytics_screen_invited_users);
    }
}
