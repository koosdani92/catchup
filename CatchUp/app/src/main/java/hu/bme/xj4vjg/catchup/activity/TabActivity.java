package hu.bme.xj4vjg.catchup.activity;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.adapter.TabPagerAdapter;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.Notification;
import hu.bme.xj4vjg.catchup.api.NotificationType;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.GetEventsTask;
import hu.bme.xj4vjg.catchup.apihelper.GetFriendsTask;
import hu.bme.xj4vjg.catchup.apihelper.GetNewNotificationCountTask;
import hu.bme.xj4vjg.catchup.apihelper.GetNotificationsTask;
import hu.bme.xj4vjg.catchup.apihelper.LoginType;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;
import hu.bme.xj4vjg.catchup.fragment.EventsFragment;
import hu.bme.xj4vjg.catchup.fragment.FriendsFragment;
import hu.bme.xj4vjg.catchup.fragment.NotificationsFragment;
import hu.bme.xj4vjg.catchup.preferences.PreferencesHelper;

public class TabActivity extends AppCompatActivity implements
        GetNewNotificationCountTask.GetNewNotificationCountListener,
        NotificationsFragment.NotificationActionListener{
    // Intent extra kulcsok
    public static final String USER_EXTRA_KEY = "UserKey";
    public static final String LOGIN_TYPE_EXTRA_KEY = "LoginTypeKey";

    // A Bejelentkezett felhasználó adatai és a felhasznált bejelentkezési mód típusa
    private User user = null;
    private LoginType loginType = null;

    // API objektum
    private ICatchUpApi catchUpApi;

    // Api hívásokat kezelő Taskok
    private GetNewNotificationCountTask getNewNotificationCountTask;

    // BroadcastReceiver a szervertől jövő értesítések kezelésére
    private BroadcastReceiver apiMessagesBroadcastReceiver;

    // UI vezérlők
    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    TabPagerAdapter tabPagerAdapter;
    TabLayout.OnTabSelectedListener tabSelectedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        if (!getUserData()) {
            return;
        }

        // Taskok inicializálása
        getNewNotificationCountTask = new GetNewNotificationCountTask(this);

        // Api üzeneteket fogó BR létrehozása
        apiMessagesBroadcastReceiver = new ApiMessagesBroadcastReceiver();

        // UI vezérlők elmentése
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        // Toolbar beállítása ActionBar-nak
        setSupportActionBar(toolbar);
        // ViewPagerAdapter feltöltése Fragmensekkel
        tabPagerAdapter = new TabPagerAdapter(this, user, getSupportFragmentManager());
        tabPagerAdapter.initializeFragments();
        viewPager.setAdapter(tabPagerAdapter);
        // Tablayout beállítása
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.friends_tab_selected);
        tabLayout.getTabAt(1).setIcon(R.drawable.events_tab_unselected);
        tabLayout.getTabAt(2).setIcon(R.drawable.notifications_tab_unselected);

        tabSelectedListener = new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()) {
                    case 0:
                        ((CatchUpApplication) getApplication())
                                .trackActionEvent(R.string.analytics_event_view_friends);

                        tabLayout.getTabAt(0).setIcon(R.drawable.friends_tab_selected);
                        break;
                    case 1:
                        ((CatchUpApplication) getApplication())
                                .trackActionEvent(R.string.analytics_event_view_events);

                        tabLayout.getTabAt(1).setIcon(R.drawable.events_tab_selected);
                        break;
                    case 2:
                        ((CatchUpApplication) getApplication())
                                .trackActionEvent(R.string.analytics_event_view_notifications);

                        tabLayout.getTabAt(2).setIcon(R.drawable.notifications_tab_selected);
                        break;

                    default:
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        tabLayout.getTabAt(0).setIcon(R.drawable.friends_tab_unselected);
                        break;
                    case 1:
                        tabLayout.getTabAt(1).setIcon(R.drawable.events_tab_unselected);
                        break;
                    case 2:
                        tabLayout.getTabAt(2).setIcon(R.drawable.notifications_tab_unselected);
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        };
        tabLayout.setOnTabSelectedListener(tabSelectedListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!getUserData()) {
            return;
        };

        // API objektum és a taskok inicializálása
        catchUpApi = CatchUpApiGenerator.generateApi(this);
        getNewNotificationCountTask.setApi(catchUpApi);

        // Ha az Activity előtérbe kerül, akkor frissítjük a Tabok tartalmát
        getNewNotificationCountTask.run(user.getId());
        tabPagerAdapter.updateFragments();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_home);
        CatchUpApplication.setApplicationVisible(true);

        // Api üzeneteket fogó BR regisztrálása
        LocalBroadcastManager.getInstance(this).registerReceiver(apiMessagesBroadcastReceiver,
                new IntentFilter(getString(R.string.catchupapi_message_received_action)));
    }

    @Override
    protected void onPause() {
        super.onPause();
        CatchUpApplication.setApplicationVisible(false);

        // Api üzeneteket fogó BR kiregisztrálása
        LocalBroadcastManager.getInstance(this).unregisterReceiver(apiMessagesBroadcastReceiver);
    }

    // User adatok elmentése
    private boolean getUserData() {
        Bundle extras = (getIntent() == null || getIntent().getExtras() == null)
                ? new Bundle() : getIntent().getExtras();

        // A user adatinak elmentése
        if (user == null || loginType == null) {
            if (extras.containsKey(TabActivity.USER_EXTRA_KEY)
                    && extras.containsKey(TabActivity.LOGIN_TYPE_EXTRA_KEY)) {
                this.user = (User) extras.getParcelable(TabActivity.USER_EXTRA_KEY);
                this.loginType = LoginType.valueOf(
                        extras.getString(TabActivity.LOGIN_TYPE_EXTRA_KEY));
            }
            else {
                MainActivity.sendApplicationError(this);
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tab, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            ((CatchUpApplication) getApplication()).trackActionEvent(R.string.analytics_event_logout);

            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(MainActivity.USER_LOGGED_OUT_EXTRA_KEY, true);
            String loginTypeString = (loginType == null) ? "" : loginType.toString();
            intent.putExtra(MainActivity.LOGOUT_TYPE_EXTRA_KEY, loginTypeString);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    // Api üzeneteket fogó BR
    private class ApiMessagesBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Ha kaptunk egy értesítést, akkor indítunk egy új érteseket lekérő taskot
            // ami, ha tényleg jött új értesítés, frissíti a megfelelő tabokat
            if (getNewNotificationCountTask != null && user != null) {
                getNewNotificationCountTask.run(user.getId());
            }
        }
    }

    // GetNewNotificationCount task callback
    @Override
    public void onGetNewNotificationCountFinished(Integer response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this);
        }

        if (response > 0) {
            Log.d("debug_key", "New notifications found");
            if (tabLayout.getTabAt(2) != null) {
                Log.d("debug_key", "Setting highlight icon");
                tabLayout.getTabAt(2).setIcon(R.drawable.notifications_tab_highlighted);
            }
            tabPagerAdapter.updateFragments();
        }
    }

    // Értesítés a NotificationFragment-en belüli esemény elfogadásról/elutasításról
    @Override
    public void onNotificationAction(NotificationType notificationType) {
        if (notificationType == NotificationType.FriendRequest) {
            tabPagerAdapter.updateFriendsFragment();
        }
        else {
            tabPagerAdapter.updateEventsFragment();
        }
    }
}
