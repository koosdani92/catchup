package hu.bme.xj4vjg.catchup.asynctask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;

public class ReverseGeocodeTask extends AsyncTask<Void, Void, List<Address>> {
    public static final String TAG = "ReverseGeocodeTask";

    public interface ReverseGeocodeListener {
        void onReverseGeocodeFinished(List<Address> addresses);
    }

    ReverseGeocodeListener listener;
    Context context;
    double latitude;
    double longitude;
    int maxResults = 1;

    public ReverseGeocodeTask(Context context, double latitude, double longitude, int maxResults) {
        this.context = context;
        this.latitude = latitude;
        this.longitude = longitude;
        this.maxResults = maxResults;
    }

    public ReverseGeocodeTask(Context context, double latitude, double longitude) {
        this.context = context;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setListener(ReverseGeocodeListener listener) {
        this.listener = listener;
    }

    @Override
    protected List<Address> doInBackground(Void... params) {
        List<Address> addresses = new ArrayList<Address>();

        Geocoder geocoder = new Geocoder(context);
        try {
            addresses.addAll(geocoder.getFromLocation(latitude, longitude, maxResults));
        } catch (IOException e) {
            Log.d(TAG, context.getString(R.string.geocode_error));
        }

        return addresses;
    }

    @Override
    protected void onPostExecute(List<Address> addresses) {
        if (listener != null) {
            listener.onReverseGeocodeFinished(addresses);
        }
    }
}
