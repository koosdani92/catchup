package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.AnswerEventInviteRequest;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.SendFriendRequestRequest;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SendFriendRequestTask extends AbstractApiTask {
    private int requestedId;

    public interface SendFriendRequestListener {
        void onSendFriendRequestFinished(Boolean response, int requestedId, ApiException exception);
    }

    private SendFriendRequestListener listener;

    public SendFriendRequestTask(SendFriendRequestListener listener) {
        this.listener = listener;
    }

    public void run(int userId, int requestedId) {
        this.requestedId = requestedId;
        Callback<RestResponse<Boolean>> callback = new Callback<RestResponse<Boolean>>() {
            @Override
            public void success(RestResponse<Boolean> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.sendFriendRequest(
                new SendFriendRequestRequest(userId, requestedId),
                callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(Boolean response, ApiException exception) {
        if (listener != null) {
            listener.onSendFriendRequestFinished(response, requestedId, exception);
        }
    }
}
