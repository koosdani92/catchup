package hu.bme.xj4vjg.catchup.apihelper;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.CreateEventRequest;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.SendEventInviteRequest;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SendEventInviteTask extends AbstractApiTask {
    public interface SendEventInviteListener {
        void onSendEventInviteFinished(Boolean response, int invitedId, ApiException exception);
    }

    private int invitedId;
    private SendEventInviteListener listener;

    public SendEventInviteTask(SendEventInviteListener listener) {
        this.listener = listener;
    }

    public void run(int userId, int eventId, int invitedId) {
        this.invitedId = invitedId;
        Callback<RestResponse<Boolean>> callback = new Callback<RestResponse<Boolean>>() {
            @Override
            public void success(RestResponse<Boolean> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.sendEventInvite(
                new SendEventInviteRequest(userId, eventId, invitedId),
                callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(Boolean response, ApiException exception) {
        if (listener != null) {
            listener.onSendEventInviteFinished(response, invitedId, exception);
        }
    }
}
