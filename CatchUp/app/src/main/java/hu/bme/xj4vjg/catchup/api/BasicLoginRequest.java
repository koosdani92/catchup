package hu.bme.xj4vjg.catchup.api;

/**
 * A basicLogin API h�v�s param�ter le�r�ja.
 */
public class BasicLoginRequest {
	public String email;
	public String pass;

	public BasicLoginRequest() {}
	
	public BasicLoginRequest(String email, String pass) {
		this.email = email;
		this.pass = pass;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
}