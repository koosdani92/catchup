package hu.bme.xj4vjg.catchup.api;

public enum EventInvitationState {
	Pending, Going, NotGoing
}
