package hu.bme.xj4vjg.catchup.activity;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.adapter.FriendAdapter;
import hu.bme.xj4vjg.catchup.adapter.SearchUserAdapter;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.ICatchUpApi;
import hu.bme.xj4vjg.catchup.api.User;
import hu.bme.xj4vjg.catchup.api.UserDetail;
import hu.bme.xj4vjg.catchup.apihelper.CatchUpApiGenerator;
import hu.bme.xj4vjg.catchup.apihelper.GetFriendsTask;
import hu.bme.xj4vjg.catchup.apihelper.SearchUserTask;
import hu.bme.xj4vjg.catchup.apihelper.SendFriendRequestTask;
import hu.bme.xj4vjg.catchup.application.CatchUpApplication;

public class SearchUsersActivity extends AppCompatActivity implements
        SearchUserTask.SearchUserListener,
        SendFriendRequestTask.SendFriendRequestListener,
        SearchUserAdapter.OnItemClickListener {
    public static final String USER_EXTRA_KEY = "UserKey";

    private User user;
    private ICatchUpApi catchUpApi;
    private List<UserDetail> users;
    private SearchUserAdapter searchUserAdapter;
    private SearchUserTask searchUserTask;
    private SendFriendRequestTask sendFriendRequestTask;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_users);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = (getIntent() == null || getIntent().getExtras() == null)
                ? new Bundle() : getIntent().getExtras();
        if (!extras.containsKey(USER_EXTRA_KEY)) {
            MainActivity.sendApplicationError(this);
            return;
        }
        user = extras.getParcelable(USER_EXTRA_KEY);

        searchUserTask = new SearchUserTask(this);
        sendFriendRequestTask = new SendFriendRequestTask(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        users = new ArrayList<UserDetail>();
        searchUserAdapter = new SearchUserAdapter(users);
        searchUserAdapter.setListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(searchUserAdapter);

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_search_user);

            String query = intent.getStringExtra(SearchManager.QUERY);
            searchUserTask.run(user.getId(), query);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        catchUpApi = CatchUpApiGenerator.generateApi(this);
        searchUserTask.setApi(catchUpApi);
        sendFriendRequestTask.setApi(catchUpApi);

        searchUserTask.run(user.getId(), "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CatchUpApplication) getApplication()).trackScreen(R.string.analytics_screen_search_friend);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_users, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(getApplicationContext(), SearchUsersActivity.class)));


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAddFriend(int friendId) {
        sendFriendRequestTask.run(user.getId(), friendId);
    }

    @Override
    public void onSendFriendRequestFinished(Boolean response, int requestedId, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this);
        }

        if (response != null && response) {
            ((CatchUpApplication) getApplication())
                    .trackActionEvent(R.string.analytics_event_request_friend);

            int i = 0;
            while (i < users.size()) {
                if (users.get(i).getUser().getId() == requestedId) {
                    users.get(i).setFriend(false);
                    users.get(i).setPendingFriend(true);
                    break;
                }
                i++;
            }
            searchUserAdapter.notifyDataSetChanged();
        }
        else {
            Toast.makeText(this,
                    getString(R.string.friend_request_not_sent),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSearchUserFinished(List<UserDetail> response, ApiException exception) {
        if (exception != null) {
            MainActivity.sendApplicationError(this);
        }

        if (response != null) {
            users.clear();
            users.addAll(response);
            searchUserAdapter.notifyDataSetChanged();
        }
    }
}
