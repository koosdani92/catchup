package hu.bme.xj4vjg.catchup.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.apihelper.LoginType;

public class PreferencesHelper {
    public static final String LOGGED_IN_USERID_KEY = "LoggedInUseridKey";
    public static final String GCM_TOKEN_KEY = "GcmTokenKey";
    public static final String GEOFENCE_EVENT_SET_KEY = "GeofenceEventSetKey";

    public static final String AUTO_LOGIN_ENABLED = "AutoLoginEnabled";
    public static final String AUTO_LOGIN_TYPE = "AutologinType";
    public static final String FACEBOOK_LOGIN_ID = "FacebookLoginId";
    public static final String GOOGLE_LOGIN_ID = "GoogleLoginId";
    public static final String BASIC_LOGIN_EMAIL = "BasicLoginEmail";
    public static final String BASIC_LOGIN_PASSWORD = "BasicLoginPassword";

    public static String getString(Context context, String key, String defValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, defValue);
    }

    public static int getInt(Context context, String key, int defValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(key, defValue);
    }

    public static boolean getBoolean(Context context, String key, boolean defValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, defValue);
    }

    public static void putString(Context context, String key, String newValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, newValue);
        editor.commit();
    }

    public static void putInt(Context context, String key, int newValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, newValue);
        editor.commit();
    }

    public static void putBoolean(Context context, String key, boolean newValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, newValue);
        editor.commit();
    }

    public static void remove(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public static boolean contains(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(key);
    }

    // Autologin adatok törlése
    public static void clearAutoLoginData(Context context) {
        PreferencesHelper.putBoolean(context, AUTO_LOGIN_ENABLED, false);
        PreferencesHelper.remove(context, PreferencesHelper.AUTO_LOGIN_TYPE);
        PreferencesHelper.remove(context, PreferencesHelper.FACEBOOK_LOGIN_ID);
        PreferencesHelper.remove(context, PreferencesHelper.GOOGLE_LOGIN_ID);
        PreferencesHelper.remove(context, PreferencesHelper.BASIC_LOGIN_EMAIL);
        PreferencesHelper.remove(context, PreferencesHelper.BASIC_LOGIN_PASSWORD);
    }

    public static Set<String> getGeofenceEventSet(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getStringSet(GEOFENCE_EVENT_SET_KEY, new HashSet<String>());
    }

    public static void putGeofenceEventSet(Context context, Set<String> geofenceEventSet) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(GEOFENCE_EVENT_SET_KEY, geofenceEventSet);
        editor.commit();
    }

    public static void removeGeofenceEventSet(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(GEOFENCE_EVENT_SET_KEY);
        editor.commit();
    }
}
