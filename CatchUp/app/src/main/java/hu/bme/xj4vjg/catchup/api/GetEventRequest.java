package hu.bme.xj4vjg.catchup.api;


public class GetEventRequest extends AuthRequest {
    private int eventId;

    public GetEventRequest() { }

    public GetEventRequest(int userId, int eventId) {
        this.userId = userId;
        this.eventId = eventId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
}
