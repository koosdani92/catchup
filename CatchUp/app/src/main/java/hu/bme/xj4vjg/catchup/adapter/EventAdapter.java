package hu.bme.xj4vjg.catchup.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.User;

// Adapter osztály a RecyclerView használatához
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {
    public interface OnItemClickListener {
        void onEventSelected(Event event);
    };

    private Context context;
    private User user;
    private List<Event> data;
    private OnItemClickListener listener;

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView cardView;
        public TextView titleTextView;
        public TextView organizerTextView;
        public EventViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            titleTextView = (TextView) itemView.findViewById(R.id.title_text_view);
            organizerTextView = (TextView) itemView.findViewById(R.id.organizer_text_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null && !data.isEmpty()) {
                listener.onEventSelected(data.get(getAdapterPosition()));
            }
        }
    }

    public EventAdapter(Context context, User user, List<Event> data) {
        this.context = context;
        this.user = user;
        this.data = data;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext())
                .inflate(R.layout.event_row, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        if (data.get(position).getOwner().getId() == user.getId()) {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cardview_highlight));
        }
        else {
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cardview_normal));
        }

        holder.titleTextView.setText(data.get(position).getTitle());
        holder.organizerTextView.setText(data.get(position).getOwner().getFullName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
