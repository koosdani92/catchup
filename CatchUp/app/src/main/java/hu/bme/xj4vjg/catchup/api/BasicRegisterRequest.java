package hu.bme.xj4vjg.catchup.api;

/**
 * A basicRegister API h�v�s param�ter le�r�ja.
 */
public class BasicRegisterRequest {
	public String email;
	public String pass;
	public String firstName;
	public String lastName;

	public BasicRegisterRequest() {}
	
	public BasicRegisterRequest(String email, String pass, String firstName, String lastName) {
		this.email = email;
		this.pass = pass;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
