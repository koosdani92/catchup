package hu.bme.xj4vjg.catchup.api;

public enum NotificationType {
	FriendRequest, EventInvite
}
