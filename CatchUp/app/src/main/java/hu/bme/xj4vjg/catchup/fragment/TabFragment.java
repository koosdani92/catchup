package hu.bme.xj4vjg.catchup.fragment;

public interface TabFragment {
    void updateContent();
}
