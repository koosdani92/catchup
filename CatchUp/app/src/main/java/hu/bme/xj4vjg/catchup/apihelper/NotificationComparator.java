package hu.bme.xj4vjg.catchup.apihelper;

import java.util.Comparator;

import hu.bme.xj4vjg.catchup.api.Notification;

public class NotificationComparator implements Comparator<Notification> {
    private int userId;

    public NotificationComparator(int userId) {
        this.userId = userId;
    }

    @Override
    public int compare(Notification notification1, Notification notification2) {
        if (notification1.getDate(userId) < notification2.getDate(userId)) {
            return 1;
        } else if (notification1.getDate(userId) > notification2.getDate(userId)) {
            return -1;
        } else {
            return 0;
        }
    }
}
