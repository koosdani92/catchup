package hu.bme.xj4vjg.catchup.apihelper;

import java.util.List;

import hu.bme.xj4vjg.catchup.api.AnswerEventInviteRequest;
import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.AuthRequest;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AnswerEventInviteTask extends AbstractApiTask {
    public interface AnswerEventInviteListener {
        void onAnswerEventInviteFinished(Boolean response, ApiException exception);
    }

    private AnswerEventInviteListener listener;

    public AnswerEventInviteTask(AnswerEventInviteListener listener) {
        this.listener = listener;
    }

    public void run(int userId, int eventId, EventInvitationState answer) {
        Callback<RestResponse<Boolean>> callback = new Callback<RestResponse<Boolean>>() {
            @Override
            public void success(RestResponse<Boolean> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.answerEventInvite(
                new AnswerEventInviteRequest(userId, eventId, answer),
                callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(Boolean response, ApiException exception) {
        if (listener != null) {
            listener.onAnswerEventInviteFinished(response, exception);
        }
    }
}