package hu.bme.xj4vjg.catchup.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hu.bme.xj4vjg.catchup.R;
import hu.bme.xj4vjg.catchup.api.EventInvitationState;
import hu.bme.xj4vjg.catchup.api.InvitedUser;

public class InvitedUserAdapter extends RecyclerView.Adapter<InvitedUserAdapter.InvitedUserViewHolder> {
    private List<InvitedUser> invitedUsers;

    public class InvitedUserViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public ImageView goingImageView;
        public ImageView pendingImageView;
        public ImageView notGoingImageView;
        public InvitedUserViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            goingImageView = (ImageView) itemView.findViewById(R.id.going_image_view);
            pendingImageView = (ImageView) itemView.findViewById(R.id.pending_image_view);
            notGoingImageView = (ImageView) itemView.findViewById(R.id.not_going_image_view);
        }
    }

    public InvitedUserAdapter(List<InvitedUser> data) {
        this.invitedUsers = data;
    }

    @Override
    public InvitedUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext())
                .inflate(R.layout.invited_user_row, parent, false);
        return new InvitedUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InvitedUserViewHolder holder, int position) {
        InvitedUser currentInvitedUser = invitedUsers.get(position);
        holder.nameTextView.setText(currentInvitedUser.getUser().getFullName());

        holder.goingImageView.setVisibility(View.GONE);
        holder.pendingImageView.setVisibility(View.GONE);
        holder.notGoingImageView.setVisibility(View.GONE);
        if (currentInvitedUser.getInvitationState() == EventInvitationState.Going) {
            holder.goingImageView.setVisibility(View.VISIBLE);
        } else if (currentInvitedUser.getInvitationState() == EventInvitationState.Pending) {
            holder.pendingImageView.setVisibility(View.VISIBLE);
        } else if (currentInvitedUser.getInvitationState() == EventInvitationState.NotGoing) {
            holder.notGoingImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return invitedUsers.size();
    }
}