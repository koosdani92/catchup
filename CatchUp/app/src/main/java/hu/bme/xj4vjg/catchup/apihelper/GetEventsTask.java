package hu.bme.xj4vjg.catchup.apihelper;

import java.util.List;

import hu.bme.xj4vjg.catchup.api.ApiException;
import hu.bme.xj4vjg.catchup.api.AuthRequest;
import hu.bme.xj4vjg.catchup.api.Event;
import hu.bme.xj4vjg.catchup.api.RestResponse;
import hu.bme.xj4vjg.catchup.api.User;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetEventsTask extends AbstractApiTask {
    public interface GetEventsListener {
        void onGetEventsFinished(List<Event> response, ApiException exception);
    }

    private GetEventsListener listener;

    public GetEventsTask(GetEventsListener listener) {
        this.listener = listener;
    }

    public void run(int userId) {
        Callback<RestResponse<List<Event>>> callback = new Callback<RestResponse<List<Event>>>() {
            @Override
            public void success(RestResponse<List<Event>> result, Response response) {
                notifyListener(result.getResponse(), result.getException());
            }

            @Override
            public void failure(RetrofitError error) {
                notifyListener(null, new ApiException(ApiException.ApiExceptionType.Unknown));
            }
        };
        catchUpApi.getEvents(new AuthRequest(userId), callback);
    }

    // Listener értesítése a barátok lekérésének eredményéről
    protected void notifyListener(List<Event> response, ApiException exception) {
        if (listener != null) {
            listener.onGetEventsFinished(response, exception);
        }
    }
}
